/* Toggle between adding and removing the "responsive" class to topnav when the user clicks on the icon */
function myFunction() {
    document.getElementsByClassName("topnav")[0].classList.toggle("responsive");
}

$(document).ready(function () {
	
	
	//$(":file").filestyle({size: "nr"});
	
	$("div[id=sidebar-wrapper]").hide();
	 $("div[id=main1]").hide();
	 $("div[id=main2]").hide();
	 $("div[id=main3]").hide();
	 $("div[id=main4]").hide();
	  $("div[id=main5]").hide();
	  $("div[id=main6]").hide();
	   $("div[id=main7]").hide();
	 //  $("div[id=main8]").hide();
	   $("div[id=main9]").hide();
	   $("div[id=main10]").hide();
	   if($("input[id=hidmenuitem]").val() =="#home" && $("input[id=hidsubitem]").val()==""){
		   $("a[href=#home]").click();
	  }
	  if($("input[id=hidmenuitem]").val() =="#createpack" && $("input[id=hidsubitem]").val()==""){
		  $("input[name=proname]").val("");
		  $("a[href=#createpack]").click();
	  }
	if($("input[id=hidmenuitem]").val() =="#modify" && $("input[id=hidsubitem]").val()==""){
		$("a[href=#modify]").click();
		
	 }
	 if($("input[id=hidmenuitem]").val() =="#qTest" && $("input[id=hidsubitem]").val()=="#moproject"){
		 $("a[href=#qTest]").click();
		  $("a[href=#qtestproject]").attr("style","background-color: #429fef;");
		 createprojectview();
	 }
	 if($("input[id=hidmenuitem]").val() =="#createpack" && $("input[id=hidsubitem]").val()=="#moproject"){
		 $("a[href=#createpack]").click();
		  $("a[href=#crproject]").attr("style","background-color: #429fef;");
		 createprojectview();
	 }
	if($("input[id=hidmenuitem]").val() =="#createpack" && $("input[id=hidsubitem]").val()=="#sconfig"){
		 createpackview()
		 $("a[href=#sconfig]").attr("style","background-color: #429fef;");
		 projectconfigview();
	 }
	if($("input[id=hidmenuitem]").val() =="#createpack" && $("input[id=hidsubitem]").val()=="#scenario"){
		 createpackview()
		 $("a[href=#scenario]").click();
	 }
	if($("input[id=hidmenuitem]").val() =="#createpack" && $("input[id=hidsubitem]").val()=="#BTFscenario"){
		 createpackview()
		 $("a[href=#BTFscenario]").click();
	 }
	if($("input[id=hidmenuitem]").val() =="#createpack" && $("input[id=hidsubitem]").val()=="#bussfunction"){
		 createpackview()
		 $("a[href=#bussfunction]").click();
	 }
	if($("input[id=hidmenuitem]").val() =="#createpack" && $("input[id=hidsubitem]").val()=="#cstructure"){
		 createpackview()
		 $("a[href=#cstructure]").click();
	 }
	if($("input[id=hidmenuitem]").val() =="#createpack" && $("input[id=hidsubitem]").val()=="#cipdata"){
		 createpackview()
		 $("a[href=#cipdata]").click();
	 }
	
	if($("input[id=hidmenuitem]").val() =="#createpack" && $("input[id=hidsubitem]").val()=="#dashboard"){
		 createpackview()
		 $("a[href=#dashboard]").click();
	 }
	if($("input[id=hidmenuitem]").val() =="#modify" && $("input[id=hidsubitem]").val()=="#moproject"){
		$("a[href=#modify]").click();
		$("a[href=#sconfig]").click();
	 }
	if($("input[name=transactionaction]").val() == "continue" && $("input[id=hidsubitem]").val()=="#cstructure"){
		$("#strucureSheetDialog").modal('show');
	}	
	if($("input[id=hidmenuitem]").val() =="#createpack" && $("input[id=hidsubitem]").val()=="#vtemplate"){
		 createpackview()
		 $("a[href=#vtemplate]").click();
	 }
	if($("input[name=verifytransactionaction]").val() == "continue" && $("input[id=hidsubitem]").val()=="#vtemplate"){
		$("#verifystrucureSheetDialog").modal('show');
	}
	

	/**
	 * pagination table entry
	 */
	
	 $('#projectTable').pageMe({pagerSelector:'#myPager',showPrevNext:true,hidePageNumbers:false,perPage:4});
	 $('#TransactionTable').pageMe({pagerSelector:'#myTPager',showPrevNext:true,hidePageNumbers:false,perPage:4});
	 $('#VerficationTable').pageMe({pagerSelector:'#myVPager',showPrevNext:true,hidePageNumbers:false,perPage:4});
	 
	 $('[data-toggle="tooltip"]').tooltip({
			 placement: $(this).data("placement") || 'top'		 
      }); 
	 
	 	 
	 $("a[id=rename]").click(function(){
			$("#renameDialog").modal('show');
			$("input[id=oldproname]").val("");
			$("input[id=oldproname]").val($(this).parents("tr").children('td')[1].children[0].textContent);
		});
	 $("button[name=renameSubmit]").click(function(){
		 $(this).parents("form").submit();
	 });
	 $("a[name=copyproject]").click(function(){
		 $("input[name=projectaction]").val("copyproject");
		 $("input[name=directoryname]").val($(this).parents("tr").children('td')[1].children[0].textContent);
		 $(this).parents("form").submit();
	 });
	 $("a[name=deleteproject]").click(function(){
		 $("#deleteDialog").modal('show');
		 $("input[name=directoryname]").val($(this).parents("tr").children('td')[1].children[0].textContent);
	 });
	 
	 $("button[name=deleteSubmit]").click(function(){
		 $("input[name=projectaction]").val("deleteproject");
		
		 $(this).parents("form").submit();
	 });
	 $("a[id=renameTrans]").click(function(){
			$("#renameTDialog").modal('show');
			$("input[id=oldtranname]").val("");
			$("input[id=oldtranname]").val($(this).parents("tr").children('td')[1].children[0].textContent);
		});
	 $("button[name=renameTrans]").click(function(){
		 $(this).parents("form").submit();
	 });
	 $("a[name=copyTrans]").click(function(){
		 $("input[name=transactionaction]").val("copytransaction");
		 $("input[name=transactionname]").val($(this).parents("tr").children('td')[1].children[0].textContent);
		 $(this).parents("form").submit();
	 });
	 $("a[name=deleteTrans]").click(function(){
		 $("#deleteTDialog").modal('show');
		 $("input[name=transactionname]").val($(this).parents("tr").children('td')[1].children[0].textContent);
		 
	 });

	 $("a[name=deleteverifyTrans]").click(function(){
		 $("#deleteVDialog").modal('show');
		 $("input[name=verifytransactionname]").val($(this).parents("tr").children('td')[1].children[0].textContent);
		 
	 });
	 
/*	  $(".up,.down").click(function () {
          
	         var $element = this;
	         var row = $($element).parents("tr:first");
	         
	         if($(this).is('.up')){
	             row.insertBefore(row.prev());
	             row.attr('class', 'selected');
	         }
	         
	         else{
	              row.insertAfter(row.next());
	              row.attr('class', 'selected');
	         }
	         
	        
	       
	    });*/
	  
	  
	  
	  var sorter = $('#tab_logic_strucure').rowSorter(
			  {
			  onDragStart : function(tbody, row,index) {
			  //log('index: ' + index);
			  console.log('onDragStart: active row\'s index is '+ index);
			  },
			  handler: "td.sort-handler",
			  onDrop : function(tbody, row,new_index, old_index) {
			  //log('old_index: ' + old_index + ', new_index: ' + new_index);                                                                                                                                                                                  
			  console.log('onDrop: row moved from '+ old_index+ ' to '+ new_index);
			  }
			  }); 
	  
	  
	  
	  
	  $("a[href=#objcap]").click(function() {

		  $("#objCapDialog").modal('show');
	    });
	  
		 $("a[href=#startnav]").click(function(){
			 createpackview();
			 $("a[href=#crproject]").click();
		 });
		 
		 $("a[href=#qtestproject]").click(function(){
				
			 qtprojectview()
				
				
			});
		 
		 function qtprojectview(){
				
				   $("div[id=main10]").show();
				   $("div[id=main11]").show();
				  
				
			}

		 $("a[href=#qtestdesign]").click(function(){
				
			 qtprojectview()
				
				
			});
		 
		 function qtprojectview(){
				
				   $("div[id=main10]").show();
				   $("div[id=main11]").show();
				  
				
			}
		

});





	$("button[name=deletetransSubmit]").click(function(){
		 $("input[name=transactionaction]").val("deletetransaction");
		 $(this).parents("form").submit();
	});
	$("button[name=deleteverifytransSubmit]").click(function(){
		 $("input[name=verifytransactionaction]").val("deleteverificationtransaction");
		 $(this).parents("form").submit();
	});

$("a[href=#createpack]").click(function(){
	 $("a[href=#crproject],a[href=#sconfig],a[href=#cstructure] ,a[href=#cipdata],a[href=#scenario],a[href=#dashboard],a[href=#vflow],a[href=#vtemplate]").attr("style","background-color:#2a6598;");
	
	createpackview()
});




$("a[name=#add_mainControllerData]").click(function(){
	 $("#addTestScenarioDialog").modal('show');
	 $("input[name=noMainControllerrow]").val("");
	 $("input[name=mainControlleraction]").val("");
	
});


$("a[name=#delete_mainControllerData]").click(function(){
	var count = $('#tab_logic_maincontrol_body').children().length;
			
				try {
					$('.checker:checkbox:checked').parents("tr").remove();
					$("input[name=noMainControllerrow]").val(count);
					 $("input[name=mainControlleraction]").val("delete");
					 $(this).parents("form").submit();
					} catch (e) {
					alert(e);
				}
		
		
	
	 
});



$("button[name=createTrans]").click(function(){
	$("input[name=transactionaction]").val("");
	
	
});
$("a[name=#add_transaction]").click(function(){
	$("input[name=currentScenarioid]").val($(this).parents("tr").children('td')[2].textContent);
	$("input[name=currentcolumnname]").val($(this).parents("td").index());
	$("input[name=transactionName]").val("");
	$("input[name=noMainControllerrow]").val("");
	 $("input[name=mainControlleraction]").val("");
	
	 $("input[name=rowindex]").val($(this).parents("tr").index());
	 //alert("hi--->" + $(this).parents("tr").index());
	 if($(this).html() !="Select Transaction"){
	 $("input[name=transactionName]").val($(this).html());
	 $("button[name=editTranscenario]").attr("disabled", false);
	 $("button[name=saveTxnscenario]").attr("disabled", false);
	 $("button[name=deleteTxnScenario]").attr("disabled", false);
	 }else{
		 $("button[name=editTranscenario]").attr("disabled", true);
		 $("button[name=saveTxnscenario]").attr("disabled", false);
		 $("button[name=deleteTxnScenario]").attr("disabled", true);
	 }
	 
	 $("#addtxnScenarioDialog").modal('show');
	
});

$("a[href=#item]").click(function(){
	$("input[name=transactionName]").val($(this).html());
});

$("a[name=#nav_Txnscreen]").click(function(){
	 $("a[href=#cstructure]").click();
});

/*var selectedval= $("select[name=transDatainputfile]").val();
$("input[name=dtransactiontype1]").val(selectedval);*/

function createpackview(){
	$("div[id=sidebar-wrapper]").show();
	$("a[href=#crproject]").show();
	$("a[href=#moproject]").hide();
	$("div[id=main1]").hide();
	$("div[id=main2]").hide();
	$("div[id=main3]").hide();
	$("div[id=main4]").hide();
	 $("div[id=main5]").hide();
	 $("div[id=main6]").hide();
	  $("div[id=main7]").hide();
	//  $("div[id=main8]").hide();
	  $("div[id=main9]").hide();
	  $("div[id=main10]").hide();
}
$("a[href=#home]").click(function(){
	homepageview()
		
});

//sathish created for qTest call

$("a[href=#qtest]").click(function(){
	qTestpageview()
		
});

function qTestpageview(){
	$("div[id=sidebar-wrap]").show()
	$("div[id=sidebar-wrapper]").hide()
	$("div[id=main1]").hide();
	 $("div[id=main2]").hide();
	 $("div[id=main3]").hide();
	 $("div[id=main4]").hide();
	  $("div[id=main5]").hide();
	  $("div[id=main6]").hide();
	   $("div[id=main7]").hide();
	 //  $("div[id=main8]").hide();
	   $("div[id=main9]").hide();
	   $("div[id=main10]").show();
}

//sathish changes for jenkins START
$("a[href=#jenkins]").click(function(){
	jenkinsview()
		
});

function jenkinsview(){
	
	 $("div[id=jenkinsconfig]").show()
	$("div[id=sidebar-wrap]").hide()
	$("div[id=sidebar-wrapper]").hide()
	$("div[id=main1]").hide();
	 $("div[id=main2]").hide();
	 $("div[id=main3]").hide();
	 $("div[id=main4]").hide();
	  $("div[id=main5]").hide();
	  $("div[id=main6]").hide();
	   $("div[id=main7]").hide();
	 //  $("div[id=main8]").hide();
	   $("div[id=main9]").hide();
	   $("div[id=main10]").hide();
	   $("div[id=main11]").hide();
	   $("div[id=main12]").show();
}

//sathish changes for jenkins END

function homepageview(){
	$("div[id=sidebar-wrapper]").hide()
	$("div[id=main1]").show();
	 $("div[id=main2]").hide();
	 $("div[id=main3]").hide();
	 $("div[id=main4]").hide();
	  $("div[id=main5]").hide();
	  $("div[id=main6]").hide();
	   $("div[id=main7]").hide();
	 //  $("div[id=main8]").hide();
	   $("div[id=main9]").hide();
	   $("div[id=main10]").hide();
	   $("div[id=main11]").hide();
}
$("a[href=#modify]").click(function(){
	modifyview()
});

function modifyview(){
	$("div[id=sidebar-wrapper]").show()
	$("a[href=#crproject]").hide();
	$("a[href=#moproject]").show();
	$("div[id=main1]").hide();
	 $("div[id=main2]").hide();
	 $("div[id=main3]").hide();
	 $("div[id=main4]").hide();
	  $("div[id=main5]").hide();
	  $("div[id=main6]").hide();
	   $("div[id=main7]").hide();
	 //  $("div[id=main8]").hide();
	   $("div[id=main9]").hide();
	
}
$("a[href=#execution]").click(function(){
	
	executeview()
	
	
	 $("input[name=navigationAction]").val("execute");
	 $(this).parents("form").submit();
});

$("a[href=#log]").click(function(){
	
	executeview()
	
	
	 $("input[name=navigationAction]").val("logfile");
	 $(this).parents("form").submit();
});

function executeview(){
	$("div[id=sidebar-wrapper]").hide()
	$("div[id=main1]").hide();
	 $("div[id=main2]").hide();
	 $("div[id=main3]").hide();
	  $("div[id=main4]").hide();
	   $("div[id=main5]").hide();
	   $("div[id=main6]").hide();
	    $("div[id=main7]").hide();
	  //  $("div[id=main8]").hide();
	    $("div[id=main9]").hide();
}

//bhaskar qtest integration changes START



//bhaskar qtest integration changes END

$("a[href=#crproject]").click(function(){
	 $("input[name=proname]").val("");
	 $("label[id=infomsg1]").empty();
	 $("label[id=infomsg2]").empty();
	 $("a[href=#crproject]").attr("style","background-color: #429fef;");
	 $("a[href=#sconfig],a[href=#cstructure] ,a[href=#cipdata],a[href=#scenario],a[href=#dashboard],a[href=#vflow],a[href=#vtemplate]").attr("style","background-color:#2a6598;");
     createprojectview();
	
});
function createprojectview(){
	$("div[id=main1]").hide();
	$("div[id=main2]").show();
	$("div[id=btnCreate]").show();
	$("div[id=btnmodify]").hide();
	
	$("div[id=main3]").hide();
	 $("div[id=main4]").hide();
	  $("div[id=main5]").hide();
	  $("div[id=main6]").hide();
	   $("div[id=main7]").hide();
	  // $("div[id=main8]").hide();
	   $("div[id=main9]").hide();
	  
	
}
$("a[href=#moproject]").click(function(){
	modifyprojectview()
	
});
/*function modifyprojectview(){
	$("div[id=main1]").hide()
	$("div[id=main2]").show()
	$("div[id=btnCreate]").hide()
	$("div[id=btnmodify]").show()
	
	
	$("div[id=main3]").hide()
	 $("div[id=main4]").hide()
	  $("div[id=main5]").hide()
	  $("div[id=main6]").hide()
}*/
$("a[href=#sconfig]").click(function(){
	//$("div[id=sidebar-wrapper]").show()
	
	
	 $("a[href=#sconfig]").attr("style","background-color: #429fef;");
	 $("a[href=#crproject],a[href=#cstructure] ,a[href=#cipdata],a[href=#scenario],a[href=#dashboard],a[href=#vflow],a[href=#vtemplate]").attr("style","background-color:#2a6598;");
	projectconfigview()
	
	
});

function projectconfigview(){
	$("div[id=main1]").hide();
	$("div[id=main2]").hide();
	$("div[id=main3]").show();
	if($("input[id=timeout]").val() ==""){
		 $("input[id=timeout]").val("100");
	}
	
	
	$("div[id=main4]").hide();
	 $("div[id=main5]").hide();
	 $("div[id=main6]").hide();
	  $("div[id=main7]").hide();
	 // $("div[id=main8]").hide();
	  $("div[id=main9]").hide();
}
$("a[href=#bussfunction]").click(function(){
	$("div[id=sidebar-wrapper]").show();
	$("div[id=main1]").hide();
	$("div[id=main2]").hide();
	$("div[id=main3]").hide();
	$("div[id=main4]").show();
	 $("div[id=main5]").hide();
	 $("div[id=main6]").hide();
	 $("div[id=main7]").hide();
	 //$("div[id=main8]").hide();
	 $("div[id=main9]").hide();
	// $("div[id=mainControllerPreparation]").hide()
	$("div[id=createStructureSheet]").hide()
	$("div[id=structurSheetPreparation]").show()
	
	
		
	
	
	
	$("a[href=#bussfunction]").attr("style","background-color: #429fef;");
	$("a[href=#crproject],a[href=#scenario],a[href=#cstructure] ,a[href=#cipdata],a[href=#sconfig],a[href=#dashboard],a[href=#vflow],a[href=#vtemplate]").attr("style","background-color:#2a6598;");
	
	
});
$("a[href=#scenario]").click(function(){
	$("div[id=sidebar-wrapper]").show()
	$("div[id=main1]").hide();
	$("div[id=main2]").hide();
	$("div[id=main3]").hide();
	$("div[id=main4]").hide();
	 $("div[id=main5]").hide();
	 $("div[id=main6]").hide();
	  $("div[id=main7]").hide();
	//  $("div[id=main8]").hide();
	  $("div[id=main9]").show();
	
	$("div[id=mainControllerPreparation]").show();
//	$("div[id=createStructureSheet]").hide()
//	$("div[id=structurSheetPreparation]").hide()
	
	$("a[href=#scenario]").attr("style","background-color: #429fef;");
	$("a[href=#crproject],a[href=#cstructure] ,a[href=#cipdata],a[href=#sconfig],a[href=#dashboard],a[href=#vflow],a[href=#vtemplate]").attr("style","background-color:#2a6598;");
	
	
});
$("a[href=#cstructure]").click(function(){
	$("div[id=sidebar-wrapper]").show()
	$("div[id=main1]").hide()
	$("div[id=main2]").hide()
	$("div[id=main3]").hide()
	$("div[id=main4]").show()
	 $("div[id=main5]").hide()
	  $("div[id=main6]").hide()
	   $("div[id=main7]").hide();
	// $("div[id=main8]").hide();
	 $("div[id=main9]").hide();
	//$("div[id=mainControllerPreparation]").hide()
	$("div[id=createStructureSheet]").show()
	$("div[id=structurSheetPreparation]").hide()
	
	
	
	$("a[href=#cstructure]").attr("style","background-color: #429fef;");
	$("a[href=#crproject],a[href=#scenario],a[href=#cipdata],a[href=#sconfig],a[href=#dashboard],a[href=#vflow],a[href=#vtemplate]").attr("style","background-color:#2a6598;");
	
});
$("a[href=#cipdata]").click(function(){
	$("div[id=sidebar-wrapper]").show();
	$("div[id=main1]").hide();
	$("div[id=main2]").hide();
	$("div[id=main3]").hide();
	$("div[id=main4]").hide();
	 $("div[id=main5]").show();
	   $("div[id=main6]").hide();
	    $("div[id=main7]").hide();
	  //  $("div[id=main8]").hide();
	    $("div[id=main9]").hide();
	//$("input[id=reqfilename]").val("BaseStructure")
	
	//$("label[id=screenName]").text("Create Input Data");
	
	//$("label[id=tempaltenamelabel]").text("BaseStructure.xls")
	//$("label[id=importlabel]").text("BaseStructure.xls")
	
	
	$("a[href=#cipdata]").attr("style","background-color: #429fef;");
	$("a[href=#crproject],a[href=#scenario],a[href=#cstructure],a[href=#sconfig],a[href=#dashboard],a[href=#vflow],a[href=#vtemplate]").attr("style","background-color:#2a6598;");
	
	
	
});
$("a[href=#vtemplate]").click(function(){
	$("div[id=sidebar-wrapper]").show();
	$("div[id=main1]").hide();
	$("div[id=main2]").hide();
	$("div[id=main3]").hide();
	$("div[id=main4]").hide();
	 $("div[id=main5]").hide();
	   $("div[id=main6]").hide();
	    $("div[id=main7]").show();
	  //  $("div[id=main8]").hide();
	    $("div[id=main9]").hide();
	    $("a[href=#vtemplate]").attr("style","background-color: #429fef;");
		 $("a[href=#crproject],a[href=#scenario] ,a[href=#cstructure],a[href=#sconfig],a[href=#cipdata],a[href=#dashboard],a[href=#vflow]").attr("style","background-color:#2a6598;");
	//$("input[id=reqfilename]").val("VerificationTableList")
	//$("label[id=tempaltenamelabel]").text("VerificationTableList.xls")
	//$("label[id=importlabel]").text("VerificationTableList.xls")
	
	
});
/*$("a[href=#vflow]").click(function(){
	$("div[id=sidebar-wrapper]").show()
	$("div[id=main1]").hide();
	$("div[id=main2]").hide();
	$("div[id=main3]").hide();
	$("div[id=main4]").hide();
	 $("div[id=main5]").hide();
	   $("div[id=main6]").hide();
	    $("div[id=main7]").hide();
	    $("div[id=main8]").show();
	    $("div[id=main9]").hide();
	//$("label[id=tempaltenamelabel]").text()
	    $("a[href=#vflow]").attr("style","background-color: #429fef;");
		 $("a[href=#crproject],a[href=#scenario] ,a[href=#cstructure],a[href=#sconfig],a[href=#cipdata],a[href=#dashboard],a[href=#vtemplate]").attr("style","background-color:#2a6598;");
		
	
});*/
$("a[href=#dashboard]").click(function(){
	$("div[id=sidebar-wrapper]").show()
	$("div[id=main1]").hide()
	$("div[id=main2]").hide()
	$("div[id=main3]").hide()
	$("div[id=main4]").hide()
	 $("div[id=main5]").hide()
	   $("div[id=main6]").show()
	 $("div[id=main7]").hide();	
	// $("div[id=main8]").hide();
	 $("div[id=main9]").hide();
	 $("a[href=#dashboard]").attr("style","background-color: #429fef;");
	 $("a[href=#crproject],a[href=#scenario] ,a[href=#cstructure],a[href=#sconfig],a[href=#cipdata],a[href=#vflow],a[href=#vtemplate]").attr("style","background-color:#2a6598;");
});



// bind "click" event for links with title="submit" 
$("a[title=logouthref]").click( function(){
  // it submits the form it is contained within
  $(this).parents("form")[0].submit();
});

var i=$("input[id=noController]").val()+1;

$("#add_row").click(function(){
 //$('#addr'+i).html("<td>"+ (i+1) +"</td><td><input name='name"+i+"' type='text' placeholder='Name' class='form-control input-md'  /> </td><td><input  name='mail"+i+"' type='text' placeholder='Mail'  class='form-control input-md'></td><td><input  name='mobile"+i+"' type='text' placeholder='Mobile'  class='form-control input-md'></td><td><input  name='exeflag"+i+"' type='text' placeholder='ExecuteFlag'  class='form-control input-md'></td><td><input  name='header1"+i+"' type='text' placeholder='Header1'  class='form-control input-md'></td><td><input  name='header2"+i+"' type='text' placeholder='Header2'  class='form-control input-md'></td><td><input  name='header3"+i+"' type='text' placeholder='Header3'  class='form-control input-md'></td> <td><input  name='header4"+i+"' type='text' placeholder='Header4'  class='form-control input-md'></td> <td><input  name='header5"+i+"' type='text' placeholder='Header5'  class='form-control input-md'></td> <td><input  name='header6"+i+"' type='text' placeholder='Header6'  class='form-control input-md'></td> <td><input  name='header7"+i+"' type='text' placeholder='Header7'  class='form-control input-md'></td> <td><input  name='header8"+i+"' type='text' placeholder='Header8'  class='form-control input-md'></td> <td><input  name='header9"+i+"' type='text' placeholder='Header9'  class='form-control input-md'></td> <td><input  name='header10"+i+"' type='text' placeholder='Header10'  class='form-control input-md'></td>");
	$('#addr'+i).html("<td><input name='groupname"+i+"' type='text' placeholder='GroupName' class='form-control input-md'  /> </td><td><input  name='testcaseid"+i+"' type='text' placeholder='TestCaseId'  class='form-control input-md'></td><td><input  name='testdesc"+i+"' type='text' placeholder='Test_Description'  class='form-control input-md'></td><td><input  name='exeflag"+i+"' type='text' placeholder='ExecuteFlag'  class='form-control input-md'></td><td><input  name='header1"+i+"' type='text' placeholder='Header1'  class='form-control input-md'></td><td><input  name='header2"+i+"' type='text' placeholder='Header2'  class='form-control input-md'></td><td><input  name='header3"+i+"' type='text' placeholder='Header3'  class='form-control input-md'></td> <td><input  name='header4"+i+"' type='text' placeholder='Header4'  class='form-control input-md'></td> <td><input  name='header5"+i+"' type='text' placeholder='Header5'  class='form-control input-md'></td> <td><input  name='header6"+i+"' type='text' placeholder='Header6'  class='form-control input-md'></td> <td><input  name='header7"+i+"' type='text' placeholder='Header7'  class='form-control input-md'></td> <td><input  name='header8"+i+"' type='text' placeholder='Header8'  class='form-control input-md'></td> <td><input  name='header9"+i+"' type='text' placeholder='Header9'  class='form-control input-md'></td> <td><input  name='header10"+i+"' type='text' placeholder='Header10'  class='form-control input-md'></td>");
 $('#tab_logic').append('<tr id="addr'+(i+1)+'"></tr>');
 i++; 
});
$("#delete_row").click(function(){
	 if(i>1){
	 $("#addr"+(i-1)).html('');
	 i--;
	 }
});


$("a[href=#save_rows_map]").click(function(){
	
	$("input[name=savemap]").val("Save");
	 $(this).parents("form").submit();
});

$("a[href=#save_continue_rows_map]").click(function(){
	
	$("input[name=savemap]").val("SaveContinue");
	 $(this).parents("form").submit();
});


$("button[name=save_rows_str]").click(function(){
	 var count = $('#tab_logic_strucure_body').children().length;
	 $("input[name=noStrucure]").val(count);
	 $("input[name=savestructure]").val("Save");
	// $(this).parents("form").submit();
});

$("button[name=save_rows_str_verify]").click(function(){
	 var count = $('#tab_logic_verify_body').children().length;
	 $("input[name=noVerifyStrucure]").val(count);
	 $("input[name=saveverifystructure]").val("Save");
	// $(this).parents("form").submit();
});


$("a[href=#save_continue_rows_str]").click(function(){
	 var count = $('#tab_logic_strucure_body').children().length;
	 $("input[name=noStrucure]").val(count);
	 $("input[name=savestructure]").val("SaveContinue");
	 $(this).parents("form").submit();
});
$("a[href=#save_rows_strData]").click(function(){
	 var count = $('#tab_logic_strucure_data_body').children().length;
	 $("input[name=noStrucuredata]").val(count);
	 $("input[name=savestructuredata]").val("Save");
	if($("select[name=dtestcaseid1]").val() != "Select"){
		 $(this).parents("form").submit();
	 }
	
});

$("a[href=#add_rows_strData]").click(function(){
	 var count = $('#tab_logic_strucure_data_body').children().length;
	 $("input[name=noStrucuredata]").val(count);
	 $("select[name=dtestcaseid1]").val("Select");
	 $("input[name=savestructuredata]").val("addnew");
	 $(this).parents("form").submit();
	// $("select[name=transDatainputfile]").click();
	/*$("input[name=dtransactiontype1]").val("");
	for(i=(count-2) ; i<=(count-2) ; i++){
		var datavalname = "ddatavalue" + i;
    	 $("input[name="+datavalname+"]").val("");
	}*/
	
});

$("a[href=#edit_rows_strData]").click(function(){
	 var count = $('#tab_logic_strucure_data_body').children().length;
	
	 
	 
});
$("a[href=#save_rows_back_strData]").click(function(){
	
	$("a[href=#scenario]").click();
	 
	 
});



$("select[name=dtestcaseid1]").on('change',function(){
	var count = $('#tab_logic_strucure_data_body').children().length;
	 $("input[name=noStrucuredata]").val(count);
	 $("input[name=savestructuredata]").val("fetch");
	 var selectedval= $("select[name=dtestcaseid1]").val();
	
	 if($("select[name=dtestcaseid1]").val() !="Select"){
		 $("input[name=strTestCaseID]").val($("select[name=dtestcaseid1]").val());
		 $(this).parents("form").submit();
	 }
	
	
});

/*function fetchtxnList(){
	var count = $('#tab_logic_strucure_data_body').children().length;
	 $("input[name=noStrucuredata]").val(count);
	 $("input[name=savestructuredata]").val("fetch");
	 var selectedval= $("select[name=dtestcaseid1]").val();
	
	 if($("select[name=dtestcaseid1]").val() !="Select"){
		 $("input[name=strTestCaseID]").val($("select[name=dtestcaseid1]").val());
		 $(this).parents("form").submit();
	 }
}*/
$("a[title=save_continue_rows]").click(function(){
	$("a[href=#cstructure]").click();
});

$("a[title=save_rows_back_createStr]").click(function(){
	$("a[href=#cstructure]").click();
});
/*$("select[name=transinputfile]").click(function(){
	 var count = $('#tab_logic_strucure_body').children().length;
	 $("input[name=noStrucure]").val(count);
	 $(this).parents("form").submit();
	
});*/
$("select[name=transDatainputfile]").on('change',function(){
	 var count = $('#tab_logic_strucure_data_body').children().length;
	 $("input[name=noStrucuredata]").val(count);
	 $("input[name=savestructuredata]").val("fetch");
	 if($("select[name=transDatainputfile]").val() !="Select"){
	 $(this).parents("form").submit();
	 
	 }
	
});

$("button[name=btnfile]").click(function(){
	$("form[name=structuploaddownloadform]").submit();
});


/*function fetchStructData(obj){
	var count = $('#tab_logic_strucure_data_body').children().length;
	 $("input[name=noStrucuredata]").val(count);
	 $("input[name=savestructuredata]").val("fetch");
	 if($("select[name=transDatainputfile]").val() !="Select"){
		 obj.parents("form").submit();
	 }
	
}*/


var m =  2;
$("#add_row_structure").click(function(){
	var count =$('#tab_logic_strucure_body').children().length;
	if(count>=m){
		m = count+1;
	}

	 $('#tab_logic_strucure_body').append('<tr id="addrstructure'+(m)+'"></tr>');
	// $('#tab_logic_strucure_body').append($('#addrstructure'+m));
	// $('#addrstructure'+m).html('<input type="hidden" name="indexid" id="indexid" value="'+(m)+'"/>'+"<td><a href='#' class='up'>Up</a></td><td> <a href='#' class='down'>Down</a></td><td><select class='form-control' placeholder='Select Execution Flag' name='structexeflag"+(m)+"'><option value='Y'>Y</option><option value='N'>N</option></select></td><td><select class='strucureaction form-control' placeholder='Select Action' name='structaction"+(m)+"' onchange='onchangeCapture(this.value, this.name);'><option value='Read'>Read</option><option value='I'>I</option><option value='NC'>NC</option><option value='V'>V</option><option value='Write'>Write</option><option value='Capture'>Capture</option></select> </td>	<td><input  name='structlogic"+m+"' type='text' placeholder='LogicalName'  class='form-control input-md' value=''></td> <td><input  name='structcontol"+m+"' type='text' placeholder='ControlName'  class='form-control input-md' value=''></td><td><select class='form-control' placeholder='Select ControlType' name='structcontrolType"+m+"'> <option value='WebEdit'>WebEdit</option><option value='WebList'>WebList</option><option value='WebLink'>WebLink</option><option value='WebButton'>WebButton</option> <option value='CheckBox'>CheckBox</option> <option value='Radio'>Radio</option> <option value='WebElement'>WebElement</option><option value='WebTable'>WebTable</option> <option value='Browser'>Browser</option><option value='Alert'>Alert</option><option value='Robot'>Robot</option><option value='Window'>Window</option><option value='URL'>URL</option> <option value='JSScript'>JSScript</option><option value='ListBox'>ListBox</option><option value='Slider'>Slider</option><option value='MaskedInputDate'>MaskedInputDate</option><option value='ActionClick'>ActionClick</option><option value='ActionMouseOver'>ActionMouseOver</option><option value='IFrame'>IFrame</option><option value='Wait'>Wait</option></select></td><td><select class='form-control' placeholder='Select ControlID' name='structcontrolid"+m+"'><option value=''>Select</option><option value='Id'>Id</option><option value='Name'>Name</option><option value='TagName'>TagName</option><option value='ClassName'>ClassName</option> <option value='XPath'>XPath</option><option value='AjaxPath'>AjaxPath</option><option value='LinkText'>LinkText</option> <option value='HTMLID'>HTMLID</option> <option value='CSSSelector'>CSSSelector</option><option value='Id_p'>Id_p</option> <option value='XPath_p'>XPath_p</option></select></td><td><input  name='structimage1"+m+"' type='text' placeholder='ImageType'  class='form-control input-md' value=''> </td><td><input  name='structindex"+m+"' type='text' placeholder='Index'  class='form-control input-md' value=''> 								</td> 								<td><input  name='structdyindex"+m+"' type='text' placeholder='DynamicIndex'  class='form-control input-md' value=''></td><td><input  name='structcolname"+m+"' type='text' placeholder='ColumnName'  class='form-control input-md' value=''></td><td style='display: none'><input style='display: none' name='structrow"+m+"' type='text' placeholder='RowNo'  class='form-control input-md' value=''></td><td style='display: none'><input name='structcol"+m+"' type='text' placeholder='ColumnNo'  class='form-control input-md' value=''></td>");
	 $('#addrstructure'+m).html('<input type="hidden" name="indexid" id="indexid" value="'+(m)+'"/>'+"<td> <a href='#' class='down'>Down</a></td><td><select class='form-control' placeholder='Select Execution Flag' name='structexeflag"+(m)+"'><option value='Y'>Y</option><option value='N'>N</option></select></td><td><select class='strucureaction form-control' placeholder='Select Action' name='structaction"+(m)+"' onchange='onchangeCapture(this.value, this.name);'><option value='Read'>Read</option><option value='I'>I</option><option value='NC'>NC</option><option value='V'>V</option><option value='Write'>Write</option><option value='Capture'>Capture</option></select> </td>	<td><input  name='structlogic"+m+"' type='text' placeholder='LogicalName'  class='form-control input-md' value=''></td> <td><input  name='structcontol"+m+"' type='text' placeholder='ControlName'  class='form-control input-md' value=''></td><td><select class='form-control' placeholder='Select ControlType' name='structcontrolType"+m+"'> <option value='WebEdit'>WebEdit</option><option value='WebList'>WebList</option><option value='WebLink'>WebLink</option><option value='WebButton'>WebButton</option> <option value='CheckBox'>CheckBox</option> <option value='Radio'>Radio</option> <option value='WebElement'>WebElement</option><option value='WebTable'>WebTable</option> <option value='Browser'>Browser</option><option value='Alert'>Alert</option><option value='Robot'>Robot</option><option value='Window'>Window</option><option value='URL'>URL</option> <option value='JSScript'>JSScript</option><option value='ListBox'>ListBox</option><option value='Slider'>Slider</option><option value='MaskedInputDate'>MaskedInputDate</option><option value='ActionClick'>ActionClick</option><option value='ActionMouseOver'>ActionMouseOver</option><option value='IFrame'>IFrame</option><option value='Wait'>Wait</option></select></td><td><select class='form-control' placeholder='Select ControlID' name='structcontrolid"+m+"'><option value=''>Select</option><option value='Id'>Id</option><option value='Name'>Name</option><option value='TagName'>TagName</option><option value='ClassName'>ClassName</option> <option value='XPath'>XPath</option><option value='AjaxPath'>AjaxPath</option><option value='LinkText'>LinkText</option> <option value='HTMLID'>HTMLID</option> <option value='CSSSelector'>CSSSelector</option><option value='Id_p'>Id_p</option> <option value='XPath_p'>XPath_p</option></select></td><td><input  name='structimage1"+m+"' type='text' placeholder='ImageType'  class='form-control input-md' value=''> </td><td><input  name='structindex"+m+"' type='text' placeholder='Index'  class='form-control input-md' value=''> 								</td> 								<td><input  name='structdyindex"+m+"' type='text' placeholder='DynamicIndex'  class='form-control input-md' value=''></td><td><input  name='structcolname"+m+"' type='text' placeholder='ColumnName'  class='form-control input-md' value=''></td><td style='display: none'><input style='display: none' name='structrow"+m+"' type='text' placeholder='RowNo'  class='form-control input-md' value=''></td><td style='display: none'><input name='structcol"+m+"' type='text' placeholder='ColumnNo'  class='form-control input-md' value=''></td>");
	 
	 
	 m++; 
	});




	$("#delete_row_strucure").click(function(){
		var count =  $('#tab_logic_strucure_body').children().length;
		 if(count>0){
			// $( "#tab_logic_strucure_body>tr:last" ).html('');
			// $( "#tab_logic_strucure_body>tr:last" ).remove('');
			 
				
					try {
					///	var table = document.getElementById(tableID);
						//var rowCount = table.rows.length;

						$('.checker:checkbox:checked').parents("tr").remove();
						// m--;
					} catch (e) {
						alert(e);
					}
			
			
		
		 }
	});
	
	
	var z =  2;
	$("#add_row_verifystructure").click(function(){
		var count =$('#tab_logic_verify_body').children().length;
		if(count>=z){
			z = count+1;
		}

		 $('#tab_logic_verify_body').append('<tr id="addrverify'+(z)+'"></tr>');
		// $('#tab_logic_strucure_body').append($('#addrstructure'+m));
		 $('#addrverify'+z).html('<input type="hidden" name="indexid" id="indexid" value="'+(z)+'"/>'+"<td><a href='#' class='up'>Up</a></td><td> <a href='#' class='down'>Down</a></td><td><select class='form-control' placeholder='Select TableType' id='tabletype"+z+"' name='tabletype"+z+"'><option value='ControlNames'>ControlNames</option><option value='HTML5'>HTML5</option><option value='Uniform'>Uniform</option><option value='NonUniform'>NonUniform</option></select></td><td><input  name='tableid"+(z)+"' type='text' placeholder='TableID'  class='form-control input-md' value=''> </td>	<td><input  name='tableidtype"+(z)+"' type='text' placeholder='TableIDType'  class='form-control input-md' value=''></td> <td><input  name='startrow"+z+"' type='text' placeholder='StartRow'  class='form-control input-md' value=''></td><td><input  name='endrow"+z+"' type='text' placeholder='EndRow'  class='form-control input-md' value=''></td><td><input  name='columnname"+z+"' type='text' placeholder='ColumnName'  class='form-control input-md' value=''></td><td><input  name='row"+z+"' type='text' placeholder='Row'  class='form-control input-md' value=''> </td><td><input  name='column"+z+"' type='text' placeholder='Column'  class='form-control input-md' value=''></td><td><input  name='controlname"+z+"' type='text' placeholder='ControlName'  class='form-control input-md' value=''></td><td><select class='form-control' placeholder='Select ControlType' name='controltype"+z+"'> <option value='WebEdit'>WebEdit</option><option value='WebList'>WebList</option><option value='WebLink'>WebLink</option><option value='WebButton'>WebButton</option> <option value='CheckBox'>CheckBox</option> <option value='Radio'>Radio</option> <option value='WebElement'>WebElement</option><option value='WebTable'>WebTable</option> <option value='Browser'>Browser</option><option value='Alert'>Alert</option><option value='Robot'>Robot</option><option value='Window'>Window</option><option value='URL'>URL</option> <option value='JSScript'>JSScript</option><option value='ListBox'>ListBox</option><option value='Slider'>Slider</option><option value='MaskedInputDate'>MaskedInputDate</option><option value='ActionClick'>ActionClick</option><option value='ActionMouseOver'>ActionMouseOver</option><option value='IFrame'>IFrame</option><option value='Wait'>Wait</option></select></td><td><select class='form-control' placeholder='Select ControlID' name='controlid"+z+"'><option value=''>Select</option><option value='Id'>Id</option><option value='Name'>Name</option><option value='TagName'>TagName</option><option value='ClassName'>ClassName</option> <option value='XPath'>XPath</option><option value='AjaxPath'>AjaxPath</option><option value='LinkText'>LinkText</option> <option value='HTMLID'>HTMLID</option> <option value='CSSSelector'>CSSSelector</option><option value='Id_p'>Id_p</option> <option value='XPath_p'>XPath_p</option></select></td><td><input  name='controlindex"+z+"' type='text' placeholder='ControlIndex'  class='form-control input-md' value=''></td><td style='display: none'><input style='display: none' name='verificationtype"+z+"' type='text' placeholder='VerificationType'  class='form-control input-md' value=''></td><td style='display: none'><input name='Order"+z+"' type='text' placeholder='Order'  class='form-control input-md' value=''></td>");
		 
		 
		 
		 z++; 
		});




		$("#delete_row_verifystrucure").click(function(){
			var count =  $('#tab_logic_verify_body').children().length;
			 if(count>0){
			 $( "#tab_logic_verify_body>tr:last" ).html('');
			 $( "#tab_logic_verify_body>tr:last" ).remove('');
			// $("#addrverify"+(z-1)).html('');
			// $("#addrverify"+(z-1)).remove();
			 z--;
			 }
			 
			
		});
	
	

	$("input[name=executebtn]").click(function(){
		 $(this).parents("form").submit();
	});
	

	
	
	
	

	$("button[name=copyproject]").click(function(){
		
		var parentObj = $(this).parents("tr");
		var  spantext= parentObj.find( "span" ).text();
		$("input[name=directoryname]").val(spantext);
		$(this).parents("form").submit();
	});
	
	$("button[name=deleteproject]").click(function(){
			
			var parentObj = $(this).parents("tr");
			var  spantext= parentObj.find( "span" ).text();
			$("input[name=directoryname]").val(spantext);
			$(this).parents("form").submit();
		});

	$("button[name=createverifyTrans]").click(function(){
		

		$("input[name=verifytransactionaction]").val("");

	});
	  
	  /**
		 * Pagination Table start
		 */
		
		$.fn.pageMe = function(opts){
		    var $this = this,
		        defaults = {
		            perPage: 7,
		            showPrevNext: false,
		            hidePageNumbers: false
		        },
		        settings = $.extend(defaults, opts);
		    
		    var listElement = $this;
		    var perPage = settings.perPage; 
		    var children = listElement.children();
		    var pager = $('.pager');
		   // $('.results tbody tr[visible="true"]')
		    if (typeof settings.childSelector!="undefined") {
		        children = listElement.find(settings.childSelector);
		    }
		    
		    if (typeof settings.pagerSelector!="undefined") {
		        pager = $(settings.pagerSelector);
		    }
		    
		    var numItems = children.size();
		    var numPages = Math.ceil(numItems/perPage);

		    pager.data("curr",0);
		    
		    if (settings.showPrevNext){
		        $('<li><a href="#" class="prev_link">«</a></li>').appendTo(pager);
		    }
		    
		    var curr = 0;
		    while(numPages > curr && (settings.hidePageNumbers==false)){
		        $('<li><a href="#" class="page_link">'+(curr+1)+'</a></li>').appendTo(pager);
		        curr++;
		    }
		    
		    if (settings.showPrevNext){
		        $('<li><a href="#" class="next_link">»</a></li>').appendTo(pager);
		    }
		    
		    pager.find('.page_link:first').addClass('active');
		    pager.find('.prev_link').hide();
		    if (numPages<=1) {
		        pager.find('.next_link').hide();
		    }
		  	pager.children().eq(1).addClass("active");
		    
		    children.hide();
		    children.slice(0, perPage).show();
		    
		    pager.find('li .page_link').click(function(){
		        var clickedPage = $(this).html().valueOf()-1;
		        goTo(clickedPage,perPage);
		        return false;
		    });
		    pager.find('li .prev_link').click(function(){
		        previous();
		        return false;
		    });
		    pager.find('li .next_link').click(function(){
		        next();
		        return false;
		    });
		    
		    function previous(){
		        var goToPage = parseInt(pager.data("curr")) - 1;
		        goTo(goToPage);
		    }
		     
		    function next(){
		        goToPage = parseInt(pager.data("curr")) + 1;
		        goTo(goToPage);
		    }
		    
		    function goTo(page){
		        var startAt = page * perPage,
		            endOn = startAt + perPage;
		        
		        children.css('display','none').slice(startAt, endOn).show();
		        
		        if (page>=1) {
		            pager.find('.prev_link').show();
		        }
		        else {
		            pager.find('.prev_link').hide();
		        }
		        
		        if (page<(numPages-1)) {
		            pager.find('.next_link').show();
		        }
		        else {
		            pager.find('.next_link').hide();
		        }
		        
		        pager.data("curr",page);
		      	pager.children().removeClass("active");
		        pager.children().eq(page+1).addClass("active");
		    
		    }
		};

		
		
		/**
		 * Pagination Table end
		 */
		
		
		$(".clickablename").click(function(){
			$("input[name=projectaction]").val("continue");
			$("input[name=directoryname]").val($(this).children()[0].innerHTML);
			$(this).parents("form").submit();

	});
		
		$(".clickabletxnname").click(function(){
			$("input[name=transactionaction]").val("continue");
			//alert("hi structure");
			$("input[name=transactionname]").val($(this).children()[0].innerHTML);
			$(this).parents("form").submit();

	});
		
		
	$(".clickabletVerifyname").click(function(){
			$("input[name=verifytransactionaction]").val("continue");
			//alert("hi verify" +$(this).children()[0].innerHTML);
			$("input[name=verifytransactionname]").val($(this).children()[0].innerHTML);
			$(this).parents("form").submit();

	});
	
	
function onchangeCapture(value ,name) {
		//$("input[name=verifytransactionaction]").val("continue");
	
		//$("input[name=verifytransactionname]").val($(this).children()[0].innerHTML);
		//$(this).parents("form").submit();
	
		var projectname = $("input[name=parentprojectname]").val();
		
		 // var selectval=  $(".strucureaction")[0].value;
		  
		  var code = name.substring(12,(name.length));
		  
		  
			if(value=="Capture"){
			
				$.get( "itafpack.do", {actiontype: "verificationlist" ,project:projectname,index:code}, function(responseText) {
		        
		        	var reqid ="addrstructure"+code;
		        	
		        	$("tr[id=" + reqid + "]").children('td')[4].innerHTML = responseText;
		        	
		        	
					//$("div[id=iframedashboard]").html(responseText);
		        	})
		        	  .done(function(data) {
		        	   // alert( "second success" );
		        	  })
		        	  .fail(function() {
		        	   // alert( "error" );
		        	  })
		        	  .always(function() {
		        	   // alert( "finished" );
		        	});
			}

  }
	
	function onverificationchange(value ,name){ 
		
		 var selectval= value;
		 var code =  name.substring(11,name.length);
		
		 $("input[name=" + "structcontol"+code+ "]").val(selectval)
		  $("select[name=" + "structcontrolType"+code+ "]").val("Select")
		   $("select[name=" + "structcontrolid"+code+ "]").val("")
		    $("input[name=" + "structimage"+code+ "]").val("")
		     $("input[name=" + "structindex"+code+ "]").val("")
		      $("input[name=" + "structdyindex"+code+ "]").val("")
		       $("input[name=" + "structcolname"+code+ "]").val("")
		       
		    
	}
	

	$("a[href=#objDtl]").click(function(){
			$("input[name=transactionaction]").val("continue");
			$("input[name=transactionname]").val($(this).parents("tr").children('td')[1].children[0].textContent);
			$(this).parents("form").submit();

	});

	$("a[href=#objverifyDtl]").click(function(){
		$("input[name=verifytransactionaction]").val("continue");
		$("input[name=verifytransactionname]").val($(this).parents("tr").children('td')[1].children[0].textContent);
		$(this).parents("form").submit();

});
	function showdialog(id){
			var  currentrow =$(".clickabletestid")[id-1];
			$("input[name=transactionname]").val(currentrow.children[3].innerHTML);
			$("input[name = testcaseid]").val(currentrow.children[3].innerHTML);
			$("input[name = groupname]").val(currentrow.children[2].innerHTML);
			$("input[name = testdesc]").val(currentrow.children[4].innerHTML);
			$("select[name = execflag]").val(currentrow.children[5].innerHTML);
			if(currentrow.children[3].innerHTML!=""){
				$("input[name=rowindex]").val(currentrow.children[0].value);
				$("input[name=updateflag]").val("true");
	        }else{
	        	$("input[name=rowindex]").val("");
				$("input[name=updateflag]").val("false");
	        }
			$("a[name=#add_mainControllerData]").click();
			
		}
	

	