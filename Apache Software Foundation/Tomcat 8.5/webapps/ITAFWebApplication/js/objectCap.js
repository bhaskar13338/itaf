var arrData;
	$(document)
			.ready(
					function() {
//						$('#btn').on('click', function() {
//							// alert('clicked');  
//							alert($('#urltxt').val());
//							$('#frame').attr('src', $('#urltxt').val());
//						});
						$('#start')
								.on(
										'click',
										function() {

											$
													.ajax({
														type : 'POST',
														url : '/ITAFWebApplication/Seleniumservice/resources/startStopObjectCapture',
														dataType : 'text',
														data : {
															paramValue : $(
																	'#start')
																	.val(),
															url : $('#urltxt')
																	.val()
														},
														contentType : 'application/x-www-form-urlencoded; charset=utf-8',
														success : function(
																response) {

															console
																	.log(response);
															//var data = JSON.parse(response);

														},
														error : function(error) {
															console.log(error);
														}
													});
										});

						$('#stop')
								.on(
										'click',
										function() {

											$
													.ajax({
														type : 'POST',
														url : '/ITAFWebApplication/Seleniumservice/resources/startStopObjectCapture',
														dataType : 'text',
														data : {
															paramValue : $(
																	'#stop')
																	.val(),
															url : $('#urltxt')
																	.val()
														},
														contentType : 'application/x-www-form-urlencoded; charset=utf-8',
														success : function(
																response) {

															//alert(response);
															arrData = JSON
																	.parse(response);
															for (var i = 0; i < arrData.elementsList.length; i++) {
																var counter = arrData.elementsList[i];
																//console.log(counter.counter_name);
																//alert(counter.elementName);

																addRow(
																		"dataTable",
																		JSON
																				.stringify(counter));
															}
														},
														error : function(error) {
															console.log(error);
														}
													});
										});
						var sorter = $('#dataTable')
								.rowSorter(
										{
											onDragStart : function(tbody, row,
													index) {
												//log('index: ' + index);
												console
														.log('onDragStart: active row\'s index is '
																+ index);
											},
											handler : "td.sort-handler",
											onDrop : function(tbody, row,
													new_index, old_index) {
												//log('old_index: ' + old_index + ', new_index: ' + new_index);
												var temp = arrData.elementsList[old_index];
												arrData.elementsList.splice(
														old_index, 1);
												arrData.elementsList.splice(
														new_index, 0, temp);
												console
														.log('onDrop: row moved from '
																+ old_index
																+ ' to '
																+ new_index);

											}
					});
			});		

	function insertScript(doc, target, src, callback) {
		var s = doc.createElement("script");
		s.type = "text/javascript";
		if (callback) {
			if (s.readyState) { //IE
				s.onreadystatechange = function() {
					if (s.readyState == "loaded" || s.readyState == "complete") {
						s.onreadystatechange = null;
						callback();
					}
				};
			} else { //Others
				s.onload = function() {
					callback();
				};
			}
		}
		s.src = src;
		target.appendChild(s);
	}
	function addRow(tableId, mess) {

		var table = document.getElementById(tableId);
		var json = JSON.parse(mess);
		var split;
		var rowCount = table.rows.length;
		var row = table.insertRow(rowCount);

		var cell1 = row.insertCell(0);
		var element1 = document.createElement("input");
		element1.type = "checkbox";
		element1.name = "chkbox[]";
		//cell1.className = "select_handle";
		cell1.appendChild(element1);

		var cell2 = row.insertCell(1);
		cell2.className = "sort-handler form-control";
		cell2.innerHTML = rowCount + 1;

		var cell3 = row.insertCell(2);
		var element2 = document.createElement("input");
		element2.type = "text";
		element2.name = "txtbox[]";
		//element2.style = "width:200px"
		element2.className = "form-control";
		element2.value = (json.logicalName).split(',')[0];
		cell3.appendChild(element2);

		var cell4 = row.insertCell(3);
		var element3 = document.createElement("select");
		element3.id = "controlID_" + rowCount;
		element3.name = "txtboxid";
		//element3.style = "width:200px";
		element3.className = "form-control";
		element3.onchange = function() {
			getControlValue(this.value, this.id, tableId);
		};
		//element3.value=json.controlID;
		cell4.appendChild(element3);

		if (!isEmpty(json.elementId)) {
			var option1 = document.createElement("option");
			option1.value = "id";
			option1.selected = "";
			option1.innerHTML = "id";
			element3.appendChild(option1);
		}
		if (!isEmpty(json.elementName)) {
			var option2 = document.createElement("option");
			option2.value = "name";
			option2.selected = "";
			option2.innerHTML = "name";
			element3.appendChild(option2);
		}
		if (!isEmpty(json.xPathValue)) {
			var option3 = document.createElement("option");
			option3.value = "xPath";
			option3.selected = "";
			option3.innerHTML = "xPath";
			element3.appendChild(option3);
		}
		var cell5 = row.insertCell(4);
		var element4 = document.createElement("input");
		element4.type = "text";
		element4.id = "controlName_" + rowCount;
		//element4.style = "width:200px"
		element4.className = "form-control";
		element4.value = (json.elementId).split(',')[0];
		cell5.appendChild(element4);

		var cell6 = row.insertCell(5);
		var element5 = document.createElement("input");
		element5.type = "text";
		element5.id = "controlValue_" + rowCount;
		//element5.style = "width:200px"
		element2.className = "form-control";
		element5.value = json.elementValue;
		cell6.appendChild(element5);

		var cell7 = row.insertCell(6);
		var element6 = document.createElement("input");
		element6.type = "text";
		element6.id = "controlPath_" + rowCount;
		//element6.style = "width:200px"
		element2.className = "form-control";
		if (!isEmpty(json.elementId)) {

			element6.value = (json.elementId).split(',')[1];

		} else if (!isEmpty(json.elementName)) {
			element6.value = (json.elementName).split(',')[1];
		} else if (!isEmpty(json.xPathValue)) {
			element6.value = (json.xPathValue);
		}
		cell7.appendChild(element6);

		var cell8 = row.insertCell(7);
		var element7 = document.createElement("input");
		element7.type = "text";
		element7.id = "controlType_" + rowCount;
		//element7.style = "width:200px"
		element2.className = "form-control";
		element7.value = json.controlType;
		cell8.appendChild(element7);
	}

	function getControlValue(val, id, tableId) {
		var rows = document.getElementById(tableId).getElementsByTagName(
				'tbody')[0].getElementsByTagName('tr');
		var index;
		var drpValue = val;
		for (i = 0; i < rows.length; i++) {
			rows[i].onchange = function(val) {
				//alert(this.rowIndex);
				index = this.rowIndex;
				if (drpValue == 'name') {
					document.getElementById("controlPath_" + id.split('_')[1]).value = (arrData.elementsList[index].elementName)
							.split(',')[1];
					arrData.elementsList[index].controlID = drpValue;
				}
				if (drpValue == 'id') {
					document.getElementById("controlPath_" + id.split('_')[1]).value = (arrData.elementsList[index].elementId)
							.split(',')[1];
					arrData.elementsList[index].controlID = drpValue;
				}
				if (drpValue == 'xPath') {
					document.getElementById("controlPath_" + id.split('_')[1]).value = arrData.elementsList[index].xPathValue;
					arrData.elementsList[index].controlID = drpValue;
				}
			}

		}
	}
	function deleteRow(tableID) {
		try {
			var table = document.getElementById(tableID);
			var rowCount = table.rows.length;

			for (var i = 0; i < rowCount; i++) {
				var row = table.rows[i];
				//var chkbox = row.cells[0].childNodes[0];
				var chkbox = row.querySelector('input[type=checkbox]');
				if (null != chkbox && true == chkbox.checked) {
					table.deleteRow(i);
					arrData.elementsList.splice(i, 1);
					rowCount--;
					i--;
				}
			}
		} catch (e) {
			alert(e);
		}
	}
	function inject_Script() {
		var http = new getXMLHttpRequestObject();
		var url = "/ITAFWebApplication/Seleniumservice/resources/injectScript";
		var parameters = "";
		http.open("POST", url, true);

		// Send the proper header information along with the request
		http.setRequestHeader("Content-type",
				"application/x-www-form-urlencoded");
		http.setRequestHeader("Content-length", parameters.length);
		http.setRequestHeader("Connection", "close");

		http.onreadystatechange = function() {// Handler function for call
			// back on state change.
			if (http.readyState == 4) {
				//alert(http.responseText);
			}
		}
		http.send("name=" + parameters);
	}

	function save_Data() {
		var http = new getXMLHttpRequestObject();
		var url = "/ITAFWebApplication/Seleniumservice/resources/updateCSV";
		//arrData=JSON.parse(arrData);
		var str = JSON.stringify(arrData);
		var parameters = str;
		var transactFilePathName = $('#dropdown').val();
		if(isEmpty(transactFilePathName)){
			alert("Please Select Transaction File Name");
			return false;
		}
		var projectName=$('#projectname').val();
		//alert(transactFilePathName);
		//alert(projectName);
		http.open("POST", url, true);

		// Send the proper header information along with the request
		http.setRequestHeader("Content-type",
				"application/x-www-form-urlencoded");
		http.setRequestHeader("Content-length", parameters.length);
		http.setRequestHeader("Connection", "close");

		http.onreadystatechange = function() {// Handler function for call
			// back on state change.
			if (http.readyState == 4) {
				//alert(http.responseText);
			}
		}
		http.send("name=" + parameters + "&" +"projectName="+projectName+ "&"+"fileName="
				+ transactFilePathName);
		arrData.elementsList = new Array();
		$("#dataTable").empty();
	}
	function getXMLHttpRequestObject() {
		var xmlhttp;
		/*
		 * @cc_on @if (@_jscript_version >= 5) try { xmlhttp = new
		 * ActiveXObject("Msxml2.XMLHTTP"); } catch (e) { try { xmlhttp = new
		 * ActiveXObject("Microsoft.XMLHTTP"); } catch (E) { xmlhttp = false; } }
		 * @else xmlhttp = false; @end @
		 */
		if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
			try {
				xmlhttp = new XMLHttpRequest();
			} catch (e) {
				xmlhttp = false;
			}
		}
		return xmlhttp;
	}

	function isEmpty(val) {
		return (val === undefined || val == null || val.length <= 0 || val=='Select') ? true
				: false;
	}	