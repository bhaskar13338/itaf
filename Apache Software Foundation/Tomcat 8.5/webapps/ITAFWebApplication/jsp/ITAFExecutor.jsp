
<%@page import="com.majesco.itaf.dao.ProjectPack"%>
<%@page import="java.util.List"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ITAF Home</title>
<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">

<link rel="stylesheet" type="text/css" href="./css/demo3/try.css">
<link
	href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css"
	rel="stylesheet">
<link href="//www.jqueryscript.net/css/jquerysctipttop.css"
	rel="stylesheet" type="text/css">
<style type="text/css">
.modal {
	text-align: center;
	padding: 0 !important;
}

.modal:before {
	content: '';
	display: inline-block;
	height: 100%;
	vertical-align: middle;
	margin-right: -4px;
}

.modal-dialog {
	display: inline-block;
	text-align: left;
	vertical-align: middle;
}
</style>
</head>
<body>

	
	<!-- Top Menu start -->
	<div id="wrapper">
		<jsp:include page="menuHeader.jsp"></jsp:include>
		<div id="executeMain">
			<form action="./itafexecute.do" method="post" data-toggle="validator"
				role="executionform">

				<input type="hidden" name="executeAction" id="executeAction" />

				<div class="padding-top-15">
					<div class="clearfix">
						<div class="col-md-12 column" style="padding-left: 100px;">
							<div  id ="error" name="error" class="alert alert-danger" style="display: none">
								    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
								    <strong>Error!</strong> iTaf Execution failed due to some error.
							 </div>	
							 <div id ="success" name="success" class="alert  alert-success" style="display: none">
							    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							    <strong>Success!</strong> iTaf Execution successful
							 </div>
							  <div id ="warning" name="warning" class="alert  alert-warning" style="display: none">
							    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							    <strong>Warning!</strong> Please select project for execution
							 </div>	
								 
						</div>
						<div class="col-md-12  form-group" style="padding-left: 100px;">
							<label class="control-label col-sm-4" for="exeproname"
								style="width: 10%">Project Name</label>
							
							<c:if test="${projectList != null && fn:length(projectList) gt 0}">
								<div class=" col-xs-4 padding-bottom-5">
								<select class="form-control" placeholder="Select Project"
									id="exeproject" name="exeproject">
									<option value="Select" selected>Select</option>
								   <c:forEach items="${projectList}" var="item" varStatus="status">
								   		<option value='<c:out value='${item.projectname}'></c:out>'
								   		<c:if test='${reqprojectname!=null && fn:containsIgnoreCase(reqprojectname, item.projectname)}'>selected</c:if>
										><c:out value='${item.projectname}'></c:out></option>
								   </c:forEach>
								</select>
							</div>
							</c:if>
							
							
							

							<div class="form-group">
								
								<c:if test="${menuItem != null &&fn:containsIgnoreCase(menuItem, '#execution')}">
								<div id="executeDiv" class=" col-xs-12 padding-bottom-5">
									<!--<input type="button" id="executebtn" name="executebtn" value="Submit"/>-->
									<a href="#executebtn"
										class="btn btn-default pull-left margin-15 button-color"
										role="button" data-toggle="modal" data-backdrop="static"
										data-keyboard="false" onclick="opendialog();">Execute</a> <a
										href="#viewdashbordbtn"
										class="btn btn-default pull-left margin-15 button-color"
										role="button" data-toggle="modal">View Dashboard</a> <a
										href="#viewverifydbtn"
										class="btn btn-default pull-left margin-15 button-color"
										role="button" data-toggle="modal">Verification Chart</a>
								</div>
								
								</c:if>
								
								<c:if test="${menuItem != null &&fn:containsIgnoreCase(menuItem, '#log')}">
									<div id="logDiv" class="col-xs-12 padding-bottom-5">
										<!--<input type="button" id="executebtn" name="executebtn" value="Submit"/>-->
										<a href="#logfilebtn" name="logfilebtn"
											class="btn btn-default pull-left margin-15 button-color">Show
											log</a>
									</div>
								
								</c:if>
							</div>


						</div>
						<div class="col-md-12 column">
							<div class="row">
								<div id="iframepiechart" class="col-md-4">

									<!-- 	<iframe  src="" id='inneriframe' scrolling=no style="overflow:hidden;height:100vh;width:100vw;padding-left:150px;" height="100%" width="100%" frameBorder="0"></iframe>-->
								</div>
								<div id="iframedashboard" class="col-md-8">

									<!-- 	<iframe  src="" id='inneriframe' scrolling=no style="overflow:hidden;height:100vh;width:100vw;padding-left:150px;" height="100%" width="100%" frameBorder="0"></iframe>-->
								</div>
							</div>
							<div id="iframeextentdashboard" class="col-md-8">

									<!-- 	<iframe  src="" id='inneriframe' scrolling=no style="overflow:hidden;height:100vh;width:100vw;padding-left:150px;" height="100%" width="100%" frameBorder="0"></iframe>-->
							</div>
						</div>

					</div>
				</div>


				<script src="./js/jquery.min.js"></script>
				<!-- Bootstrap Core JavaScript -->
				<script src="./js/bootstrap.min.js"></script>
				<script src="./js/demo3/executetry.js"></script>
				<script src="./js/demo3/bootstrap-progressbar.js"></script>
				<script src="./js/loader.js"></script>
				<script type="text/javascript">
					function opendialog() {
						if ($("select[name=exeproject]").val() == "Select") {
							// $('#executebtn').modal('hide');

							$('#executebtn').modal({
								show : 'false'
							});

							$("a[href=#viewdashbordbtn]").attr("style",
									"display:none");
							$("a[href=#viewverifydbtn]").attr("style",
									"display:none");
							$("div[id=iframedashboard]").html("");
							
							$("div[id=warning]").attr(
									"style",
							"display:block");
							$("div[id=success]").attr(
									"style",
							"display:none");
							$("div[id=error]").attr(
									"style",
							"display:none");
						} else {
							$("input[name=executeAction]").val("execute");
							var projectname = $("select[name=exeproject]")
									.val();

							var progress = setInterval(
									function() {
										var $bar = $('.bar');

										if ($bar.width() == 500) {

											// complete

											clearInterval(progress);

											//  $("a[href=#executebtn]").parents("form").submit();
											$
													.post(
															"itafexecute.do",
															{
																executeAction : "execute",
																exeproject : projectname
															},
															function(
																	responseText) {
																//alert( "success" );

																//alert(responseText);

																$bar.width(500);
																$('.progress')
																		.removeClass(
																				'active');
																// 	$("div[id=iframedashboard]").html(responseText);
																
																if (!jQuery
																		.isEmptyObject(JSON
																				.parse(responseText))) {
																	google.charts
																			.setOnLoadCallback(drawChart(responseText));
																	google.charts
																			.setOnLoadCallback(drawTableChart(responseText));
																	$(
																	"a[href=#viewdashbordbtn]")
																	.attr(
																			"style",
																			"display:block");
																	$(
																	"a[href=#viewverifydbtn]")
																	.attr(
																			"style",
																			"display:block");
																	
																	$("div[id=warning]").attr(
																			"style",
																	"display:none");
																	$("div[id=success]").attr(
																			"style",
																	"display:block");
																	$("div[id=error]").attr(
																			"style",
																	"display:none");
																}else{
																	$(
																	"a[href=#viewdashbordbtn]")
																	.attr(
																			"style",
																			"display:none");
																$(
																	"a[href=#viewverifydbtn]")
																	.attr(
																			"style",
																			"display:none");
																
																$("div[id=warning]").attr(
																		"style",
																"display:none");
																$("div[id=success]").attr(
																		"style",
																"display:none");
																$("div[id=error]").attr(
																		"style",
																"display:block");
																}
																
																$('#executebtn')
																		.modal(
																				'hide');
																$bar.width(0);

															}).done(
															function(data) {
																//    alert( "second success" );
															}).fail(function() {
														//    alert( "error" );

													}).always(function() {
														//  alert( "finished" );

													});

										} else {

											// perform processing logic here

											$bar.width($bar.width() + 50);
										}

										// $bar.text($bar.width()/5 + "%");
									}, 800);

						}
					}
					$("a[href=#viewverifydbtn]")
							.click(
									function() {
										var projectname = $(
												"select[name=exeproject]")
												.val();
										if ($("select[name=exeproject]").val() != "Select") {

											$
													.post(
															"itafexecute.do",
															{
																executeAction : "#VChart",
																exeproject : projectname
															},
															function(
																	responseText) {
																//alert( "success" );

																//	alert(responseText);

																google.charts
																		.setOnLoadCallback(drawChart(responseText));
																$(
																		"a[href=#viewdashbordbtn]")
																		.attr(
																				"style",
																				"display:block");
																$(
																		"a[href=#viewverifydbtn]")
																		.attr(
																				"style",
																				"display:block");

															}).done(
															function(data) {
																// alert( "second success" );
															}).fail(function() {
														// alert( "error" );
													}).always(function() {
														// alert( "finished" );
													});
										}

									});
					/*$("a[href=#executebtn]").click(function(){
					$("input[name=executeAction]").val("execute");
					var projectname=  $("select[name=exeproject]").val();
					if($("select[name=exeproject]").val() != "Select"){
						var progress = setInterval(function() {
						    var $bar = $('.bar');

						    if ($bar.width()==500) {
						      
						        // complete
						      
						        clearInterval(progress);
						        
						      //  $("a[href=#executebtn]").parents("form").submit();
						       $.post( "itafexecute.do", {executeAction: "execute" ,exeproject:projectname}, function(responseText) {
						        	//alert( "success" );
						        	
						        	//alert(responseText);
						        	 $bar.width(500);
						        	$('.progress').removeClass('active');
							      // 	$("div[id=iframedashboard]").html(responseText);
							        google.charts.setOnLoadCallback(drawChart(responseText));
						        	$("a[href=#viewdashbordbtn]").attr("style","display:block");
						        	$("a[href=#viewverifydbtn]").attr("style","display:block");
						        	$('#executebtn').modal('hide');
								        $bar.width(0);
						        	})
						        	  .done(function(data) {
						        	   // alert( "second success" );
						        	  })
						        	  .fail(function() {
						        	   // alert( "error" );
						        	  })
						        	  .always(function() {
						        	   // alert( "finished" );
						        	});
						        
						    } else {
						      
						        // perform processing logic here
						      
						        $bar.width($bar.width()+50);
						    }
						    
						   // $bar.text($bar.width()/5 + "%");
							}, 800);
					}else{
						$("select[name=exeproject]").attr("required", "true");
						 $('#executebtn').modal('hide');
					}
					
					
					});*/

					$("a[href=#logfilebtn]").click(function() {
						$("input[name=executeAction]").val("logfile");
						//$("a[href=#logfilebtn]").parents("form").submit();
						var projectname = $("select[name=exeproject]").val();
						$.post("itafexecute.do", {
							executeAction : "logfile",
							exeproject : projectname
						}, function(responseText) {
							//  alert( "success" );

							//alert(responseText);
							$("div[id=iframeextentdashboard]").html(responseText);
						}).done(function(data) {
							//  alert( "second success" );
						}).fail(function() {
							//  alert( "error" );
						}).always(function() {
							//alert( "finished" );
						});
					});

					google.charts.load('current', {
						'packages' : [ 'corechart' ]
					});
					google.charts.load('current', {
						'packages' : [ 'table' ]
					});

					function drawChart(dataJson) {
						var resultStr = JSON.parse(dataJson);
						document
						.getElementById('iframedashboard').style.display = "block";	
						document
						.getElementById('iframeextentdashboard').style.display = "none";
						
						document
						.getElementById('iframepiechart').style.display = "block";
						var data = google.visualization.arrayToDataTable([
								[ 'Task', 'Test Case Execution ' ],
								[ 'Pass', resultStr.PASS ],
								[ 'Fail', resultStr.FAIL ] ]);

						var options = {
							title : 'Test Case Execution',
							'width' : 400,
							'height' : 400,
							is3D : true
						};

						var chart = new google.visualization.PieChart(document
								.getElementById('iframepiechart'));

						chart.draw(data, options);
					}

					function drawTableChart(dataJson) {
						var object = JSON.parse(dataJson).reportdata;
						var arr = JSON.parse(object);
						//alert(arr.resultdata[0].groupName)
						document
						.getElementById('iframepiechart').style.display = "block";	
						document
						.getElementById('iframeextentdashboard').style.display = "none";
						document
						.getElementById('iframedashboard').style.display = "block";
						var data = new google.visualization.DataTable();
						data.addColumn('string', 'GroupName');
						data.addColumn('string', 'Iteration');
						data.addColumn('string', 'TestCaseID');
						data.addColumn('string', 'TransactionType');
						data.addColumn('string', 'TestCaseDesription');
						data.addColumn('string', 'StartDate');
						data.addColumn('string', 'EndDate');
						data.addColumn('string', 'Status');
						data.addColumn('string', 'Description');
						for (var i = 0; i < arr.resultdata.length; i++) {
							data.addRows([ [ arr.resultdata[i].groupName,
									arr.resultdata[i].iteration,
									arr.resultdata[i].testCaseID,
									arr.resultdata[i].transactionType,
									arr.resultdata[i].testCaseDesription,
									arr.resultdata[i].startDate, arr.resultdata[i].endDate,
									arr.resultdata[i].status,
									arr.resultdata[i].description ]

							]);
						}
						var table = new google.visualization.Table(document
								.getElementById('iframedashboard'));

						table.draw(data, {
							showRowNumber : true,
							width : '100%',
							height : '100%',
							page : 'event',
							pageSize : 10
						});
					}
				</script>



			</form>
		</div>


	</div>



	<!-- Modal -->
	<div class="modal fade" id="executebtn" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header button-color">
					<h4 class="modal-title button-color" id="myModalLabel">Please
						Wait</h4>
				</div>
				<div class="modal-body center-block">
					<p>Preparing iTAF for Execution...</p>
					<div class="progress">
						<div class="progress-bar bar" role="progressbar" aria-valuenow="0"
							aria-valuemin="0" aria-valuemax="100"></div>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->


	<!-- footer section start -->



	<jsp:include page="footer.jsp" />
	<!-- footer section end -->


</body>
</html>