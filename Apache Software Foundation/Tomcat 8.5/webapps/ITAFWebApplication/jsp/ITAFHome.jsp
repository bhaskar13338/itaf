<%@page import="com.majesco.itaf.dao.ConfigFile"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ITAF-Home</title>
<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="./css/demo2/headerfooter.css">
<link rel="stylesheet" type="text/css"
	href="./css/demo2/sidebartreeview.css">
<link rel="stylesheet" type="text/css"
	href="./css/demo2/verticalmenu.css">
</head>
<body>
	<%
		//allow access only if session exists
		String user = null;
		if (session.getAttribute("user") == null) {
			response.sendRedirect("login.jsp");
		} else
			user = (String) session.getAttribute("user");
		String userName = null;
		String sessionID = null;
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals("user"))
					userName = cookie.getValue();
				if (cookie.getName().equals("JSESSIONID"))
					sessionID = cookie.getValue();
			}
		} else {
			sessionID = session.getId();
		}
		
		List<String> fileNames = new ArrayList<>();
		fileNames = (ArrayList<String>)session.getAttribute("projectFilesList");
		String itafRepopath = (String)session.getAttribute("itafreposutirypath");
		HashMap<String,ConfigFile> projectConfigMap= (HashMap<String,ConfigFile>)session.getAttribute("projectConfigMap");
		
	%>
	<script>
 		 var parentPath = "<%= itafRepopath %>";
	</script>


	<div id="header" class="navbar navbar-default navbar-fixed-top">
		<div class="navbar-header">
			<button class="navbar-toggle collapsed" type="button"
				data-toggle="collapse" data-target=".navbar-collapse">
				<i class="icon-reorder"></i>
			</button>
			<a class="navbar-brand" href="#"> Insurance Testing Automation
				Framework (iTAF) </a>
		</div>
		<form action="<%=response.encodeURL("./logout.do")%>"
			method="post">
			<nav class="collapse navbar-collapse">
			<ul class="nav navbar-nav pull-right">
				<li class="dropdown">
					<a href="#" id="nbAcctDD" class="dropdown-toggle" data-toggle="dropdown">
					<span class="glyphicon glyphicon-user"></span> 
					<i class="icon-user"></i>
						<%=user%>
					<i class="icon-sort-down"></i>
					</a>
					<ul class="dropdown-menu pull-right">
						<li><a href="#" title="logouthref">Log Out</a></li>
					</ul></li>
			</ul>
			</nav>
		</form>
	</div>
	<div id="wrapper">
		<!-- sidebar start -->
		<form action="./ProjectWorkspaceServlet" id="projectMenu"  name ="projectMenu" method="post" >
		<div id="sidebar-wrapper" class="col-md-2">
			<div id="sidebar">
				<!-- Put Your Code here -->
				<div id="MainMenu">
					<div class="list-group panel">
						<a href="#demo4" class="list-group-item list-group-item-success"
							data-toggle="collapse" data-parent="#MainMenu">Create New
							Project </a> <a href="#demo3"
							class="list-group-item list-group-item-success"
							data-toggle="collapse" data-parent="#MainMenu">Design
							Subsystem</a>
							
									
						<div class="collapse" id="demo3">
						
						<%if(fileNames!=null && fileNames.size()>0){ %>			
<%for(int i=0 ;i<fileNames.size(); i++){%>	
	  <a href="#SubMenu<%=i+1%>" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu<%=i+1 %>"><%=fileNames.get(i) %><i class="fa fa-caret-down"></i></a>
      <div class="collapse list-group-submenu" id="SubMenu<%=i+1%>">
        <a href="#commonresources" class="list-group-item" data-parent="#SubMenu<%=i+1%>">CommonResources</a>
        <a href="#resources" class="list-group-item" data-parent="#SubMenu<%=i+1%>">Resources</a>
        <a href="#templates" class="list-group-item" data-parent="#SubMenu<%=i+1%>">Templates</a>
      </div>
      <%}} %>
						
						
	   							<!-- ended -->
						</div>
						

						<a href="#demo2" class="list-group-item list-group-item-success"
							data-toggle="collapse" data-parent="#MainMenu">Execution
							Subsystem </a>
					</div>
				</div>
				<!-- end -->
			</div>
			
			</form>
		</div>
		<!-- sidebar end -->
		<div id="main-wrapper" class="col-md-10 pull-right"
			style="padding: 40px 0 0px 0;">
			<div id="main">
				<div class="page-header" style="margin: 10px 0 20px;">
					<h3>!! Welcome !!</h3>
				</div>
				<p>iTAF – Automation is often seen as a panacea that reduces
					testing time and increases quality. But writing and modifying
					automation scripts adds significantly to the cost and time incurred
					while testing. Majesco has built a unique hybrid automation
					framework – Insurance Test Automation Framework (iTAF)- that
					enables rapid implementation of automation without adding
					significantly to the costs</p>
			</div>
			<div id="main2">
			<p>test this2</p>	

			</div>
			<div id="main3">
				<!-- only show for CR click -->
				<div class="container">
				  <div class="row">
				    <div class="col-md-10">
				       <h3></h3>
				        <!-- tabs -->
				        <div class="tabbable">
				          <ul class="nav nav-tabs">
				            <li class="active"><a href="#one" data-toggle="tab">Config</a></li>
				            <li><a href="#two" data-toggle="tab">MainController</a></li>
				            <li><a href="#twee" data-toggle="tab">TransactionInputFiles</a></li>
				            <li><a href="#four" data-toggle="tab">UniqueNumber</a></li>
				            <li><a href="#five" data-toggle="tab">VerificationTableList</a></li>
				          </ul>
				          <div class="tab-content" style="width: 1000px;height: 1000px">
				            <div class="tab-pane active" id="one">
				            <!-- Create code Here start -->
				       
				            	
				        <div style="margin-top: 30px">
				        
					   <form class="form-horizontal" role="createprojform" data-toggle="validator" action="./ProjectWorkspaceServlet" method="post">
					  					   
						 
						<div class="form-group">
							<label class="control-label col-sm-4" for="burl">BASE URL</label>
							<div class="col-sm-6">
								<input  type="text" class="form-control" id="baseurl" name="baseurl"
									 placeholder="Enter Project Url" style="width: 600px;" value="" required></inupt>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4" for="burl">BROWSER TYPE</label>
							<div class="col-sm-6">
								
									 <select class="form-control" placeholder="Select Browser" id="browsertype" name="browsertype" >
										  <option value="InternetExplorer">InternetExplorer</option>
										  <option value="Chrome">Chrome</option>
										  <option value="FireFox">FireFox</option>
										  <option value="Safari">Safari</option>
									</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4" for="burl">MSEXCEL</label>
							<div class="col-sm-6">
								
									 <select class="form-control" placeholder="Select Browser" id="msexecl" name="msexecl" >
										  <option value="2003">2003</option>
									</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4" for="burl">USER INTERACTION</label>
							<div class="col-sm-6">
								
									 <select class="form-control" placeholder="Select Browser" id="userinteraction" name="userinteraction" >
										  <option value="True">True</option>
										  <option value="False">False</option>
									</select>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-sm-4" for="cnumber">CYCLE NUMBER</label>
							<div class="col-sm-6">
								<input  type="text" class="form-control" id="cyclenumber" name="cyclenumber"
									 placeholder="Enter Cycle Number" style="width: 600px;" value="" required></inupt>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4" for="cnumber">TIMEOUT</label>
							<div class="col-sm-6">
								<input  type="text" class="form-control" id="timeout" name="timeout"
									 placeholder="Enter Timeout" style="width: 600px;" required></inupt>
							</div>
						</div>	
						<div class="form-group">
							<label class="control-label col-sm-4" for="cnumber">Email</label>
							<div class="col-sm-6">
								<input  type="email" class="form-control" id="email" name="email"
									 placeholder="Enter email address" style="width: 600px;" required></inupt>
							</div>
						</div>		
						
						 <div class="form-group">
							<label class="control-label col-sm-4" for="cProjname">PROJECT NAME</label>
							<div class="col-sm-6">
								<input  type="text" class="form-control" id="cprojectname" name="cprojectname"
									 style="width: 600px;" value="" readonly="readonly"></inupt>
							</div>
						</div>	
						<div class="form-group">
							<label class="control-label col-sm-4" for="confpath">CONTROLLER FILEPATH</label>
							<div class="col-sm-6">
								<input  type="text" class="form-control" id="controllerfilepath" name="controllerfilepath"
									 style="width: 600px;" value="" readonly="readonly"></inupt>
							</div>
						</div>
						
						 <div class="form-group">
							<label class="control-label col-sm-4" for="transinfo">TRANSACTION INFO</label>
							<div class="col-sm-6">
								<input  type="text" class="form-control" id="transactioninfo" name="transactioninfo"
									 style="width: 600px;" value="" readonly="readonly"></inupt>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4" for="vertblistpath">VERIFICATION TABLE LIST PATH</label>
							<div class="col-sm-6">
								<input  type="text" class="form-control" id="verificationtablelistpath" name="verificationtablelistpath"
									 style="width: 600px;" value="" readonly="readonly"></inupt>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4" for="transippath">TRANSACTION INPUT FILEPATH</label>
							<div class="col-sm-6">
								<input  type="text" class="form-control" id="transactioninputfilepath" name="transactioninputfilepath"
									 style="width: 600px;" value="" readonly="readonly"></inupt>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4" for="ipdatapath">INPUT DATA FILEPATH</label>
							<div class="col-sm-6">
								<input  type="text" class="form-control" id="inputdatafilepath" name="inputdatafilepath"
									 style="width: 600px;" value="" readonly="readonly"></inupt>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4" for="respath">RESULT FILEPATH</label>
							<div class="col-sm-6">
								<input  type="text" class="form-control" id="resultpath" name="resultpath"
									 style="width: 600px;" value="" readonly="readonly"></inupt>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4" for="resultop">RESULTOUTPUT</label>
							<div class="col-sm-6">
								<input  type="text" class="form-control" id="resultoutput" name="resultoutput"
									 style="width: 600px;" value="" readonly="readonly"></inupt>
							</div>
						</div>
						
						
						<div class="form-group">
							<label class="control-label col-sm-4" for="verResultpath">VERIFICATION RESULTS PATH</label>
							<div class="col-sm-6">
								<input  type="text" class="form-control" id="verificationresultspath" name="verificationresultspath"
									 style="width: 600px;" value="" readonly="readonly"></inupt>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-sm-4" for="exevalpath">EXPECTED VALUES PATH</label>
							<div class="col-sm-6">
								<input  type="text" class="form-control" id="expectedvaluespath" name="expectedvaluespath"
									 style="width: 600px;" value="" readonly="readonly"></inupt>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4" for="vertemppath">VERIFCATION TEMPLATE PATH</label>
							<div class="col-sm-6">
								<input  type="text" class="form-control" id="verificationtemplatepath" name="verificationtemplatepath"
									 style="width: 600px;" value="" readonly="readonly"></inupt>
							</div>
						</div>
						
						
						<div class="form-group">
							<div class="col-sm-offset-4 col-sm-10">
								<button type="submit" class="btn btn-default" id="update" name="update" value="update">Update</button>
							</div>
						</div>
					</form>
				</div>
				            <!-- Create code Here end -->
				            
				            </div>
				            <div class="tab-pane" id="two">
				            <!-- add code tab 2 here -->
							<div style="margin-top: 30px"> 
			
			            <h3> Choose File to Upload in Server </h3>
			
					            <form  name="fileform" action="./UploadDownloadFileServlet" method="post" enctype="multipart/form-data">
					
					                <input type="file" name="file" />
					
					                <input type="submit" value="upload" />
					                <a href="#downloadlink"> Down load template</a>
					
					            </form>         
			
			        			</div>

				            
				            <!-- add code tab 2 end -->
				            
				            </div>
				            <div class="tab-pane" id="twee">Thirdamuno, ipsum dolor sit amet, consectetur adipiscing elit. Duis pharetra varius quam sit amet vulputate. 
				            Quisque mauris augue, molestie tincidunt condimentum vitae.</div>
				             <div class="tab-pane" id="four">fsdfThirdamuno, ipsum dolor sit amet, consectetur adipiscing elit. Duis pharetra varius quam sit amet vulputate. 
				            Quisque mauris augue, molestie tincidunt condimentum vitae.</div>
				             <div class="tab-pane" id="five">gggggThirdamuno, ipsum dolor sit amet, consectetur adipiscing elit. Duis pharetra varius quam sit amet vulputate. 
				            Quisque mauris augue, molestie tincidunt condimentum vitae.</div>
				           </div>
				        </div>
				        <!-- /tabs -->
				
				    </div>
				  </div><!-- /row -->
				</div>
				<!-- only show for CR click -->
				
			</div>
			<div id="main4">
					<div style="margin-top: 30px;">
					<form class="form-horizontal" role="createprojform" data-toggle="validator" action="./ProjectWorkspaceServlet" method="post">
						<div class="form-group">
							<label class="control-label col-sm-2" for="proname">Project Name</label>
							<div class="col-sm-6">
								<input  type="text" class="form-control" id="proname" name="proname"
									placeholder="Enter Project Name" maxlength="20" style="width: 250px;" required>
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-6">
								<button type="submit" class="btn btn-default" id="create" name="create" value="create">Create</button>
							</div>
						</div>
					</form>
				</div>
				
			</div>

			<div class="col-md-12 footer">
				<ul class="nav navbar-nav">
					<li><a href="">Copyright 2016. All rights reserved with
							MAJESCO Automation Testing COE </a></li>
				</ul>
			</div>

		</div>
	</div>

	<!-- Navigation -->

	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src="./js/jquery.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="./js/bootstrap.min.js"></script>

	<script src="./js/demo2/mainwrapper.js"></script>
	<script src="./js/logout.js"></script>



</body>
</html>