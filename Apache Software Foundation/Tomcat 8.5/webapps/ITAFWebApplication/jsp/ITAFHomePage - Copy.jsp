<%@page import="com.majesco.itaf.dao.ProjectPack"%>
<%@page import="com.majesco.itaf.dao.ConfigFile"%>
<%@page import="com.majesco.itaf.dao.TransactionStructureData"%>
<%@page import="com.majesco.itaf.dao.TransactionStructureFile"%>
<%@page import="com.majesco.itaf.dao.TransactionMappingFile"%>
<%@page import="java.util.List"%>
<%@page import="com.majesco.itaf.dao.MainControllerFile"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ITAF Home</title>
<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
<!-- <link href="./css/demo4/bootstrap.css" rel="stylesheet">-->
<link rel="stylesheet" type="text/css" href="./css/demo3/try.css">

<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">

</head>

<body>

<%
String url = request.getRequestURL().toString();
String baseURL = url.substring(0, url.length() - request.getRequestURI().length()) + request.getContextPath() + "/";
		
List<MainControllerFile> controllerFiles =(List<MainControllerFile>) request.getAttribute("currentProMainController");
String subMenu =(String) request.getAttribute("subItem");
List<TransactionMappingFile>  mappingFiles =(List<TransactionMappingFile>) request.getAttribute("currentProTransMapping");
List<TransactionStructureFile>  structureFiles =(List<TransactionStructureFile>) request.getAttribute("currentProTranStrdefault");
List<TransactionStructureData>  structureDatas =(List<TransactionStructureData>) request.getAttribute("currentProTranStrDatadefault");
List<TransactionStructureData>  reqstructureDatas =(List<TransactionStructureData>) request.getAttribute("requestedStrData");		

%>

<input type="hidden" id="hidReqprojectname" value="${currentProject}" />
<input type="hidden" id="hidmenuitem" value="${menuItem}" />
<input type="hidden" id="hidsubitem" value="${subItem}" />
<input type="hidden" id="hidconfFlag" value="${configFlag}" />
<!-- Top Menu start -->
	<div id="wrapper">
		<div id="header">
		<form action="./itafpack.do" method="post">
			<input type="hidden" name="navigationAction" id="navigationAction"/>
			<ul class="topnav" style="background-color: #2a6598">
			
				<li><a href="#home">Home</a></li>
				<li><a href="#createpack">Create Pack</a></li>
				<!-- <li><a href="#modify">Modify</a></li>-->
				<li><a href="#execution">Execution</a></li>
				<li><a href="#schedule">Schedule</a></li>
				<li><a href="#access">Grant Access</a></li>
				<li><a href="#log">Execution Log</a></li>
				<li class="icon"><a href="javascript:void(0);"
					onclick="myFunction()">&#9776;</a></li>
					
				<li  style="float: right">
			        <!-- <a href="#"  class="dropbtn"><span class="glyphicon glyphicon-user"></span>  <i class="icon-user"></i>Bhaskar<i class="icon-sort-down"></i></a>
					     -->
		 </form>
				<form action="./logout.do" method="post">
			    <ul class="nav navbar-nav pull-right">
					<li class="dropdown">
						<a href="#" id="nbAcctDD" class="dropdown-toggle" data-toggle="dropdown">
						<span class="glyphicon glyphicon-user"></span> 
						<i class="icon-user"></i>
							Bhaskar
						<i class="icon-sort-down"></i>
						</a>
						<ul class="dropdown-menu dropdown-menu pull-right">
							<li><a href="#" title="logouthref">Log Out</a></li>
						</ul></li>
				</ul>
				</form>
			    </li>
			</ul>
		</div>
<!-- Top Menu end -->
<!-- Top  vertical Menu start -->
		<!-- sidebar start -->
		<form id="projectMenu" name="projectMenu" method="post">
			<div id="sidebar-wrapper" class="col-md-2" style="padding-left: 5px;">
				<div id="sidebar">
					<!-- Put Your Code here -->
					<div id="MainMenu">
						<div class="list-group panel">
							<a href="#crproject" class="list-group-item list-group-item-success"
								data-toggle="collapse" data-parent="#MainMenu">Search/Create
								Project</a> 
								<!-- <a href="#moproject" class="list-group-item list-group-item-success"
								data-toggle="collapse" data-parent="#MainMenu">Modify
								Project</a>--><a href="#sconfig"
								class="list-group-item list-group-item-success"
								data-toggle="collapse" data-parent="#MainMenu">Set
								Configration</a>
								 <a href="#cstructure"
								class="list-group-item list-group-item-success"
								data-toggle="collapse" data-parent="#MainMenu">Create
								Structure Sheet</a> 
								<a href="#bussfunction"
								class="list-group-item list-group-item-success"
								data-toggle="collapse" data-parent="#MainMenu">Define
								Business Functions</a>
								<a href="#scenario"
								class="list-group-item list-group-item-success"
								data-toggle="collapse" data-parent="#MainMenu">Define
								Scenario</a> <a href="#cipdata"
								class="list-group-item list-group-item-success"
								data-toggle="collapse" data-parent="#MainMenu">Create Input
								Data</a> <a href="#vtemplate"
								class="list-group-item list-group-item-success"
								data-toggle="collapse" data-parent="#MainMenu">Create
								Verification Template</a> <a href="#vflow"
								class="list-group-item list-group-item-success"
								data-toggle="collapse" data-parent="#MainMenu">Verification
								Flow</a> <a href="#dashboard"
								class="list-group-item list-group-item-success"
								data-toggle="collapse" data-parent="#MainMenu">Dashboard</a>
						</div>
					</div>
					<!-- end -->
				</div>
			</div>
		</form>
		<!-- Top  vertical Menu start End -->
		<!-- Home Page default section start -->
		<div id="main-wrapper" class="col-md-10 pull-right"	>
			<div id="main1" >
				<div class="page-header" style="margin: 0px 0 20px;">
					<h3>!! Welcome !!</h3>
				</div>
				<p>iTAF – Automation is often seen as a panacea that reduces
					testing time and increases quality. But writing and modifying
					automation scripts adds significantly to the cost and time incurred
					while testing. Majesco has built a unique hybrid automation
					framework – Insurance Test Automation Framework (iTAF)- that
					enables rapid implementation of automation without adding
					significantly to the costs</p>
			</div>
			<!-- Home Page default section end -->
			<!-- Project Creation/update/modify section start -->
			<div id="main2">
				
			<form  action="./itafpack.do" role="createprojform" data-toggle="validator" method="post">
			
					<div class="col-md-12 form-group">
						<label id="infomsg1" style="color: red">${msgprojectexist}</label>
						<label id="infomsg2" style="color: green">${msgprojectsuccess}</label>
							
					</div>
					
					<div class="col-md-12 form-group">
								
									<label class="control-label col-sm-4" for="proname" style="width:10%">Project
										Name</label>
									<div class="row">
										<div class="col-sm-4" >
											<input type="text" class="form-control" id="proname"
												name="proname" placeholder="Enter Project Name" maxlength="30"
												 value="${currentProject}" required>
										</div>
										<div class="col-sm-4" style="padding-left:0px;">
											<button type="submit" class="btn btn-default button-color" id="search"
												name="search" value="search">Search</button>
										</div>
									
								     </div>
						</div>

						<div class="col-md-12 form-group" id="btnCreate">
							<div class="col-sm-10" style="margin-left:105px;">
								<button type="submit" class="btn btn-default button-color" id="create"
									name="create" value="create">Create</button>
								<button type="submit" class="btn btn-default button-color" id="addnew"
									name="addnew" value="addnew">Add New</button>
								
								<button type="submit" class="btn btn-default button-color" id="continue"
									name="continue"  value="continue">Continue</button>
							</div>
						</div>
						<!-- <div class="col-xs-12 form-group" id="btnmodify">
							<div class="col-sm-offset-4 col-sm-10">
								<button type="submit" class="btn btn-default button-color" id="search"
									name="search" value="search">Search</button>
									<button type="submit" class="btn btn-default button-color" id="rename"
									name="rename" value="rename" disabled>Rename</button>
									<button type="submit" class="btn btn-default button-color" id="continue"
								    name="continue"  value="continue">Continue</button>
									
									
							</div>
						</div>-->
				</form>	
				<form  action="./itafpack.do" role="createprojform"  method="post">
			    <input type="hidden" name="directoryname" value=""/>
				<%if(request.getAttribute("projectFilesList")!=null && ((List<ProjectPack>)request.getAttribute("projectFilesList")).size()>0){
					
					List<ProjectPack>projectlist = (List<ProjectPack>)request.getAttribute("projectFilesList");
					
					%>
					<div class="row clearfix">
				    
						<div class="col-md-12 column margin-15">
					<div class="table-responsive">
					  <table class="table table-condensed table-striped table-bordered table-hover no-margin">
					    <thead>
					      <tr>
					        <th style="width:5%">
					          <input class="no-margin" type="checkbox">
					        </th>
					        <th style="width:25%">Project Name</th>
					        <th style="width:15%" class="hidden-phone">Date</th>
					        <th style="width:20%" class="hidden-phone">Created By</th>
					         <th style="width:40%" class="hidden-phone">Actions</th>
					       
					      </tr>
					    </thead>
					    <tbody id="projectTable">
					    <%for(ProjectPack pack :projectlist){ %>
					      <tr>
					        <td>
					          <input class="no-margin" type="checkbox">
					        </td>
					        <td>
					          <span class="name"><%=pack.getProjectname() %></span>
					        </td>
					       
					         <td class="hidden-phone"><%=pack.getCreateddate() %></td>
					        <td class="hidden-phone">
					           <%=pack.getCreatedby() %>
					        </td>
					       <td class="hidden-phone">
					          <div class="btn-group btn-group-sm" role="group">
					          <button type="submit" class="btn btn-default button-color" id="copyproject"
									name="copyproject" value="copyproject" >Copy Project</button>
					         <button type="submit" class=" open-AddBookDialog btn btn-default button-color" id="rename"
									name="rename" value="rename"  data-toggle="modal" data-id="ISBN-001122" >Rename</button>
									
									
							<button type="submit" class="btn btn-default button-color" id="deleteproject"
									name="deleteproject" value="deleteproject" >Delete</button>		
					         
					     
					         
					         
					         
					         
					          </div>
					        </td>
					        
					      </tr>
					     <%} %>
					    </tbody>
					  </table>
					</div>
					</div>
					<div class="col-md-12 text-center">
				      <ul class="pagination pagination-sm pager" id="myPager"></ul>
				     </div>
					</div>
					
				<%} %>
				<div class="modal hide" id="addBookDialog">
					 <div class="modal-header">
					    <button class="close" data-dismiss="modal">×</button>
					    <h3>Modal header</h3>
					  </div>
					    <div class="modal-body">
					        <p>some content</p>
					        <input type="text" name="bookId" id="bookId" value=""/>
					    </div>
				</div>

				</form>
				
				
				
			</div>

				<!-- Project Creation/update/modify section end -->
		 <!-- Project  Config Creation/update/modify section start -->
			<div id="main3" >
				<form  role="configform" data-toggle="validator"  action="./itafpack.do" method="post">
				<div class="col-xs-12 form-group">
				<label class="col-sm-2" id="screenName" name="screenName">Set Configration -</label>
				<%if(((String)request.getAttribute("currentProject"))!=null &&((String)request.getAttribute("currentProject")).length()>0) {%>
					<label class="col-sm-2" for="burl" id="configProject">${currentProject}</label>
				<%}else{%>
					<label class="col-sm-2" for="burl" id="configProject"><font color=red>Project Details required.</font></label>
				<%} %>
				<label><font color=red>${msgprojectnotexist}</font></label>
				</div>
				<div class="col-xs-12 form-group">
					<label class="control-label col-sm-4" for="burl">Base URL</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="baseurl"
							name="baseurl" placeholder="Enter Project Url"
							style="width: 600px;" value="${currentProConfig.baseurl}" required>
						</inupt>
					</div>
				</div>
				<div class="col-xs-12 form-group">
					<label class="control-label col-sm-4" for="burl">Browser Type</label>
					<div class="col-sm-6">

						<select class="form-control" placeholder="Select Browser"
							id="browsertype" name="browsertype">
							<option value="InternetExplorer" <%if(request.getAttribute("currentProConfig") !=null &&( ((ConfigFile)request.getAttribute("currentProConfig")).getBrowsertype()!=null && ((ConfigFile)request.getAttribute("currentProConfig")).getBrowsertype().equalsIgnoreCase("InternetExplorer") )){ %>selected<%} %>>Internet Explorer</option>
							<option value="Chrome" <%if(request.getAttribute("currentProConfig") !=null && (((ConfigFile)request.getAttribute("currentProConfig")).getBrowsertype()!=null && ((ConfigFile)request.getAttribute("currentProConfig")).getBrowsertype().equalsIgnoreCase("Chrome") )){ %>selected<%} %>>Chrome</option>
							<option value="FireFox" <%if(request.getAttribute("currentProConfig") !=null && (((ConfigFile)request.getAttribute("currentProConfig")).getBrowsertype()!=null && ((ConfigFile)request.getAttribute("currentProConfig")).getBrowsertype().equalsIgnoreCase("FireFox") )){ %>selected<%} %>>FireFox</option>
							<option value="Safari" <%if(request.getAttribute("currentProConfig") !=null && (((ConfigFile)request.getAttribute("currentProConfig")).getBrowsertype()!=null && ((ConfigFile)request.getAttribute("currentProConfig")).getBrowsertype().equalsIgnoreCase("Safari") )){ %>selected<%} %>>Safari</option>
						</select>
					</div>
				</div>
				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="burl">MSEXCEL</label>
					<div class="col-sm-6">

						<select class="form-control" placeholder="Select Browser"
							id="msexecl" name="msexecl">
							<option value="2003" selected>2003</option>
						</select>
					</div>
				</div>
				<div class="col-xs-12 form-group">
					<label class="control-label col-sm-4" for="burl">Execution Approch</label>
					<div class="col-sm-6">

						<select class="form-control" placeholder="Select Browser"
							id="executionapproch" name="executionapproch">
							<option value="Linear" <%if(request.getAttribute("currentProConfig") !=null && (((ConfigFile)request.getAttribute("currentProConfig")).getExecution_approach() !=null && ((ConfigFile)request.getAttribute("currentProConfig")).getExecution_approach().equalsIgnoreCase("Linear") )){ %>selected<%} %>>Linear</option>
							<option value="TimeTravel" <%if(request.getAttribute("currentProConfig") !=null && (((ConfigFile)request.getAttribute("currentProConfig")).getExecution_approach() !=null && ((ConfigFile)request.getAttribute("currentProConfig")).getExecution_approach().equalsIgnoreCase("TimeTravel") )){ %>selected<%} %>>TimeTravel</option>
							
						</select>
					</div>
				</div>
				<div class="col-xs-12 form-group">
					<label class="control-label col-sm-4" for="burl">User Interaction</label>
					<div class="col-sm-6">

						<select class="form-control" placeholder="Select Browser"
							id="userinteraction" name="userinteraction">
							<option value="True" <%if(request.getAttribute("currentProConfig") !=null && (((ConfigFile)request.getAttribute("currentProConfig")).getUserinteraction() !=null && ((ConfigFile)request.getAttribute("currentProConfig")).getUserinteraction().equalsIgnoreCase("True") )){ %>selected<%} %>>True</option>
							<option value="False" <%if(request.getAttribute("currentProConfig") !=null && ( ((ConfigFile)request.getAttribute("currentProConfig")).getUserinteraction() !=null && ((ConfigFile)request.getAttribute("currentProConfig")).getUserinteraction().equalsIgnoreCase("False") )){ %>selected<%} %>>False</option>
						</select>
					</div>
				</div>
				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="cnumber">Cycle Number</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="cyclenumber"
							name="cyclenumber" placeholder="Enter Cycle Number"
							style="width: 600px;" value="Cycle1" required>
						</inupt>
					</div>
				</div>
				<div class="col-xs-12 form-group">
					<label class="control-label col-sm-4" for="cnumber">Timeout</label>
					<div class="col-sm-5">
						 
						 	<input type="text" class="form-control" id="timeout"
								name="timeout" placeholder="Timeout in Seconds" style="width:505px;"
								required value="${currentProConfig.timeout}">
							</inupt>
							
					</div>
					
				</div>
				<div class="col-xs-12 form-group">
					<label class="control-label col-sm-4" for="cnumber">Email</label>
					<div class="col-sm-6">
					
						<input type="email" class="form-control" id="email" name="email"
							placeholder="Enter Email Address (Use ; to seperate the entries) " style="width: 505px;" required value="${currentProConfig.email_id}">
						</inupt>
						
					
					</div>
				</div>

				<div class="col-xs-12 form-group">
					<label class="control-label col-sm-4" for="cProjname">Project Name</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="cprojectname"
							name="cprojectname" style="width: 505px;" value="${currentProConfig.project_name}"
							readonly="readonly">
						</inupt>
					</div>
				</div>
				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="confgpath">Config Filepath</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="configfilepath"
							name="configfilepath" style="width: 600px;" value="${currentProConfig.config_filepath}"
							readonly="readonly">
						</inupt>
					</div>
				</div>
				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="confpath">Controller Filepath</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="controllerfilepath"
							name="controllerfilepath" style="width: 600px;" value="${currentProConfig.controller_filepath}"
							readonly="readonly">
						</inupt>
					</div>
				</div>

				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="transinfo">Transaction Info</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="transactioninfo"
							name="transactioninfo" style="width: 600px;" value="${currentProConfig.transaction_info}"
							readonly="readonly">
						</inupt>
					</div>
				</div>
				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="vertblistpath">Verification Table List Path</label>
					<div class="col-sm-6">
						<input type="text" class="form-control"
							id="verificationtablelistpath" name="verificationtablelistpath"
							style="width: 600px;" value="${currentProConfig.verificationtablelistpath}" readonly="readonly">
						</inupt>
					</div>
				</div>
				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="transippath">Transaction Input Filepath</label>
					<div class="col-sm-6">
						<input type="text" class="form-control"
							id="transactioninputfilepath" name="transactioninputfilepath"
							style="width: 600px;" value="${currentProConfig.transaction_input_filepath}" readonly="readonly">
						</inupt>
					</div>
				</div>
				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="ipdatapath">Input Data Filepath</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="inputdatafilepath"
							name="inputdatafilepath" style="width: 600px;" value="${currentProConfig.input_data_filepath}"
							readonly="readonly">
						</inupt>
					</div>
				</div>
				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="respath">Result Filepath</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="resultpath"
							name="resultpath" style="width: 600px;" value="${currentProConfig.result_filepath}"
							readonly="readonly">
						</inupt>
					</div>
				</div>
				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="resultop">Result Output</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="resultoutput"
							name="resultoutput" style="width: 600px;" value="${currentProConfig.resultoutput}"
							readonly="readonly">
						</inupt>
					</div>
				</div>
				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="resultop">Dashboard Results Path</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="dashboardresult"
							name="dashboardresult" style="width: 600px;" value="${currentProConfig.dashboardresultspath}"
							readonly="readonly">
						</inupt>
					</div>
				</div>
				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="resultop">Dynamic Result Path</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="dynresultpath"
							name="dynresultpath" style="width: 600px;" value="${currentProConfig.dynamicresultspath}"
							readonly="readonly">
						</inupt>
					</div>
				</div>
				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="resultop">Dashboard Filepath</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="dashboardfilepath"
							name="dashboardfilepath" style="width: 600px;" value="${currentProConfig.dashboard_filepath}"
							readonly="readonly">
						</inupt>
					</div>
				</div>
				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="resultop">HTML Report Filepath</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="htmlfilepath"
							name="htmlfilepath" style="width: 600px;" value="${currentProConfig.htmlreport_filepath}"
							readonly="readonly">
						</inupt>
					</div>
				</div>
				
				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="resultop">Consolelog Filepath</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="consolefilepath"
							name="consolefilepath" style="width: 600px;" value="${currentProConfig.consolelog_filepath}"
							readonly="readonly">
						</inupt>
					</div>
				</div>
				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="verResultpath">Verification Result Path</label>
					<div class="col-sm-6">
						<input type="text" class="form-control"
							id="verificationresultspath" name="verificationresultspath"
							style="width: 600px;" value="${currentProConfig.verificationresultspath}" readonly="readonly">
						</inupt>
					</div>
				</div>

				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="exevalpath">Expected Values Path</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="expectedvaluespath"
							name="expectedvaluespath" style="width: 600px;" value="${currentProConfig.expectedvaluespath}"
							readonly="readonly">
						</inupt>
					</div>
				</div>
				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="vertemppath">VerifiCation Template Path</label>
					<div class="col-sm-6">
						<input type="text" class="form-control"
							id="verificationtemplatepath" name="verificationtemplatepath"
							style="width: 600px;" value="${currentProConfig.verifcationtemplatepath}" readonly="readonly">
						</inupt>
					</div>
				</div>


				<div class="col-xs-12 form-group">
					<div class="col-sm-offset-4 col-sm-10">
						<button type="submit" class="btn btn-default button-color" id="update"
							name="update" value="update">Update</button>
						<button type="submit" class="btn btn-default button-color" id="updatecontinue"
							name="updatecontinue" value="updatecontinue">Update & Continue</button>
						
					</div>
				</div>
				</form>
			</div>
			 <!-- Project  Config Creation/update/modify section end -->
			 <!-- Project  template file import /export section start -->
			<div id="main4" >
			<form name="uploaddownloadform" action="./uploaddownload.do" method="post" enctype="multipart/form-data">
			<input type="hidden" name="reqfilename" id="reqfilename"/>
			<input type="hidden" name="parentprojectname" id="parentprojectname" value="${currentProject}"/>
			<input type="hidden" id="hidconfFlag"   name="hidconfFlag" value="${configFlag}" />
			<div class="col-xs-12 form-group">
			<div class="col-sm-8" style="width:60% ">
				<label class="col-sm-4" id="screenName" name="screenName"> </label>
				<%if(((String)request.getAttribute("currentProject"))!=null &&((String)request.getAttribute("currentProject")).length()>0) {%>
				<input type="hidden" id="parentprojectname" name="parentprojectname" value="${currentProject}"/>
					 <label class="control-label col-sm-2" for="burl" id="parentprojectlabel" name="parentProjectlabel" >${currentProject}</label>
				
				<%}else{%>
					<label class="control-label col-sm-2" for="burl"><font color=red>Project Details required.</font></label>
				<%} %>
				<label class="control-label col-sm-4" for="burl"><font color="red">${confmessage}</font></label>
				<label class="control-label col-sm-4" for="burl"><font color="red">${msgfileselectionerror}</font></label>
				
				
			</div>
			</div>
			<div class="col-xs-12 form-group">
					<div class="col-sm-6" style="width:40% ">
						<label  for="templatedownload">Please Checkout Business File </label>
						<label class="control-label"  for="templatedownload" id="tempaltenamelabel"></label>
					</div>
					<div class="col-sm-4">
						<button type="submit" class="btn btn-default button-color" id="btnfile"
							name="btnfile" value="export">Update</button>
					</div>
				</div>
				<div class="col-xs-12 ">
				
				<div class="col-sm-6" style="width:40% ">
					<label  for="templatedownload">Save Business File</label>
					
				</div>	
								
				<div class="col-sm-4">
						<div class="row">
							<div class="col-xs-9">	
								<!-- <input type="file" name="importfile" id="importfile"/>-->
								<input type="file"  name="importfile" id="importfile" class="filestyle" data-size="nr">
							</div>
							<div class="col-xs-3">
								<button type="submit" class="btn btn-default button-color" id="btnfile" name="btnfile" value="import">Save</button>
							</div>
						</div>
					</div>
					</div>
				</form>
				
				 <div class="col-xs-12 form-group">
				<!--	<div class="col-sm-6" style="width:40% ">
						<label class="control-label" style="width:100%" for="templatedownload">Note: Please do not change template filename and its header containt.</label>
					</div>-->
			 		 
				</div>
				
				<% if(controllerFiles !=null && controllerFiles.size()>0){%>
				<input type="hidden" name="noController" id="noController" value="<%=controllerFiles.size()%>"> 
				<div id="mainController" class="col-xs-12 form-group" style="overflow-x: scroll;">
				<div class="container">
				    <div class="row clearfix">
				    
						<div class="col-md-12 column">
				
				<table class="table table-bordered table-hover" id="tab_logic" title="mainTableController">
						<thead>
							<tr>
								<th>GroupName</th>
								<th>TestCaseID</th>
								<th>Test_Description</th>
								<th>ExecuteFlag</th>
								<th></th>
								<th>Header1</th>
								<th>Header2</th>
								<th>Header3</th>
								<th>Header4</th>
								<th>Header5</th>
								<th>Header6</th>
								<th>Header7</th>
								<th>Header8</th>
								<th>Header9</th>
								<th>Header10</th>
							</tr>
						</thead>
						<tbody>
				
				<% for(int i=0 ; i<controllerFiles.size();i++){
				%>
					
						<% MainControllerFile controllerFile = controllerFiles.get(i);%>
							<tr id='addr<%=i%>'>
								<td><%=controllerFile.getGroupName()%></td>
								<td><%=controllerFile.getTestcaseid() %></td>
								<td><%=controllerFile.getTest_description() %></td>
								<td><%=controllerFile.getExecute_flag() %></td>
								<td><%=controllerFile.getAction() %></td>
								<td><%=controllerFile.getHeader1() %></td>
								<td><%=controllerFile.getHeader2() %></td>
								<td><%=controllerFile.getHeader3() %></td>
								<td><%=controllerFile.getHeader4() %></td>
								<td><%=controllerFile.getHeader5() %></td>
								<td><%=controllerFile.getHeader6() %></td>
								<td><%=controllerFile.getHeader7() %></td>
								<td><%=controllerFile.getHeader8() %></td>
								<td><%=controllerFile.getHeader9() %></td>
								<td><%=controllerFile.getHeader10() %></td>
							</tr>
							
						
					<%} %>
					</tbody>

					</table>
					
							</div>
							</div>
							<a id="add_row" class="btn btn-default pull-left margin-15 button-color">Add Row</a><a id='delete_row' class="pull-left btn btn-default margin-15 button-color">Delete Row</a><a id="save_rows" class="btn btn-default pull-left margin-15 button-color">Save</a><a  title="save_continue_rows"  href ="#" class="btn btn-default pull-left margin-15 button-color">Save & Continue</a>
						</div>
					</div>
			<%} %>	
			
			
			<!-- Add here for Mapping -->
				<% if(mappingFiles !=null && mappingFiles.size()>0){%>
				<form id="mappingform" name="mappingform" action="./itafpack.do" method="post">
				<input type="hidden" name="noMapping" id="noMapping" value="<%=mappingFiles.size()%>"/>
				<input type="hidden" name="savemap" id="savemap" value=""/>
				<input type="hidden" name="parentprojectname" id="parentprojectname" value="${currentProject}"/>
				<div id="mappingInput" class="col-xs-12 form-group" style="overflow-y: scroll;">
				<div class="container">
				    <div class="row clearfix">
						<div class="col-md-12 column">
				
				<table class="table table-bordered table-hover table-width-80" id="tab_logic_map" title="mappingInputTable">
						<thead>
							<tr>
								<th>TransactionCode</th>
								<th>TransactionType</th>
								<th>DirPath</th>
								<th>InputSheet</th>
								
							</tr>
						</thead>
						<tbody>
				
				<% for(int i=0 ; i<mappingFiles.size();i++){
				%>
					
						<% TransactionMappingFile mappingFile = mappingFiles.get(i);%>
							<tr id='addrmap<%=i%>'>
								<td>
								<input name="transcode<%=i%>" type='text' placeholder='TransactionCode' class='form-control input-md' value="<%=mappingFile.getTransactionCode()%>" />
								</td>
								<td><input  name="transtype<%=i%>" type='text' placeholder='TransactionType'  class='form-control input-md' value="<%=mappingFile.getTranscationType() %>" readonly="readonly">
								
								</td>
								<td>
								<input  name="dirpath<%=i%>" type='text' placeholder='DirPath'  class='form-control input-md' value="<%=mappingFile.getDirPath() %>">
								
								</td>
								<td><input  name="inputsheet<%=i%>" type='text' placeholder='InputSheet'  class='form-control input-md' value="<%=mappingFile.getInputSheet() %>">
								</td>
								
							</tr>
							
							
							
							
							
						
					<%} %>
					</tbody>

					</table>
					
							</div>
							</div>
							<a name="save_rows_map" href="#save_rows_map" class="btn btn-default pull-left margin-15 button-color">Save</a>
							<a name="save_continue_rows_map" href="#save_continue_rows_map" class="btn btn-default pull-left margin-15 button-color">Save & Continue</a>
							<!-- <input type="submit" id="save_rows_map" name="save_rows_map" value="Save"></input>-->
						</div>
					</div>
					</form> 
			<%} %>	
			
			<!-- end here for mapping -->
			<!-- Start InputStructureMapping for mapping -->
			<form id="structureform" name="structureform" action="./itafpack.do" method="post">
				<input type="hidden" name="noStrucure" id="noStrucure" value=""/>
				<input type="hidden" name="savestructure" id="savestructure" value=""/>
				<input type="hidden" name="parentprojectname" id="parentprojectname" value="${currentProject}"/>
				<div id="inputStructure" class="col-xs-12 form-group" style="overflow-x: scroll;">
				<div class="container">
				    <div class="row clearfix">
						<div class="col-md-12 column">
				<% if(mappingFiles!=null &&  (mappingFiles.get(0).getInputSheet()!=null && !mappingFiles.get(0).getInputSheet().equals(""))){%>
				<div class="row col-xs-5 padding-bottom-5">	
					<select class="form-control" placeholder="Select Transaction  Input File"
								id="transinputfile" name="transinputfile">
					<%for(int i=0 ; i<mappingFiles.size();i++){%>
								<% TransactionMappingFile mappingFile = mappingFiles.get(i);%>
								<option value="<%=mappingFile.getDirPath()+"//"+mappingFile.getInputSheet()%>"<%if((String) request.getAttribute("structruFileName")!= null && (mappingFile.getDirPath()+"//"+mappingFile.getInputSheet()).equalsIgnoreCase((String) request.getAttribute("structruFileName"))) {%> selected <%} %>><%=mappingFile.getInputSheet()%></option>
								<%} %>
					</select>
				</div>
				<%} %>
				<table class="table table-bordered table-hover" id="tab_logic_strucure_1" title="inputStructurTable">
						<thead>
							<tr>
								<th>ExecuteFlag</th>
								<th style="width:250px">Action</th>
								<th>LogicalName</th>
								<th>ControlName</th>
								<th>ControlType</th>
								<th>ControlID</th>
								<th>ImageType</th>
								<th>Index</th>
								<th>DynamicIndex</th>
								<th>ColumnName</th>
								<th>RowNo</th>
								<th>ColumnNo</th>
							</tr>
						</thead>
						<tbody id="tab_logic_strucure_body">
				
						<%if(structureFiles!=null && structureFiles.size()>0) {%>
						<%for(int i=0 ; i<structureFiles.size();i++){ %>
						<%TransactionStructureFile structureFile = structureFiles.get(i); %>
								<tr id='addrstructure<%=i+1%>'><input type="hidden" name="indexid" id="indexid" value="<%=i+1%>"/>
								<td>
								
								<select class="form-control" placeholder="Select Execution Flag"
									id="structexeflag<%=i+1%>" name="structexeflag<%=i+1%>">
									<option value="Y" <% if (structureFile.getExecuteflag().equals("Y")){%>selected <%}%>>Y</option>
									
									<option value="N" <% if (structureFile.getExecuteflag().equals("N")){%>selected <%}%>>N</option>
									
									
								</select>
								
								</td>
								<td>
								
								<select class="form-control"placeholder="Select Action"
									id="structaction<%=i+1%>" name="structaction<%=i+1%>">
									<option value="Read" <% if (structureFile.getAction().equals("Read")){%>selected <%}%>>Read</option>
									<option value="I" <% if (structureFile.getAction().equals("I")){%>selected <%}%>>I</option>
									<option value="NC" <% if (structureFile.getAction().equals("NC")){%>selected <%}%>>NC</option>
									<option value="V" <% if (structureFile.getAction().equals("V")){%>selected <%}%>>V</option>
									<option value="Read" <% if (structureFile.getAction().equals("Read")){%>selected <%}%>>Read</option>
									<option value="Write" <% if (structureFile.getAction().equals("Write")){%>selected <%}%>>Write</option>
								</select>
								</td>
								<td>
								<input  name="structlogic<%=i+1%>" type='text' placeholder='LogicalName'  class='form-control input-md' value="<%=structureFile.getLogicalname()%>">
								
								</td>
								<td><input  name="structcontol<%=i+1%>" type='text' placeholder='ControlName'  class='form-control input-md' value="<%=structureFile.getControlname()%>">
								</td>
								<td>
								
								<select class="form-control" placeholder="Select ControlType"
									id="structcontrolType<%=i+1%>" name="structcontrolType<%=i+1%>">
									<option value="WebEdit" <% if (structureFile.getControltype().equals("WebEdit")){%>selected <%}%>>WebEdit</option>
									<option value="WebList" <% if (structureFile.getControltype().equals("WebList")){%>selected <%}%>>WebList</option>
									<option value="WebButton" <% if (structureFile.getControltype().equals("WebButton")){%>selected <%}%>>WebButton</option>
									<option value="CheckBox" <% if (structureFile.getControltype().equals("CheckBox")){%>selected <%}%>>CheckBox</option>
									<option value="Radio" <% if (structureFile.getControltype().equals("Radio")){%>selected <%}%>>Radio</option>
									<option value="WebElement" <% if (structureFile.getControltype().equals("WebElement")){%>selected <%}%>>WebElement</option>
									<option value="WebTable" <% if (structureFile.getControltype().equals("WebTable")){%>selected <%}%>>WebTable</option>
									<option value="Browser" <% if (structureFile.getControltype().equals("Browser")){%>selected <%}%>>Browser</option>
									<option value="Alert" <% if (structureFile.getControltype().equals("Alert")){%>selected <%}%>>Alert</option>
									<option value="Robot" <% if (structureFile.getControltype().equals("Robot")){%>selected <%}%>>Robot</option>
									<option value="Window" <% if (structureFile.getControltype().equals("Window")){%>selected <%}%>>Window</option>
									<option value="URL" <% if (structureFile.getControltype().equals("URL")){%>selected <%}%>>URL</option>
									<option value="JSScript" <% if (structureFile.getControltype().equals("JSScript")){%>selected <%}%>>JSScript</option>
									<option value="ListBox" <% if (structureFile.getControltype().equals("ListBox")){%>selected <%}%>>ListBox</option>
									<option value="Slider" <% if (structureFile.getControltype().equals("Slider")){%>selected <%}%>>Slider</option>
									<option value="MaskedInputDate" <% if (structureFile.getControltype().equals("MaskedInputDate")){%>selected <%}%>>MaskedInputDate</option>
									<option value="ActionClick" <% if (structureFile.getControltype().equals("ActionClick")){%>selected <%}%>>ActionClick</option>
									<option value="ActionMouseOver" <% if (structureFile.getControltype().equals("ActionMouseOver")){%>selected <%}%>>ActionMouseOver</option>
									<option value="IFrame" <% if (structureFile.getControltype().equals("IFrame")){%>selected <%}%>>IFrame</option>
									<option value="Capture" <% if (structureFile.getControltype().equals("Capture")){%>selected <%}%>>Capture</option>
									<option value="Wait" <% if (structureFile.getControltype().equals("Wait")){%>selected <%}%>>Wait</option>
								</select>
								</td>
								<td>
								<select class="form-control" placeholder="Select ControlID"
									id="structcontrolid<%=i+1%>" name="structcontrolid<%=i+1%>">
									<option value="" <% if (structureFile.getControlid().equals("")){%>selected <%}%>>Select</option>
									<option value="Id" <% if (structureFile.getControlid().equals("Id")){%>selected <%}%>>Id</option>
									<option value="Name" <% if (structureFile.getControlid().equals("Name")){%>selected <%}%>>Name</option>
									<option value="TagName" <% if (structureFile.getControlid().equals("TagName")){%>selected <%}%>>TagName</option>
									<option value="ClassName" <% if (structureFile.getControlid().equals("ClassName")){%>selected <%}%>>ClassName</option>
									<option value="XPath" <% if (structureFile.getControlid().equals("XPath")){%>selected <%}%>>XPath</option>
									<option value="AjaxPath" <% if (structureFile.getControlid().equals("AjaxPath")){%>selected <%}%>>AjaxPath</option>
									<option value="LinkText" <% if (structureFile.getControlid().equals("LinkText")){%>selected <%}%>>LinkText</option>
									<option value="HTMLID" <% if (structureFile.getControlid().equals("HTMLID")){%>selected <%}%>>HTMLID</option>
									<option value="CSSSelector" <% if (structureFile.getControlid().equals("CSSSelector")){%>selected <%}%>>CSSSelector</option>
									<option value="Id_p" <% if (structureFile.getControlid().equals("Id_p")){%>selected <%}%>>Id_p</option>
									<option value="XPath_p" <% if (structureFile.getControlid().equals("XPath_p")){%>selected <%}%>>XPath_p</option>
								</select>
								</td>
								<td><input  name="structimage<%=i+1%>" type='text' placeholder='ImageType'  class='form-control input-md' value="<%=structureFile.getImagetype()%>">
								</td>
								<td><input  name="structindex<%=i+1%>" type='text' placeholder='Index'  class='form-control input-md' value="<%=structureFile.getIndex()%>">
								</td>
								<td><input  name="structdyindex<%=i+1%>" type='text' placeholder='DynamicIndex'  class='form-control input-md' value="<%=structureFile.getDynamicindex()%>">
								</td>
								<td><input  name="structcolname<%=i+1%>" type='text' placeholder='ColumnName'  class='form-control input-md' value="<%=structureFile.getColumnname()%>">
								</td>
								<td><input  name="structrow<%=i+1%>" type='text' placeholder='RowNo'  class='form-control input-md' value="<%=structureFile.getRowno()%>">
								</td>
								<td><input  name="structcol<%=i+1%>" type='text' placeholder='ColumnNo'  class='form-control input-md' value="<%=structureFile.getColumnno()%>">
								</td>
							</tr>
							
						<% }}else{%>
							<tr id='addrstructure1'><input type="hidden" name="indexid" id="indexid" value="1"/>
								<td>
								
								<select class="form-control" placeholder="Select Execution Flag"
									id="structexeflag1" name="structexeflag1">
									<option value="Y">Y</option>
									<option value="N">N</option>
								</select>
								
								</td>
								<td>
								
								<select class="form-control"placeholder="Select Action"
									id="structaction1" name="structaction1">
									<option value="Read">Read</option>
									<option value="I">I</option>
									<option value="NC">NC</option>
									<option value="V">V</option>
									<option value="Read">Read</option>
									<option value="Write">Write</option>
								</select>
								</td>
								<td>
								<input  name="structlogic1" type='text' placeholder='LogicalName'  class='form-control input-md' value="">
								
								</td>
								<td><input  name="structcontol1" type='text' placeholder='ControlName'  class='form-control input-md' value="">
								</td>
								<td>
								
								<select class="form-control" placeholder="Select ControlType"
									id="structcontrolType1" name="structcontrolType1">
									<option value="WebEdit">WebEdit</option>
									<option value="WebList">WebList</option>
									<option value="WebButton">WebButton</option>
									<option value="CheckBox">CheckBox</option>
									<option value="Radio">Radio</option>
									<option value="WebElement">WebElement</option>
									<option value="WebTable">WebTable</option>
									<option value="Browser">Browser</option>
									<option value="Alert">Alert</option>
									<option value="Robot">Robot</option>
									<option value="Window">Window</option>
									<option value="URL">URL</option>
									<option value="JSScript">JSScript</option>
									<option value="ListBox">ListBox</option>
									<option value="Slider">Slider</option>
									<option value="MaskedInputDate">MaskedInputDate</option>
									<option value="ActionClick">ActionClick</option>
									<option value="ActionMouseOver">ActionMouseOver</option>
									<option value="IFrame">IFrame</option>
									<option value="Capture">Capture</option>
									<option value="Wait">Wait</option>
								</select>
								</td>
								<td>
								<select class="form-control" placeholder="Select ControlID"
									id="structcontrolid1" name="structcontrolid1">
									<option value="">Select</option>
									<option value="Id">Id</option>
									<option value="Name">Name</option>
									<option value="TagName">TagName</option>
									<option value="ClassName">ClassName</option>
									<option value="XPath">XPath</option>
									<option value="AjaxPath">AjaxPath</option>
									<option value="LinkText">LinkText</option>
									<option value="HTMLID">HTMLID</option>
									<option value="CSSSelector">CSSSelector</option>
									<option value="Id_p">Id_p</option>
									<option value="XPath_p">XPath_p</option>
								</select>
								</td>
								<td><input  name="structimage1" type='text' placeholder='ImageType'  class='form-control input-md' value="">
								</td>
								<td><input  name="structindex1" type='text' placeholder='Index'  class='form-control input-md' value="">
								</td>
								<td><input  name="structdyindex1" type='text' placeholder='DynamicIndex'  class='form-control input-md' value="">
								</td>
								<td><input  name="structcolname1" type='text' placeholder='ColumnName'  class='form-control input-md' value="">
								</td>
								<td><input  name="structrow1" type='text' placeholder='RowNo'  class='form-control input-md' value="">
								</td>
								<td><input  name="structcol1" type='text' placeholder='ColumnNo'  class='form-control input-md' value="">
								</td>
							</tr>
							
							<%} %>
							
							
							
						
					
					</tbody>

					</table>
					
							</div>
							</div>
							<!-- <a id="save_rows_map" class="btn btn-default pull-left" name="save_rows_map" value="mappingSave">Save</a>-->
							<!-- <a id="add_row_structure" class="btn btn-default pull-left">Add Row</a><input type="submit" id="save_rows_str" name="save_rows_str" value="Save">Save</input><a id='delete_row_strucure' class="pull-left btn btn-default">Delete Row</a>-->
							<a id="add_row_structure" class="btn btn-default pull-left margin-15 button-color">Add Row</a><a id='delete_row_strucure' class="pull-left btn btn-default margin-15 button-color">Delete Row</a><a class="btn btn-default pull-left margin-15 button-color" href="#save_rows_str" name="save_rows_str">Save</a>
							<a class="btn btn-default pull-left margin-15 button-color" href="#save_continue_rows_str" name="save_continue_rows_str">Save & Continue</a>
							
						</div>
					</div>
			
			</form>
			
			<!-- end InputStructureMapping for mapping -->
			<!-- Start Inputstructur DataMapping start  -->
			
			<!-- Start Inputstructur DataMapping end  -->
		
			</div>
			  <!-- Project  template file import /export section end -->
			 <div id="main5" >
			  	<form id="structureDataform" name="structureDataform" action="./itafpack.do" method="post">
				<input type="hidden" name="noStrucuredata" id="noStrucuredata" value=""/>
				<input type="hidden" name="parentprojectname" id="parentprojectname" value="${currentProject}"/>
				<input type="hidden" name="savestructuredata" id="savestructuredata" value=""/>
				<div id="inputStructure" class="col-xs-12 form-group" style="overflow-y: scroll;">
				<div class="container">
				    <div class="row clearfix">
				    
				    <%if(((String)request.getAttribute("currentProject"))!=null &&((String)request.getAttribute("currentProject")).length()>0) {%>
					<label class="col-sm-2" for="burl" id="configProject">${currentProject}</label>
					<%}else{%>
						<label class="col-sm-2" for="burl" id="configProject"><font color=red>Project Details required.</font></label>
					<%} %>
				    	<div class="col-md-12 column">
				    	<% if(mappingFiles!=null &&  (mappingFiles.get(0).getInputSheet()!=null && !mappingFiles.get(0).getInputSheet().equals(""))){%>
							<div class="row col-xs-5 padding-bottom-5">	
							<select class="form-control" placeholder="Select Transaction  Input File"
										id="transDatainputfile" name="transDatainputfile" >
							<%for(int i=0 ; i<mappingFiles.size();i++){%>
										<% TransactionMappingFile mappingFile = mappingFiles.get(i);%>
										<option value="<%=mappingFile.getDirPath()+"//"+mappingFile.getInputSheet()%>"<%if((String) request.getAttribute("structruFileName")!= null && (mappingFile.getDirPath()+"//"+mappingFile.getInputSheet()).equalsIgnoreCase((String) request.getAttribute("structruFileName"))) {%> selected <%} %>><%=mappingFile.getInputSheet()%></option>
										<%}%>
							</select>
							</div>
							<%} %>
				    		<table class="table table-bordered table-hover table-width-80" id="tab_logic_strucure_data" title="inputStructurdataTable">
								<thead>
									<tr>
										
										
								
									</tr>
								</thead>
								
								<tbody id="tab_logic_strucure_data_body">
								<%
								String testcaseId= null;
								String transactionType= null;
								
								if(structureDatas !=null && structureDatas.size()>0){ 
									testcaseId = structureDatas.get(0).getTestcaseid();
									transactionType = structureDatas.get(0).getTransactiontype();
								
								}
								if(reqstructureDatas !=null){
									testcaseId = reqstructureDatas.get(0).getTestcaseid();
								}
								
								
								%>
								
								<tr>
									<td>TestCaseID</td>
									<td><input  name="dtestcaseid1" type='text' placeholder='ImageType'  class='form-control input-md' value="<%=testcaseId%>">
									<%if(structureDatas !=null && structureDatas.size()>0){
										%>
									<select name="dtestcaseid1" class="form-control">
									<%for(TransactionStructureData structureData:structureDatas){ %>
										<option <% if(testcaseId!=null && testcaseId.equalsIgnoreCase(structureData.getTestcaseid())){%> selected <%} %>><%=structureData.getTestcaseid()%></option>
									<%} %>
									</select>
									<%} %>
									
									</td>
								</tr>
								<tr>	
									<td>TransactionType</td>
									<td><input  name='dtransactiontype1' type='text' placeholder='Index'  class='form-control input-md' value='<%=transactionType%>'>						</td>
								</tr>
							
								<% int j=0;
								if(reqstructureDatas !=null){
									for(TransactionStructureData reqstructureData:reqstructureDatas){
								%>
							
								<%if(!reqstructureData.getLogicalname().isEmpty()){ %>
									<tr>
										<td><input  name="dlogicalname<%=j+1%>" type='text' placeholder='DynamicIndex'  readonly="readonly"  class='form-control input-md' value="<%=reqstructureData.getLogicalname()%>"></td>
										<td><input  name="ddatavalue<%=j+1%>" type='text' placeholder='DynamicIndex'  class='form-control input-md' value="<%=reqstructureData.getValue()%>">
										</td>
									</tr>	
									<%} %>
								<%j++;
									}}%>
								
								</tbody>
						</table>
				    	
				    	</div>
				    </div>
				    
				    </div>
				    		<div class="col-md-12 column">
				    		 <a href="#" title = "save_rows_back_strData" class="btn btn-default pull-left margin-15 button-color" name="save_rows_back_strData" >Back</a>
							 <a href="#save_rows_strData" class="btn btn-default pull-left margin-15 button-color" name="save_rows_strData" >Save</a>
							  <a href="#add_rows_strData" class="btn btn-default pull-left margin-15 button-color" name="add_rows_strData" >Add</a>
							  <a href="#edit_rows_strData" class="btn btn-default pull-left margin-15 button-color" name="edit_rows_strData" >Edit</a>
							 </div>
							<!-- <input type="submit" id="save_rows_strData" name="save_rows_strData" value="Save">Save</input>-->
							
													
						</div>
						
				</div>

				
				<script src="./js/jquery.min.js"></script>
	<!-- Bootstrap Core JavaScript -->
	<script src="./js/bootstrap.min.js"></script>
	<script src="./js/demo3/try.js"></script>
	
    <script type="text/javascript" src="./js/demo3/bootstrap-filestyle.min.js"> </script>
	<script type="text/javascript">
	
		
		
		$(":file").filestyle({icon: false,size: "nr",buttonName: "btn-primary"});
		
			</script>
<!--  <script src="./js/demo4/mindmup-editabletable.js"></script>-->
				
				<div id="main6">
				<%if((String) request.getAttribute("currentProject")!=null) {%>
				    <%String dashboardurl=baseURL +"files/advancedReport.html?projectname="+(String) request.getAttribute("currentProject")+"&actionType=execute";  %>
					<iframe  src= "<%=dashboardurl%>" id='inneriframe' scrolling=yes style="overflow:hidden;height:100vh;width:100vw" height="100%" width="100%"></iframe>
				<%} %>	
				</div>
				</div>
			
				</div >
				
				
				
			 <!-- footer section start -->
			
			
		 
			<div class="col-md-12 footer" >
				<ul class="nav" style="">
					<li><a href="" style="padding-left: 0px; padding-right: 0px; color:black;">Copyright
							2016. All rights reserved with MAJESCO Automation Testing COE </a></li>
				</ul>
			</div>
			<!-- footer section end -->

	

</body>
</html>