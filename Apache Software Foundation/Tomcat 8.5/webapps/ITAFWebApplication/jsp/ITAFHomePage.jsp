<%@page import="com.majesco.itaf.dao.ProjectPack"%>
<%@page import="com.majesco.itaf.dao.ConfigFile"%>
<%@page import="com.majesco.itaf.dao.TransactionStructureData"%>
<%@page import="com.majesco.itaf.dao.TransactionStructureFile"%>
<%@page import="com.majesco.itaf.dao.TransactionMappingFile"%>
<%@page import="java.util.List"%>
<%@page import="com.majesco.itaf.dao.MainControllerFile"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ITAF Home</title>
<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
<!-- <link href="./css/demo4/bootstrap.css" rel="stylesheet">-->
<link rel="stylesheet" type="text/css" href="./css/demo3/try.css">

<!-- <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet"> -->

</head>
 
<body>

<%
String url = request.getRequestURL().toString();
System.out.println(url);
		
String baseURL = url.substring(0, url.length() - request.getRequestURI().length()) + request.getContextPath() + "/";
System.out.println(baseURL);		
List<MainControllerFile> controllerFiles =(List<MainControllerFile>) request.getAttribute("currentProMainController");
String subMenu =(String) request.getAttribute("subItem");
List<TransactionMappingFile>  mappingFiles =(List<TransactionMappingFile>) request.getAttribute("currentProTransMapping");
//List<TransactionStructureFile>  structureFiles =(List<TransactionStructureFile>) request.getAttribute("currentProTranStrdefault");
List<TransactionStructureData>  structureDatas =(List<TransactionStructureData>) request.getAttribute("currentProTranStrDatadefault");
List<TransactionStructureData>  reqstructureDatas =(List<TransactionStructureData>) request.getAttribute("requestedStrData");		

%>

<input type="hidden" id="hidReqprojectname" value="${currentProject}" />
<input type="hidden" id="hidmenuitem" value="${menuItem}" />
<input type="hidden" id="hidsubitem" value="${subItem}" />
<input type="hidden" id="hidconfFlag" value="${configFlag}" />
<!-- Top Menu start -->
	<div id="wrapper">
	 <jsp:include page="menuHeader.jsp" />
	
<!-- Top Menu end -->
<!-- Top  vertical Menu start -->
		<!-- sidebar start -->
		<jsp:include page="createPackMenu.jsp" />
		<!-- Top  vertical Menu start End -->
		<!-- Home Page default section start -->
		
		<div id="main-wrapper" class="col-md-10 pull-right"	>
		
			<div id="main1"  style ="display: none;">
				
				<div class="page-header" style="margin: 0px 0 20px;">
					<h3>!! Welcome !!</h3>
				</div>
				<p> Automation is often seen as a panacea that reduces
					testing time and increases quality. But writing and modifying
					automation scripts adds significantly to the cost and time incurred
					while testing. Majesco has built a unique hybrid automation
					framework – Insurance Test Automation Framework (iTAF) - that
					enables rapid implementation of automation without adding
					significantly to the costs.</p>
					
					<div>
							<div class="col-md-12 column">
				    		 <a href="#startnav" title = "startnav" class="btn btn-default pull-right margin-15 button-color" name="startnav" >Get Started</a>
				    		 </div>

					</div>
					
					
			</div>
			<!-- Home Page default section end -->
			<!-- Project Creation/update/modify section start -->
			
			
			<div id="main2" style ="display: none;" >
				
				<jsp:include page="createProjectPack.jsp"></jsp:include>
				
			</div>
			

				<!-- Project Creation/update/modify section end -->
		 <!-- Project  Config Creation/update/modify section start -->
			<div id="main3" style ="display: none;" >
			
			  	 <div class="col-xs-12">
						<c:choose> 
						
						 <c:when test="${subItem == '#sconfig' and not empty msgprojectnotexist and empty currentProject}">
									<div class="alert alert-danger">
								    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
								    <strong>Error!</strong> <c:out value="${msgprojectnotexist}"></c:out>.
								 </div>	
						  </c:when>
						  <c:when test="${subItem == '#sconfig' and not empty msgprojectsuccess}">  
					      <div class="alert  alert-success">
							    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							    <strong>Success!</strong> <c:out value="${msgprojectsuccess}"></c:out>
							 </div>	  
					    </c:when>
					    
					    <c:when test="${empty currentProject}">  
					      <div class="alert  alert-warning">
							    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							    <strong>Warning!</strong> Project Details required.
							 </div>	  
					    </c:when>  
					  
					    <c:otherwise>  
					       <span class = "label label-primary">Project Name - ${currentProject}</span>
					    </c:otherwise>  
						</c:choose>  
					</div>
				
			    <jsp:include page="projectConfig.jsp"></jsp:include>
			</div>
			 <!-- Project  Config Creation/update/modify section end -->
			 <!-- Project  template file import /export section start -->
			<div id="main4" style ="display: none;">
			 <div class="col-xs-12">
				
				<c:choose> 
					 <c:when test="${subItem == '#cstructure' and not empty msgprojectnotexist and not empty currentProject}">
								<div class="alert alert-danger">
							    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							    <strong>Error!</strong> <c:out value="${msgprojectnotexist}"></c:out>.
							 </div>	
					  </c:when>
					  <c:when test="${subItem == '#cstructure' and  not empty msgprojectsuccess}">  
				      <div class="alert  alert-success">
						    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						    <strong>Success!</strong> <c:out value="${msgprojectsuccess}"></c:out>
						 </div>	  
				    </c:when>     
				    <c:when test="${empty currentProject}">  
				      <div class="alert  alert-warning">
						    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						    <strong>Warning!</strong> Project Details required.
						 </div>	  
				    </c:when>  
				  
				    <c:otherwise>  
				      <span class = "label label-primary">Project Name - ${currentProject}</span>
				    </c:otherwise>  
				</c:choose>  
			</div>
			
			
			<!-- Add Create Structure -->
			<div id="createStructureSheet" class="margintop15">
				<jsp:include page="createStructureSheet.jsp"></jsp:include>
			</div>
			<!-- Add Create Structure -->
			<!-- Start Structure Configuration for mapping -->
			<div id= "structurSheetPreparation">
				<jsp:include page="structurSheetPreparation.jsp" />
			</div>		
			<!-- end Structure Configuration for mapping -->
			
					
			</div>
			
			
			<!-- Project  template file import /export section start -->
			<div id="main9" style ="display: none;">
			 <div class="col-xs-12">
				
				<c:choose> 
					 <c:when test="${subItem == '#scenario' and not empty msgprojectnotexist and not empty currentProject}">
								<div class="alert alert-danger">
							    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							    <strong>Error!</strong> <c:out value="${msgprojectnotexist}"></c:out>.
							 </div>	
					  </c:when>
					  <c:when test="${subItem == '#scenario' and not empty msgprojectsuccess}">  
				      <div class="alert  alert-success">
						    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						    <strong>Success!</strong> <c:out value="${msgprojectsuccess}"></c:out>
						 </div>	  
				    </c:when>     
				    <c:when test="${empty currentProject}">  
				      <div class="alert  alert-warning">
						    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						    <strong>Warning!</strong> Project Details required.
						 </div>	  
				    </c:when>  
				  
				    <c:otherwise>  
				      <span class = "label label-primary">Project Name - ${currentProject}</span>
				    </c:otherwise>  
				</c:choose>  
			</div>
			<!-- Start MainController -->						
			<div id="mainControllerPreparation" style="padding-top:25px;display: none;" class="margintop15">
				<jsp:include page="mainContoller.jsp" />
			</div>		
			
				
			<!-- end mainController -->
			
			</div>
			
			
			
			
			
			  <!-- Project  template file import /export section end -->
			 <div id="main5" style ="display: none;" >
				 <div class="col-xs-12">
				
				<c:choose> 
					 <c:when test="${subItem == '#cipdata' and not empty msgprojectnotexist and not empty currentProject}">
								<div class="alert alert-danger">
							    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							    <strong>Error!</strong> <c:out value="${msgprojectnotexist}"></c:out>.
							 </div>	
					  </c:when>
					  <c:when test="${subItem == '#cipdata' and not empty msgprojectsuccess}">  
				      <div class="alert  alert-success">
						    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						    <strong>Success!</strong> <c:out value="${msgprojectsuccess}"></c:out>
						 </div>	  
				    </c:when>     
				    <c:when test="${empty currentProject}">  
				      <div class="alert  alert-warning">
						    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						    <strong>Warning!</strong> Project Details required.
						 </div>	  
				    </c:when>  
				  
				    <c:otherwise>  
				      <span class = "label label-primary">Project Name - ${currentProject}</span>
				    </c:otherwise>  
				</c:choose>  
			</div>
			
						<jsp:include page="structurDataValueSheet.jsp"></jsp:include>
					
			</div>
		
			<div id="main7" style ="display: none;">
				 <div class="col-xs-12">
				
				<c:choose> 
					 <c:when test="${subItem == '#vtemplate' and not empty msgprojectnotexist and not empty currentProject}">
								<div class="alert alert-danger">
							    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							    <strong>Error!</strong> <c:out value="${msgprojectnotexist}"></c:out>.
							 </div>	
					  </c:when>
					  <c:when test="${subItem == '#vtemplate' and not empty msgprojectsuccess}">  
				      <div class="alert  alert-success">
						    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						    <strong>Success!</strong> <c:out value="${msgprojectsuccess}"></c:out>
						 </div>	  
				    </c:when>     
				    <c:when test="${empty currentProject}">  
				      <div class="alert  alert-warning">
						    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						    <strong>Warning!</strong> Project Details required.
						 </div>	  
				    </c:when>  
				  
				    <c:otherwise>  
				      <span class = "label label-primary">Project Name - ${currentProject}</span>
				    </c:otherwise>  
				</c:choose>  
			</div>
					<jsp:include page="verificationstructuresheet.jsp"></jsp:include>
				
			</div>
			<div id="main8" style ="display: none;">
					
				verify structure details
				</div>
				
	<script src="./js/jquery.min.js"></script>
	<!-- Bootstrap Core JavaScript -->
	<script src="./js/bootstrap.min.js"></script>
	<script src="./js/demo3/try.js"></script>
	
	<script src="./js/demo3/searchTable.js"></script>
	<script src="./js/demo3/RowSorter.js"></script>	
	<script src="./js/demo3/responsiveTables.js"></script>
    <script type="text/javascript" src="./js/demo3/bootstrap-filestyle.min.js"> </script>
    <script src="./js/objectCap.js"></script>
	<script type="text/javascript">
	
		
		
		$(":file").filestyle({icon: false,size: "nr",buttonName: "btn-primary"});
		
		
	</script>
<!--  <script src="./js/demo4/mindmup-editabletable.js"></script>-->
				
				<div id="main6" style ="display: none;" >
			
				<c:choose> 
					 <c:when test="${subItem == '#dashboard' and not empty msgprojectnotexist and not empty currentProject}">
								<div class="alert alert-danger">
							    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							    <strong>Error!</strong> <c:out value="${msgprojectnotexist}"></c:out>.
							 </div>	
					  </c:when>
					  <c:when test="${subItem == '#dashboard' and not empty msgprojectsuccess}">  
				      <div class="alert  alert-success">
						    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						    <strong>Success!</strong> <c:out value="${msgprojectsuccess}"></c:out>
						 </div>	  
				    </c:when>     
				    <c:when test="${empty currentProject}">  
				      <div class="alert  alert-warning">
						    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						    <strong>Warning!</strong> Project Details required.
						 </div>	  
				    </c:when>  
				  
				    <c:otherwise>  
				      <span class = "label label-primary">Project Name - ${currentProject}</span>
				    	 <div class="margintop5">
				    	  <iframe  src= "<c:out value="${baseURL}files/advancedReport.html?projectname=${currentProject}&actionType=execute"></c:out>" id='inneriframe' scrolling=yes style="overflow:hidden;height:100vh;width:100vw" height="100%" width="100%"></iframe>
				      	</div>
				    </c:otherwise>  
				</c:choose>  
				
				
				
				
				
				</div>
				
				
				</div>
			
			</div>
				<!-- </div > -->
	<!--  Bhaskar changes for qTEst START -->
				<div id="main10" style ="display: none;"  >
				
				<jsp:include page="qTestNavigationMenu.jsp"></jsp:include>
				
			    </div>
			     <div  class="col-md-10 pull-right">  	
				
				<div id="main11" style ="display: none;"  >
				
				<jsp:include page="satz.jsp"></jsp:include>
				
			    </div>
			    	
			    </div>
	<!--  Bhaskar changes for qTEst END -->
			    	
	<!--  Bhaskar changes for Jenkins START -->
			    <div id="main12" style ="display: none;"  >
				
				<jsp:include page="jenkinsconfig.jsp"></jsp:include>
				
			    </div>
	<!--  Bhaskar changes for Jenkins END -->
				
			 <!-- footer section start -->
			
			
		 
			<jsp:include page="footer.jsp" />
			<!-- footer section end -->

	

</body>
</html>