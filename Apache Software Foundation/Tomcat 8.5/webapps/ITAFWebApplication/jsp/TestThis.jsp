<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">

<link href="./css/demo4/bootstrap.css" rel="stylesheet">


</head>
<body>

<table id="mainTable" class="table table-striped">
<thead>
<tr>
<th>GroupName</th>
<th>TestCaseID</th>
<th>Test_Description</th>
<th>ExecuteFlag</th>
<th>Header1</th>
<th>Header2</th>
<th>Header3</th>
<th>Header4</th>
<th>Header5</th>
<th>Header6</th>
<th>Header7</th>
<th>Header8</th>
<th>Header9</th>
<th>Header10</th>

</tr>
</thead>
<tbody>
<tr>
<td>Car</td>
<td>100</td>
<td>200</td>
<td>0</td>
</tr>
<tr>
<td>Bike</td>
<td>330</td>
<td>240</td>
<td>1</td>
</tr>
<tr>
<td>Plane</td>
<td>430</td>
<td>540</td>
<td>3</td>
</tr>
<tr>
<td>Yacht</td>
<td>100</td>
<td>200</td>
<td>0</td>
</tr>
<tr>
<td>Segway</td>
<td>330</td>
<td>240</td>
<td>1</td>
</tr>
</tbody>
<tfoot>
<tr>
<th><strong>TOTAL</strong></th>
<th></th>
<th></th>
<th></th>
</tr>
</tfoot>
</table>

<script src="./js/jquery.min.js"></script>
<script src="./js/demo4/numeric-input-example.js"></script>

<script src="./js/demo4/mindmup-editabletable.js"></script>
<script>
$('#mainTable').editableTableWidget().numericInputExample().find('td:first').focus();
</script>
<script>
$('#mainTable').editableTableWidget({editor: $('<textarea>')}).numericInputExample().find('td:first').focus();
</script>

</body>
</html>