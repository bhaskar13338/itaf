<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
			<form id="projectMenu" name="projectMenu" method="post">
			<div id="sidebar-wrapper" class="col-md-2" style="padding-left: 5px; display: none;">
				<div id="sidebar">
					<!-- Put Your Code here -->
					<div id="MainMenu">
						<div class="list-group panel">
							<a href="#crproject" class="list-group-item list-group-item-success"
								data-toggle="collapse" data-parent="#MainMenu">Search/Create
								Project</a> 
								<!-- <a href="#moproject" class="list-group-item list-group-item-success"
								data-toggle="collapse" data-parent="#MainMenu">Modify
								Project</a>--><a href="#sconfig"
								class="list-group-item list-group-item-success"
								data-toggle="collapse" data-parent="#MainMenu">Set
								Configuration</a>
								 <a href="#cstructure"
								class="list-group-item list-group-item-success"
								data-toggle="collapse" data-parent="#MainMenu">Search/Create
								Object Screen</a> 
								<!-- <a href="#bussfunction"
								class="list-group-item list-group-item-success"
								data-toggle="collapse" data-parent="#MainMenu">Set Transaction Configuration </a>-->
								<a href="#scenario"
								class="list-group-item list-group-item-success"
								data-toggle="collapse" data-parent="#MainMenu">Scenario Workflow</a>
<!-- 								<a href="#BTFscenario"
								class="list-group-item list-group-item-success"
								data-toggle="collapse" data-parent="#MainMenu">BTF Scenario Workflow</a>  -->
								<a href="#cipdata"
								class="list-group-item list-group-item-success"
								data-toggle="collapse" data-parent="#MainMenu">Create Input
								Data</a> 
								<a href="#vtemplate"
								class="list-group-item list-group-item-success"
								data-toggle="collapse" data-parent="#MainMenu">Create
								Verification Template</a> 
								<!-- <a href="#vflow"
								class="list-group-item list-group-item-success"
								data-toggle="collapse" data-parent="#MainMenu">Verification
								Flow</a> -->
								<a href="#dashboard"
								class="list-group-item list-group-item-success"
								data-toggle="collapse" data-parent="#MainMenu">Dashboard</a>
						</div>
					</div>
					<!-- end -->
				</div>
			</div>
		</form>
</body>
</html>