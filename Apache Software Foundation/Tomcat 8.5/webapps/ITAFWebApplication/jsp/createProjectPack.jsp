<%@page import="com.majesco.itaf.dao.ProjectPack"%>
<%@page import="java.util.List"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
					
			<form  action="./itafpack.do" role="createprojform" data-toggle="validator" method="post">
				
				 <div class="col-xs-12">
				
				<c:choose> 
				
					 <c:when test="${subItem == '#moproject' and not empty msgprojectnotexist and empty currentProject}">
								<div class="alert alert-danger">
							    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							    <strong>Error!</strong> <c:out value="${msgprojectnotexist}"></c:out>
							 </div>	
					  </c:when>
					  <c:when test="${subItem == '#moproject' and not empty msgprojectsuccess}">  
				      <div class="alert  alert-success">
						    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						    <strong>Success!</strong> <c:out value="${msgprojectsuccess}"></c:out>
						 </div>	  
				    </c:when>   
				   
				    <c:when test="${empty currentProject}">  
				      <div class="alert  alert-warning">
						    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						    <strong>Warning!</strong> Project Details required.
						 </div>	  
				    </c:when>
				   
				  
				    <c:otherwise>  
				       <span class = "label label-primary">Project Name - ${currentProject}</span>
				    </c:otherwise>  
				</c:choose>  
			</div>
			
					
					<div class="col-md-12 form-group margintop15">
								
									<label class="control-label col-sm-4" for="proname" style="width:10%">Project
										Name</label>
									<div class="row">
										<div class="col-sm-4" >
											<input type="text" class="form-control" id="proname"
												name="proname" placeholder="Enter Project Name" maxlength="30"
												 value="">
										</div>
										<div class="col-sm-4" style="padding-left:0px;">
											<button type="submit" class="btn btn-default button-color" id="search1"
												name="search1" value="search">Search</button>
										</div>
									
								     </div>
						</div>

						<div class="col-md-12 form-group" id="btnCreate">
							<div class="col-sm-10" style="margin-left:105px;">
								<button type="submit" class="btn btn-default button-color" id="create"
									name="create" value="create">Create Project</button>
								<!-- <button type="submit" class="btn btn-default button-color" id="addnew"
									name="addnew" value="addnew">Add New</button>-->
								
								<!-- <button type="submit" class="btn btn-default button-color" id="continue"
									name="continue"  value="continue">Continue</button>-->
							</div>
						</div>
						
				</form>	
				<form  action="./itafpack.do" role="createprojform"  method="post">
			    <input type="hidden" name="directoryname" value=""/>
			     <input type="hidden" name="projectaction" value=""/>
			     	<c:if test="${projectFilesList != null && fn:length(projectFilesList) gt 0}">
			     	<div class="row clearfix">
				    
					<div class="col-md-12 column margin-15">
					<div class="table-responsive">
					  <table class="table table-condensed table-striped table-bordered table-hover no-margin" >
					    <thead>
					      <tr>
					        <th style="width:5%">
					          <label class="no-margin"  ></label>
					        </th>
					        <th style="width:25%">Project Name</th>
					        <th style="width:15%" class="hidden-phone">Date</th>
					        <th style="width:20%" class="hidden-phone">Created By</th>
					         <th style="width:40%" class="hidden-phone">Actions</th>
					       
					      </tr>
					     
					    </thead>
					    <tbody id="projectTable">
			     		<c:forEach items="${projectFilesList}" var="item" varStatus="status">
   						 
   						 	 <tr>
					        <td>
					        	 <label class="no-margin" style="text-align: center;"><c:out value="${(status.index)+1}"/></label>
					         </td>
					        <td class="clickablename tdtextcolor">
					          <span class="name"><c:out value="${item.projectname}"/></span>
					        </td>
					       
					         <td class="hidden-phone"><c:out value="${item.createddate}"/></td>
					        <td class="hidden-phone"><c:out value="${item.createdby}"/>
					         
					        </td>
					       <td class="hidden-phone">
					          <div class="btn-group btn-group-sm" role="group">
					          <a href="#" class="btn btn-default button-color" id="copyproject"
									name="copyproject" value="copyproject">Copy Project</a>
					         <a href="#" class="open-renameDialog btn btn-default button-color" id="rename"
									name="rename" value="rename">Rename</a>	
									
							 <a href="#" class="btn btn-default button-color" id="deleteproject"
									name="deleteproject" value="deleteproject">Delete</a>		
					         
					     
					         
					         
					         
					         
					          </div>
					        </td>
					        
					      </tr>
		
   						 
   						 
						</c:forEach>
						
						 </tbody>
					  </table>
					</div>
					</div>
					<div class="col-md-12 text-center">
				      <ul class="pagination pagination-sm pager" id="myPager"></ul>
				     </div>
					</div>
			     	</c:if>
				
				<div class="modal fade" id="renameDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-header button-color">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        <h4 class="modal-title button-color" id="myModalLabel">Rename Project</h4>
					      </div>
					      <div class="modal-body">
					       <div class="row">
					       		<div class="col-sm-4" >
									<input type="text" class="form-control" id="oldproname"
												name="oldproname" placeholder="Enter Project Name" maxlength="30"
												 value="" readonly="readonly">
							</div>
										
								<div class="col-sm-4" >
									<input type="text" class="form-control" id="newproname"
												name="newproname" placeholder="Enter New Project Name" maxlength="30"
												 value="" required>
								</div>
					       </div>
					      </div>
					      <div class="modal-footer">
					        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					        <button type="submit" class="btn btn-primary" name ="renameSubmit" id ="renameSubmit" value="rename">Save</button>
					      </div>
					    </div>
					  </div>
				</div>
				<div class="modal fade" id="deleteDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-header button-color">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        <h4 class="modal-title button-color" id="myModalLabel">Delete Project</h4>
					      </div>
					      <div class="modal-body">
					          Do you really want to delete Project?
					      </div>
					      <div class="modal-footer">
					         <button type="submit" class="btn btn-primary" name ="deleteSubmit" id ="deleteSubmit" value="delete">Yes</button>
					         <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
					      </div>
					    </div>
					  </div>
				</div>
				

				</form>
				

				
</body>
</html>