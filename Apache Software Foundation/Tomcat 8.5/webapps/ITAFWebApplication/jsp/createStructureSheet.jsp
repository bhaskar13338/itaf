<%@page import="java.util.List"%>
<%@page import="com.majesco.itaf.dao.ProjectTransactions"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
	
	<form action="./itafpack.do" role="createprojform"
		data-toggle="validator" method="post">
		<input type="hidden" id="projectname" name="projectname" value="${currentProject}" />
		<input type="hidden" name="transactionaction" value="${currentTask}" />
		

		<div class=" col-md-12 form-group">

			<label class="col-sm-2" for="transactionname">Object Screen
				Name</label>
			<div class="">
				<div class="col-sm-4">
					<input type="text" class="form-control" id="transactionname"
						name="transactionname" placeholder="Enter Screen Object Name"
						maxlength="30" value="">
				</div>
				<div class="col-sm-4" style="padding-left: 0px;">
					<button type="submit" class="btn btn-default button-color"
						id="searchtrans" name="searchtrans" value="search">Search</button>


				</div>

			</div>
		</div>

		<div class=" col-md-12 form-group" id="btnCreate">
			<div class="col-sm-10" style="margin-left: 180px;">
				<button type="submit" class="btn btn-default button-color"
					id="createTrans" name="createTrans" value="create">Create
					Object Screen</button>
				<a href="#objcap" class="btn btn-default button-color" id="objcap"
					name="objcap" value="objcap">Object Capture</a>
			</div>
		</div>

	</form>
	
	<c:if test="${currentProTransactionList != null && fn:length(currentProTransactionList) gt 0}">
	
	<form action="./itafpack.do" role="createTransactionform" method="post">
		<input type="hidden" name="transactionname" value="" /> <input
			type="hidden" name="transactionaction" value="" /> <input
			type="hidden" name="projectname" value="${currentProject}" />
		<div class="col-md-12">
			<div class="row clearfix">

				<div class="column margin-15">
					<div class="table-responsive">
						<table
							class="table table-condensed table-striped table-bordered table-hover no-margin">
							<thead>
								<tr>
									<th style="width: 5%"><label class="no-margin"></label></th>
									<th style="width: 25%">Object Screen Name</th>
									<th style="width: 15%" class="hidden-phone">Date</th>
									<th style="width: 20%" class="hidden-phone">Created By</th>
									<th style="width: 40%" class="hidden-phone">Actions</th>

								</tr>
							</thead>
							<tbody id="TransactionTable">
								
								
								<c:forEach items="${currentProTransactionList}" var="sitem" varStatus="sstatus">
								<tr>
									<td><label class="no-margin"><c:out value="${(sstatus.index)+1}"/></label></td>
									<td class="clickabletxnname tdtextcolor"><span
										class="name"><c:out value="${sitem.transactionName}"/></span>
									</td>

									<td class="hidden-phone"><c:out value="${sitem.createddate}"/></td>
									<td class="hidden-phone"><c:out value="${sitem.createdby}"/></td>
									<td class="hidden-phone">
										<div class="btn-group btn-group-sm" role="group">
											<a href="#objDtl" class="btn btn-default button-color"
												id="objDtl" name="objDtl" value="objDtl">Object
												Definition</a> <a href="#" class="btn btn-default button-color"
												id="copyTrans" name="copyTrans" value="copyTrans">Copy
												Transaction</a> <a href="#" class="btn btn-default button-color"
												id="renameTrans" name="renameTrans" value="renameTrans">Rename</a>
											<a href="#" class="btn btn-default button-color"
												id="deleteTrans" name="deleteTrans" value="deleteTrans">Delete</a>
										</div>
									</td>

								</tr>
								
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				<div class="col-md-12 text-center">
					<ul class="pagination pagination-sm pager" id="myTPager"></ul>
				</div>
			</div>
		</div>
		

		<div class="modal fade" id="renameTDialog" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" data-backdrop="static"
			data-keyboard="false">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header button-color">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title button-color" id="myModalLabel">Rename
							Transaction</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-sm-4">
								<input type="text" class="form-control" id="oldtranname"
									name="oldtranname" placeholder="Enter Transaction Name"
									maxlength="30" value="" >
							</div>

							<div class="col-sm-4">
								<input type="text" class="form-control" id="newtranname"
									name="newtranname" placeholder="Enter New Transaction Name"
									maxlength="30" value="" >
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary" name="renameTrans"
							id="renameTrans" value="renameTrans">Save</button>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="deleteTDialog" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" data-backdrop="static"
			data-keyboard="false">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header button-color">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title button-color" id="myModalLabel">Delete
							Transaction</h4>
					</div>
					<div class="modal-body">Do you really want to delete
						Transaction?</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary"
							name="deletetransSubmit" id="deletetransSubmit" value="delete">Yes</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
					</div>
				</div>
			</div>
		</div>
	</form>
	
	</c:if>
	<div class="modal fade" id="strucureSheetDialog" tabindex="-1"
		role="dialog" aria-labelledby="myModalLabel" data-backdrop="static"
		data-keyboard="false">
		<form id="structureform" name="structureform" action="./itafpack.do"
			method="post" role="structureform" data-toggle="validator">
			<div class="modal-dialog mymodal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header button-color">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title button-color" id="myModalLabel">Object
							Details -${currentTransaction}</h4>

					</div>
					<div class="modal-body" style="padding: 0px;">
						<jsp:include page="structurSheetPreparation.jsp"></jsp:include>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary" name="save_rows_str"
							id="save_rows_str" value="">Save</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="modal fade" id="objCapDialog" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" data-backdrop="static"
			data-keyboard="false">
			<div class="modal-dialog mymodal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header button-color">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title button-color" id="myModalLabel">Object
							Capture</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<label class="col-sm-3" for="appUrl">Application Url:</label>
							<div class="col-sm-4">

								<input type="text" class="form-control" id="urltxt"
									name="urltxt" placeholder="Enter Screen Object Name"
									value="${currentProConfig.baseurl}" readonly="readonly">								
							</div>

							<div class="col-sm-4" style="padding-left: 0px;">
								<button type="submit" class="btn btn-default button-color"
									id="start" name="start" value="start">Go</button>
							</div>

						</div>



						<div class="row padding-top-15">
							<label class="col-sm-3" for="transactionname">Object
								Screen Name:</label>
							<div class="col-sm-4">
								<select name="dropdown" id="dropdown" class="form-control">
									<option>Select</option>
									<c:forEach items="${currentProTransactionList}" var="element">
										<option value="<c:out value="${element.transactionName}"></c:out>"><c:out value="${element.transactionName}"></c:out></option>
									</c:forEach>
								</select>
							</div>
							<div class="col-sm-4" style="padding-left: 0px;">
								<button type="submit" class="btn btn-default button-color"
									id="stop" name="stop" value="Stop">Stop</button>
							</div>

						</div>


					</div>
					<div class="container" style="margin-top: 10px;">
						<div class="row">
							<div class="no-more-tables">
								<table id="dataTable"
									class="col-sm-12 table-condensed table-striped table-bordered table-hover no-margin">
									
								</table>

							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary" name="renameTrans"
							id="deleteObjRow" value="deleteObjRow" onclick="deleteRow('dataTable');">Delete Row</button>
						<button type="submit" class="btn btn-primary" name="renameTrans"
							id="saveObjRows" value="saveObjRows" onclick="save_Data();">Save</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
</body>
</html>