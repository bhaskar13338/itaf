<%@page import="com.majesco.itaf.dao.TransactionMappingFile"%>
<%@page import="java.util.List"%>
<%@page import="com.majesco.itaf.dao.MainControllerFile"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="./css/demo3/responsiveTables.css">
<style type="text/css">
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 12px !important;
    line-height: 1.42857143;
    vertical-align: top;
    border-top: 1px solid #ddd;
}</style>
</head>
<body>

			<form name="uploaddownloadform" action="./uploaddownload.do" method="post" enctype="multipart/form-data">
			<input type="hidden" name="reqfilename" id="reqfilename" value="MainController"/>
			<input type="hidden" name="parentprojectname" id="parentprojectname" value="${currentProject}"/>
			<input type="hidden" id="hidconfFlag"   name="hidconfFlag" value="${configFlag}" />
			<div class ="row">
			
			<div class="col-xs-12 form-group">
				<div class="col-sm-6" style="width:40% ">
						<label  for="templatedownload">Please Checkout Business Scenario File </label>
						<label class="control-label"  for="templatedownload" id="tempaltenamelabel"></label>
				</div>
				<div class="col-sm-4">
				   <button type="submit" class="btn btn-default button-color" id="btnfile"
							name="btnfile" value="export">Download</button>
			    </div>
			</div>
			<div class="col-xs-12 ">
				
				<div class="col-sm-6" style="width:40% ">
					<label  for="templatedownload">Save Business Scenario</label>
					
				</div>	
								
				<div class="col-sm-4">
						<div class="row">
							<div class="col-xs-9">	
								<!-- <input type="file" name="importfile" id="importfile"/>-->
								<input type="file"  name="importfile" id="importfile" class="filestyle" data-size="nr">
							</div>
							<div class="col-xs-3">
								<button type="submit" class="btn btn-default button-color" id="btnfile" name="btnfile" value="import">Save</button>
							</div>
						</div>
					</div>
				</div>
				
				</div>
				</form>




<form role="mainContollerform" data-toggle="validator"  action="./itafpack.do" method="post">
<input type="hidden" name= "projectname" value="${currentProject}">
<input type="hidden" name= "currentScenarioid" value=""/>
<input type="hidden" name= "currentcolumnname" value=""/>
<input type="hidden" name= "noMainControllerrow" value=""/>
<input type="hidden" name="mainControlleraction" value=""/>
<div class ="row padding-bottom-5 padding-top-15">
<div class="span12">
	<!--  <a href="#nav_Txnscreen" class="btn btn-default pull-left margin-15 button-color" name="#nav_Txnscreen" >Create Transactions</a>-->
	 <a href="#add_mainControllerData" class="btn btn-default pull-left margin-15 margintop5 button-color" name="#add_mainControllerData" >Create Scenario</a>
	 <a href="#delete_mainControllerData" class="btn btn-default pull-left margin-15 margintop5 button-color" name="#delete_mainControllerData" >Delete Scenario</a>
</div>
</div>
<div class ="row">

<c:if test="${currentProMainController != null && fn:length(currentProMainController) gt 0}">
<div class="table-responsive" style="overflow-y:auto">
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
            	<th class="fixed-column"></th>
                <th class="fixed-column">GroupName</th>
                <th  class="fixed-column">TestCaseID</th>
                <th  class="fixed-column">Test_Description</th>
                <th  class="fixed-column">ExecuteFlag</th>
                <th  class="fixed-column">Action</th>
                <!-- <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>-->
                
             
        	 <c:forEach items="${currentProMainController[0].headerList}" var="item" varStatus="status">
        	 		<th></th>
        	 </c:forEach>
        	
            </tr>
        </thead>
        <tbody id="tab_logic_maincontrol_body">
        
        <c:forEach items="${currentProMainController}" var="citem" varStatus="status">
      
				<tr  class="clickabletestid" id='addr<c:out value='${(status.index)+1}'/>'><input type="hidden" name="indexid" id="indexid" value="<c:out value='${(status.index)+1}'/>"/>
				<td class="fixed-column" ><input type='checkbox' class='checker'></td>
                <td class="fixed-column" ><c:out value='${citem.groupName}'/></td>
                <td class="fixed-column tdtextcolor" onclick="showdialog(<c:out value='${(status.index)+1}'/>)" ><c:out value='${citem.testcaseid}'/></td>
                <td class="fixed-column" ><c:out value='${citem.test_description}'/></td>
                <td class="fixed-column"><c:out value='${citem.execute_flag}'/></td>
                <td class="fixed-column" ><c:out value='${citem.action}'/></td>


	             <c:forEach items="${citem.headerList}" var="hitem" varStatus="hstatus">
	          			 <td> 
	          			 <a href="#add_transaction" class="btn btn-default  btn-xs pull-left margin-15 button-color" name="#add_transaction"  <c:if test='${hitem!=null && fn:length(hitem) gt 0}'>style="background-color: #FF8549"</c:if> >
	          			
	          			  <c:choose>
	          			  <c:when test="${hitem == null}">
	          			  Select Object
	          			  </c:when>
	          			  <c:when test="${hitem != null && (fn:length(hitem) eq  0)}">
	          			  Select Object
	          			  </c:when>
	          			   <c:otherwise>
	          			   <c:out value="${hitem}"></c:out>
	          			   </c:otherwise>
	          			  </c:choose>
	          			
	          			
	          			
	          			
	          			 </a>
	          			 
	          			</td>
	             </c:forEach>
            </tr>
         
          </c:forEach>
        </tbody>
    </table>
</div>
</c:if>

</div>
<!-- add scenario model start -->
<div class="modal fade" id="addTestScenarioDialog" tabindex="-1" role="dialog" aria-labelledby="myTestScenarioLabel" data-backdrop="static" data-keyboard="false">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-header button-color">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        <h4 class="modal-title button-color" id="myModalLabel">Business Process Details</h4>
					      </div>
					      <div class="modal-body">
					      <input type="hidden" name="rowindex" value=""/>
					      <input type="hidden" name="updateflag" value="false"/>
					       <div class="form-group">
								    <label class="control-label col-sm-2" for="testcaseid">TestCaseID#</label>
								    <div class="col-sm-8">
								      <input type="text" class="form-control" name ="testcaseid" id="testcaseid" placeholder="Enter TestCaseId">
								    </div>
								  </div>
								  <div class="form-group">
								    <label class="control-label col-sm-2" for="groupname">Group Name</label>
								    <div class="col-sm-8"> 
								      <input type="text" class="form-control" name = "groupname" id="groupname" placeholder="Enter GroupName">
								    </div>
								  </div>
								   <div class="form-group">
								    <label class="control-label col-sm-2" for="testdesc">Test Description</label>
								    <div class="col-sm-8"> 
								      <input type="text" class="form-control" name ="testdesc" id="testdesc" placeholder="Enter Test Description">
								    </div>
								  </div>
								   <div class="form-group">
								    <label class="control-label col-sm-2" for="execflag">Execution Flag</label>
								    <div class="col-sm-8"> 
								     <select class="form-control" placeholder="Select Execution Flag"
										id="execflag" name="execflag" required>
										<option value="Y">Y</option>
										<option value="N">N</option>
									</select>
								    </div>
								  </div>
					      </div>
					      <div class="modal-footer">
					       <button type="submit" class="btn btn-primary" name ="saveTranscenario" id ="saveTranscenario" value="saveTranscenario">Save</button>
					       <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					       
					      </div>
					    </div>
					  </div>
		</div>
		<!-- add scenario model end -->
		
		 <!-- add transaction in scenario  model start -->
		  <div class="modal fade" id="addtxnScenarioDialog" tabindex="-1" role="dialog" aria-labelledby="mytxnScenarioLabel" data-backdrop="static" data-keyboard="false">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-header button-color">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        <h4 class="modal-title button-color" id="myModalLabel">Add Business Process Transaction</h4>
					      </div>
					      <div class="modal-body">
					       <div class="form-group">
							    <label class="control-label col-sm-2" for="transactionName">Selected Transaction</label>
							    <div class="col-sm-6">
							      <input type="text" class="form-control" name ="transactionName" id="transactionName" placeholder="Enter Transaction Name">
							    </div>
							     <div class="col-sm-2">
							    <!--   <button type="submit" class="btn btn-primary" name ="deleteTxnScenario" id ="deleteTxnScenario" value="deleteTxnScenario">Delete</button>-->
							      <button type="submit" class="btn btn-primary" name ="searchTxnScenario" id ="searchTxnScenario" value="searchTxnScenario">Search</button>
							    </div>
							 </div>
							  <div class="form-group">
							  <c:if test="${currentProTransMapping != null && fn:length(currentProTransMapping) gt 0}">
							 
								    <a href = "#" class = "list-group-item active">
									   Select Tranaction Form List
									</a>
									 <c:forEach items="${currentProTransMapping}" var="mitem" varStatus="mstatus">
									
									<a href="#item" class="list-group-item"><c:out value="${mitem.transcationType}"/></a>
									
									</c:forEach>
									<a href="#item" class="list-group-item">No Transaction</a>
							
							 </c:if>
							  </div>
						  
								  
					      </div>
					      <div class="modal-footer">
					        <!-- <button type="button" class="btn btn-primary" name ="editTranscenario" id ="editTranscenario" value="editTranscenario">Edit</button>-->
					       <button type="submit" class="btn btn-primary" name ="saveTxnscenario" id ="saveTxnscenario" value="saveTxnscenario">Save</button>
					       <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					       
					      </div>
					    </div>
					  </div>
		</div>
		<!-- add Transaction in scenario  model end -->
		
	</form>	
	
</body>
</html>