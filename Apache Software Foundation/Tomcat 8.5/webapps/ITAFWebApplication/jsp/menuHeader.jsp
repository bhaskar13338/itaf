
<%@page import="com.majesco.itaf.dao.ProjectPack"%>
<%@page import="java.util.List"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
           
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ITAF Home</title>
<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">

<link rel="stylesheet" type="text/css" href="./css/demo3/try.css">
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
<link href="//www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
</head>
<body>
	
	
<!-- Top Menu start -->
	
		<div id="header">
		<form action="./itafexecute.do" method="POST">
			<input type="hidden" name="navigationAction" id="navigationAction"/>
			<ul class="topnav" style="background-color: #2a6598">
			    <li><a href="#home">Home</a></li>
				<li><a href="#createpack">Create Pack</a></li>
				<!-- <li><a href="#modify">Modify</a></li>-->
				<li><a href="#execution">Execution</a></li>
<!-- 				<li><a href="#qtest">qTest</a></li>
				<li><a href="#jenkins">Jenkins</a></li> -->
				<li><a href="#schedule">Schedule</a></li>
				<li><a href="#access">Grant Access</a></li>
				<li><a href="#log">Execution Log</a></li>
				<!--  Bhaskar added dropdown for iTAF Utilities START -->
				
<!-- 				<li class="dropdown">
				    <a href="#" id="nbAcctDD" class="dropdown-toggle" data-toggle="dropdown">iTAF Utilities
   					 <span class="caret"></span>
    					<ul class="dropdown-menu">
    				  <li><a href="#">File Compare</a></li>
   					 </ul>
   					  </a>
   				</li>
   				</ul> -->
   				
   				<!--  Bhaskar added dropdown for iTAF Utilities END -->
				<li class="icon"><a href="javascript:void(0);"
					onclick="myFunction()">&#9776;</a></li>
					
				<li  style="float: right">
			        <!-- <a href="#"  class="dropbtn"><span class="glyphicon glyphicon-user"></span>  <i class="icon-user"></i>Bhaskar<i class="icon-sort-down"></i></a>
					     -->
				</form>
				<form action="./logout.do" method="post">
			    <ul class="nav navbar-nav pull-right">
					<li class="dropdown">
						<a href="#" id="nbAcctDD" class="dropdown-toggle" data-toggle="dropdown">
						<span class="glyphicon glyphicon-user"></span> 
						<i class="icon-user"></i>
							ITAF Admin
						<i class="icon-sort-down"></i>
						</a>
						<ul class="dropdown-menu dropdown-menu pull-right">
							<li><a href="#" title="logouthref">Log Out</a></li>
						</ul>
					</li>
				</ul>
				</form>
			    </li>
			</ul>
		</div>
		
	
	


</body>
</html>