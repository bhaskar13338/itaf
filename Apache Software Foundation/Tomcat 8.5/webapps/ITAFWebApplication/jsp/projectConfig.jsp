<%@page import="com.majesco.itaf.dao.ConfigFile"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
    <%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<form  role="configform" data-toggle="validator"  action="./itafpack.do" method="post">
				
				<div class="col-xs-12 form-group margintop15">
					<label class="control-label col-sm-4" for="burl">Base URL</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="baseurl"
							name="baseurl" placeholder="Enter Project Url"
							style="width: 600px;" value="${currentProConfig.baseurl}" required>
						</inupt>
					</div>
				</div>
				<div class="col-xs-12 form-group">
					<label class="control-label col-sm-4" for="burl">Browser Type</label>
					<div class="col-sm-6">

						<select class="form-control" placeholder="Select Browser"
							id="browsertype" name="browsertype">
							<option value="InternetExplorer" <c:if test="${currentProConfig != null && currentProConfig.browsertype!=null && fn:containsIgnoreCase(currentProConfig.browsertype,'InternetExplorer')}">selected</c:if>>Internet Explorer</option>
							<option value="Chrome" <c:if test="${currentProConfig != null && currentProConfig.browsertype!=null && fn:containsIgnoreCase(currentProConfig.browsertype,'Chrome')}">selected</c:if>>Chrome</option>
							<option value="FireFox" <c:if test="${currentProConfig != null && currentProConfig.browsertype!=null && fn:containsIgnoreCase(currentProConfig.browsertype,'FireFox')}">selected</c:if>>FireFox</option>
							<option value="Safari" <c:if test="${currentProConfig != null && currentProConfig.browsertype!=null && fn:containsIgnoreCase(currentProConfig.browsertype,'Safari')}">selected</c:if>>Safari</option>
						</select>
					</div>
				</div>
				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="burl">MSEXCEL</label>
					<div class="col-sm-6">

						<select class="form-control" placeholder="Select Browser"
							id="msexecl" name="msexecl">
							<option value="2003" selected>2003</option>
						</select>
					</div>
				</div>
				<div class="col-xs-12 form-group">
					<label class="control-label col-sm-4" for="burl">Execution Approach</label>
					<div class="col-sm-6">

						<select class="form-control" placeholder="Select Browser"
							id="executionapproch" name="executionapproch" data-toggle="tooltip" data-trigger="hover" data-placement="top" title="linear & timetravel">
							<option value="Linear" <c:if test="${currentProConfig != null && currentProConfig.execution_approach!=null && fn:containsIgnoreCase(currentProConfig.execution_approach,'Linear')}">selected</c:if>>Linear</option>
							<option value="TimeTravel" <c:if test="${currentProConfig != null && currentProConfig.execution_approach!=null && fn:containsIgnoreCase(currentProConfig.execution_approach,'TimeTravel')}">selected</c:if>>TimeTravel</option>
							
							
						</select>
					</div>
				</div>
				<div class="col-xs-12 form-group">
					<label class="control-label col-sm-4" for="burl">User Interaction</label>
					<div class="col-sm-6">

						<select class="form-control" placeholder="Select Browser"
							id="userinteraction" name="userinteraction" data-toggle="tooltip" data-trigger="hover" data-placement="top" title="true & false">
							<option value="True" <c:if test="${currentProConfig != null && currentProConfig.userinteraction!=null && fn:containsIgnoreCase(currentProConfig.userinteraction,'True')}">selected</c:if>>True</option>
							<option value="False" <c:if test="${currentProConfig != null && currentProConfig.userinteraction!=null && fn:containsIgnoreCase(currentProConfig.userinteraction,'False')}">selected</c:if>>False</option>
						</select>
					</div>
				</div>
				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="cnumber">Cycle Number</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="cyclenumber"
							name="cyclenumber" placeholder="Enter Cycle Number"
							style="width: 600px;" value="Cycle1" required>
						</inupt>
					</div>
				</div>
				<div class="col-xs-12 form-group">
					<label class="control-label col-sm-4" for="cnumber" data-toggle="tooltip" data-trigger="hover" data-placement="top" title="expiry time">Timeout (Secs)</label>
					<div class="col-sm-5">
						 
						 	<input type="text" class="form-control" id="timeout"
								name="timeout" placeholder="Timeout in Seconds" style="width:505px;"
								required value="${currentProConfig.timeout}" >
							</inupt>
							
					</div>
					
				</div>
				<div class="col-xs-12 form-group">
					<label class="control-label col-sm-4" for="cnumber">Email</label>
					<div class="col-sm-6">
					
						<input type="email" class="form-control" id="email" name="email"
							placeholder="Enter Email Address (Use ; to seperate the entries) " style="width: 505px;" required value="${currentProConfig.email_id}">
						</inupt>
						
					
					</div>
				</div>

				<div class="col-xs-12 form-group">
					<label class="control-label col-sm-4" for="cProjname">Project Name</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="cprojectname"
							name="cprojectname" style="width: 505px;" value="${currentProConfig.project_name}"
							readonly="readonly">
						</inupt>
					</div>
				</div>
				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="confgpath">Config Filepath</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="configfilepath"
							name="configfilepath" style="width: 600px;" value="${currentProConfig.config_filepath}"
							readonly="readonly">
						</inupt>
					</div>
				</div>
				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="confpath">Controller Filepath</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="controllerfilepath"
							name="controllerfilepath" style="width: 600px;" value="${currentProConfig.controller_filepath}"
							readonly="readonly">
						</inupt>
					</div>
				</div>

				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="transinfo">Transaction Info</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="transactioninfo"
							name="transactioninfo" style="width: 600px;" value="${currentProConfig.transaction_info}"
							readonly="readonly">
						</inupt>
					</div>
				</div>
				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="vertblistpath">Verification Table List Path</label>
					<div class="col-sm-6">
						<input type="text" class="form-control"
							id="verificationtablelistpath" name="verificationtablelistpath"
							style="width: 600px;" value="${currentProConfig.verificationtablelistpath}" readonly="readonly">
						</inupt>
					</div>
				</div>
				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="transippath">Transaction Input Filepath</label>
					<div class="col-sm-6">
						<input type="text" class="form-control"
							id="transactioninputfilepath" name="transactioninputfilepath"
							style="width: 600px;" value="${currentProConfig.transaction_input_filepath}" readonly="readonly">
						</inupt>
					</div>
				</div>
				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="ipdatapath">Input Data Filepath</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="inputdatafilepath"
							name="inputdatafilepath" style="width: 600px;" value="${currentProConfig.input_data_filepath}"
							readonly="readonly">
						</inupt>
					</div>
				</div>
				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="respath">Result Filepath</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="resultpath"
							name="resultpath" style="width: 600px;" value="${currentProConfig.result_filepath}"
							readonly="readonly">
						</inupt>
					</div>
				</div>
				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="resultop">Result Output</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="resultoutput"
							name="resultoutput" style="width: 600px;" value="${currentProConfig.resultoutput}"
							readonly="readonly">
						</inupt>
					</div>
				</div>
				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="resultop">Dashboard Results Path</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="dashboardresult"
							name="dashboardresult" style="width: 600px;" value="${currentProConfig.dashboardresultspath}"
							readonly="readonly">
						</inupt>
					</div>
				</div>
				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="resultop">Dynamic Result Path</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="dynresultpath"
							name="dynresultpath" style="width: 600px;" value="${currentProConfig.dynamicresultspath}"
							readonly="readonly">
						</inupt>
					</div>
				</div>
				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="resultop">Dashboard Filepath</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="dashboardfilepath"
							name="dashboardfilepath" style="width: 600px;" value="${currentProConfig.dashboard_filepath}"
							readonly="readonly">
						</inupt>
					</div>
				</div>
				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="resultop">HTML Report Filepath</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="htmlfilepath"
							name="htmlfilepath" style="width: 600px;" value="${currentProConfig.htmlreport_filepath}"
							readonly="readonly">
						</inupt>
					</div>
				</div>
				
				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="resultop">Consolelog Filepath</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="consolefilepath"
							name="consolefilepath" style="width: 600px;" value="${currentProConfig.consolelog_filepath}"
							readonly="readonly">
						</inupt>
					</div>
				</div>
				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="verResultpath">Verification Result Path</label>
					<div class="col-sm-6">
						<input type="text" class="form-control"
							id="verificationresultspath" name="verificationresultspath"
							style="width: 600px;" value="${currentProConfig.verificationresultspath}" readonly="readonly">
						</inupt>
					</div>
				</div>

				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="exevalpath">Expected Values Path</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="expectedvaluespath"
							name="expectedvaluespath" style="width: 600px;" value="${currentProConfig.expectedvaluespath}"
							readonly="readonly">
						</inupt>
					</div>
				</div>
				<div class="col-xs-12 form-group" style="display: none">
					<label class="control-label col-sm-4" for="vertemppath">VerifiCation Template Path</label>
					<div class="col-sm-6">
						<input type="text" class="form-control"
							id="verificationtemplatepath" name="verificationtemplatepath"
							style="width: 600px;" value="${currentProConfig.verifcationtemplatepath}" readonly="readonly">
						</inupt>
					</div>
				</div>


				<div class="col-xs-12 form-group">
					<div class="col-sm-offset-4 col-sm-10">
						<button type="submit" class="btn btn-default button-color" id="update"
							name="update" value="update">Save</button>
						<button type="submit" class="btn btn-default button-color" id="updatecontinue"
							name="updatecontinue" value="updatecontinue">Save & Continue</button>
						
					</div>
				</div>
				</form>
			
</body>
</html>