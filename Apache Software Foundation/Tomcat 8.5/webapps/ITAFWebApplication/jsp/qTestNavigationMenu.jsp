<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>qTest</title>
</head>
<body>
			<form id="qtestMenu" name="qtestMenu" method="post">
			<div id="sidebar-wrap" class="col-md-2" style="padding-left: 5px; display: none;">
				<div id="sidebar">
					<!-- Put Your Code here -->
					<div id="MainMenu">
						<div class="list-group panel">
							<a href="#qtestproject" class="list-group-item list-group-item-success"
								data-toggle="collapse" data-parent="#MainMenu">Create
								qTest Project</a>
								 <a href="#qtestdesign"
								class="list-group-item list-group-item-success"
								data-toggle="collapse" data-parent="#MainMenu">Test Design
								</a> 
								<a href="#qtestitafexe"
								class="list-group-item list-group-item-success"
								data-toggle="collapse" data-parent="#MainMenu">Test Execution</a>
								<a href="#qtestresults"
								class="list-group-item list-group-item-success"
								data-toggle="collapse" data-parent="#MainMenu">Test Results</a> 
								<a href="#qtestdashboard"
								class="list-group-item list-group-item-success"
								data-toggle="collapse" data-parent="#MainMenu">Dashboard</a>
						</div>
					</div>
					<!-- end -->
				</div>
			</div>
		</form>
</body>
</html>