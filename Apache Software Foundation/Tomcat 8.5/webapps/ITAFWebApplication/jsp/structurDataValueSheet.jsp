<!--<%@page import="org.apache.el.parser.AstMinus"%>-->
<%@page import="com.majesco.itaf.dao.MainControllerFile"%>
<%@page import="com.majesco.itaf.dao.TransactionStructureData"%>
<%@page import="java.util.List"%>
<%@page import="com.majesco.itaf.dao.TransactionMappingFile"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

	<form id="structureDataform" name="structureDataform" role="structureDataform" data-toggle="validator" action="./itafpack.do" method="post">
				<input type="hidden" name="noStrucuredata" id="noStrucuredata" value=""/>
				<input type="hidden" name="parentprojectname" id="parentprojectname" value="${currentProject}"/>
				<input type="hidden" name="savestructuredata" id="savestructuredata" value=""/>
				
	<div class="col-md-12 column" style="padding-top:15px;">
	
							<div class="padding-bottom-5">	
							
							<div class="form-group" >
							<c:if test="${filterMainControler!=null &&fn:length(filterMainControler) gt 0}">
							
								<div class="col-sm-4" style="padding-left:0px;">
									<select name="dtestcaseid1" class="form-control">
											<option value="Select"> Select TestCaseID </option>
									
											<c:forEach items="${filterMainControler}" var="testitem" varStatus="teststatus">
											
													<option  value ='<c:out value="${testitem}"/>' <c:if test="${reqTestCaseid!=null && fn:containsIgnoreCase(reqTestCaseid,testitem)}">selected</c:if>><c:out value="${testitem}"/></option>
												
											</c:forEach>
										
										</select>	
								</div>
								
							</c:if>	
							<c:if test="${filtertransaction!=null &&fn:length(filtertransaction) gt 0}">
						
								<div class="col-sm-4">
								
										<select class="form-control" placeholder="Select Transaction  Input File"
													id="transDatainputfile" name="transDatainputfile">
											<option value="Select"> Select Transaction </option>
											<c:forEach items="${filtertransaction}" var="transitem" varStatus="transstatus">
												<option value='<c:out value="${transitem}"/>'<c:if test="${structruFileName!=null && (fn:length(transitem) gt 0) && fn:containsIgnoreCase(structruFileName,transitem) }">selected</c:if>><c:out value="${transitem}"/></option>
											</c:forEach>
										</select>
								</div>
								
								</c:if>
							</div>
							</div>
							
						<c:if test="${filtertransaction!=null &&fn:length(filtertransaction) gt 0}">
			<div id="datauploaddownload" class="padding-bottom-5">
			
			<input type="hidden" name="reqfilename" id="reqfilename" value="${structruFileName}"/>
			<input type="hidden" name="parentprojectname" id="parentprojectname" value="${currentProject}"/>
			<input type="hidden" id="hidconfFlag"   name="hidconfFlag" value="${configFlag}" />
			<div class ="row">
			
			<div class="col-xs-12 form-group">
				<div class="col-sm-6" style="width:40% ">
						<label  for="templatedownload">Structure Data File </label>
						<label class="control-label"  for="templatedownload" id="tempaltenamelabel"></label>
				</div>
				<div class="col-sm-4">
				   <button type="submit" class="btn btn-default button-color" id="btnfile"
							name="btnfile" value="export">Download</button>
			    </div>
			</div>
			<div class="col-xs-12 ">
				
				<div class="col-sm-6" style="width:40% ">
					<label  for="templatedownload">Save Structure Data</label>
					
				</div>	
								
				<div class="col-sm-4">
						<div class="row">
							<div class="col-xs-9">	
								<!-- <input type="file" name="importfile" id="importfile"/>-->
								<input type="file"  name="importfile" id="importfile" class="filestyle" data-size="nr">
							</div>
							<div class="col-xs-3">
								<button type="submit" class="btn btn-default button-color" id="btnfile" name="btnfile" value="import">Save</button>
							</div>
						</div>
					</div>
				</div>
				
				</div>
				
								
							</div>	
							
							<div id="datavaluediv">
							
							<table class="table table-bordered table-hover table-width-80" id="tab_logic_strucure_data" title="inputStructurdataTable">
								<thead>
									<tr>
									
									</tr>
								</thead>
								
								<tbody id="tab_logic_strucure_data_body">
							
								<tr>
									<td>TestCaseID</td>
									<td>
										<input type="text" class="form-control" id="strTestCaseID" name="strTestCaseID" placeholder="Enter TestCaseID"  required value="<c:if test='${structureDataFiles!=null &&fn:length(structureDataFiles) gt 0 }'><c:out value="${structureDataFiles[0].testcaseid}"/> </c:if>"  ></inupt>
									
									</td>
								</tr>
								
															
								
								
								<c:if test='${structureDataFiles!=null &&fn:length(structureDataFiles) gt 0 }'>
									
							
								<c:forEach items="${structureDataFiles}" var="sditem" varStatus="sdstatus">
								<c:if test="${not empty sditem}">
								
									<tr>
										<td><input  name="dlogicalname<c:out value='${(sdstatus.index)+1}'/>" type='text' placeholder='DataName'  readonly="readonly"  class='form-control input-md' value="<c:out value='${sditem.logicalname}'/>"></td>
										<td><input  name="ddatavalue<c:out value='${(sdstatus.index)+1}'/>" type='text' placeholder='DataValue'  class='form-control input-md' value="<c:out value='${sditem.value}'/>"></td>
									</tr>	
									
									</c:if>
								
									</c:forEach>
								</c:if>
								</tbody>
						</table>
							</div>
							
							
							<div class="col-md-12 column">
				    		 <a href="#save_rows_back_strData" title = "save_rows_back_strData" class="btn btn-default pull-left margin-15 button-color" name="save_rows_back_strData" >Back</a>
							 <a href="#save_rows_strData" class="btn btn-default pull-left margin-15 button-color" name="save_rows_strData" >Save</a>
							  <a href="#add_rows_strData" class="btn btn-default pull-left margin-15 button-color" name="add_rows_strData" >Clear</a>
							 <!--  <a href="#edit_rows_strData" class="btn btn-default pull-left margin-15 button-color" name="edit_rows_strData" >Edit</a>-->
							 </div>
 				
 				</c:if>
	</div>
	
	</form>
</body>
</html>