<%@page import="java.util.List"%>
<%@page import="com.majesco.itaf.dao.TransactionStructureFile"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

<style>
<style>
table {
       width: 400px;
       font-size: 14px;
       font-family: tahoma, arial, sans-serif;
}

table thead th {
       background-color: #ccc;
       padding: 5px 8px;
}

table td {
      /* background-color: #ddd;    */
}

table.sorting-table {
       cursor: move;
}

table tr.sorting-row td {
       background-color: #f80;
}

.sort-handler {
       float: right;
      /* background-color: #f80;*/
       cursor: move;
}
</style>

</style>


</head>
<body>



<%
List<TransactionStructureFile>  structureFiles =(List<TransactionStructureFile>) request.getAttribute("currentProTranStrdefault");
%>

<%if(structureFiles !=null && structureFiles.size()>0) {%>
	
				<input type="hidden" name="noStrucure" id="noStrucure" value=""/>
				<input type="hidden" name="savestructure" id="savestructure" value=""/>
				<input type="hidden" name="parentprojectname" id="parentprojectname" value="${currentProject}"/>
				<input type="hidden" name="transinputfile" value="${currentTransaction}"/>
				
				<div id="inputStructure" class="col-xs-12 form-group" style="overflow-x: scroll;">
				<div class="">
				<div class="row clearfix">
				<div class="col-md-12 column">
				
				
				
				<table class="table table-bordered table-hover table-responsive" id="tab_logic_strucure" title="inputStructurTable">
						<thead>
							<tr>
							   <!--  <th></th>-->
							    <th></th>
								<th style="width:75px;">Execute Flag</th>
								<th style="width:100px;">Action</th>
								<th>LogicalName</th>
								<th>ControlName</th>
								<th style="width:150px;">ControlType</th>
								<th style="width:150px;">ControlID</th>
								<th>ImageType</th>
								<th>Index</th>
								<th>DynamicIndex</th>
								<th>ColumnName</th>
								<th style="display: none">RowNo</th>
								<th style="display: none">ColumnNo</th>
							</tr>
						</thead>
						<tbody id="tab_logic_strucure_body">
				
						<%if(structureFiles!=null && structureFiles.size()>0) {%>
						<%for(int i=0 ; i<structureFiles.size();i++){ %>
						<%TransactionStructureFile structureFile = structureFiles.get(i); %>
								<tr id='addrstructure<%=i+1%>' ><input type="hidden" name="indexid" id="indexid" value="<%=i+1%>"/>
								<!-- <td> <a href="#" class='up'>Up</a></td>-->
                                <td class="sort-handler"> <input type='checkbox' class='checker'>
        						<td>
								
								<select class="form-control" placeholder="Select Execution Flag"
									id="structexeflag<%=i+1%>" name="structexeflag<%=i+1%>">
									<option value="Y" <% if (structureFile.getExecuteflag().equals("Y")){%>selected <%}%>>Y</option>
									
									<option value="N" <% if (structureFile.getExecuteflag().equals("N")){%>selected <%}%>>N</option>
									
									
								</select>
								
								</td>
								<td>
								
								<select class="strucureaction form-control" placeholder="Select Action"
									id="structaction<%=i+1%>" name="structaction<%=i+1%>"  style="min-width: 80px;" onchange="onchangeCapture(this.value, this.name);">
									<option value="Read" <% if (structureFile.getAction().equals("Read")){%>selected <%}%>>Read</option>
									<option value="I" <% if (structureFile.getAction().equals("I")){%>selected <%}%>>I</option>
									<option value="NC" <% if (structureFile.getAction().equals("NC")){%>selected <%}%>>NC</option>
									<option value="V" <% if (structureFile.getAction().equals("V")){%>selected <%}%>>V</option>
									<option value="Write" <% if (structureFile.getAction().equals("Write")){%>selected <%}%>>Write</option>
									<option value="Capture" <% if (structureFile.getAction().equals("Capture")){%>selected <%}%>>Capture</option>
								</select>
								</td>
								<td>
								<input  name="structlogic<%=i+1%>" type='text' placeholder='LogicalName'  class='form-control input-md' value="<%=structureFile.getLogicalname()%>">
								
								</td>
								<td><input  name="structcontol<%=i+1%>" type='text' placeholder='ControlName'  class='form-control input-md' value="<%=structureFile.getControlname()%>">
								</td>
								<td>
								
								<select class="form-control" placeholder="Select ControlType"
									id="structcontrolType<%=i+1%>" name="structcontrolType<%=i+1%>">
									<option value="WebEdit" <% if (structureFile.getControltype().equals("WebEdit")){%>selected <%}%>>WebEdit</option>
									<option value="WebList" <% if (structureFile.getControltype().equals("WebList")){%>selected <%}%>>WebList</option>
									<option value="WebButton" <% if (structureFile.getControltype().equals("WebButton")){%>selected <%}%>>WebButton</option>
									<option value="WebLink" <% if (structureFile.getControltype().equals("WebLink")){%>selected <%}%>>WebLink</option>
									<option value="CheckBox" <% if (structureFile.getControltype().equals("CheckBox")){%>selected <%}%>>CheckBox</option>
									<option value="Radio" <% if (structureFile.getControltype().equals("Radio")){%>selected <%}%>>Radio</option>
									<option value="WebElement" <% if (structureFile.getControltype().equals("WebElement")){%>selected <%}%>>WebElement</option>
									<option value="WebTable" <% if (structureFile.getControltype().equals("WebTable")){%>selected <%}%>>WebTable</option>
									<option value="Browser" <% if (structureFile.getControltype().equals("Browser")){%>selected <%}%>>Browser</option>
									<option value="Alert" <% if (structureFile.getControltype().equals("Alert")){%>selected <%}%>>Alert</option>
									<option value="Robot" <% if (structureFile.getControltype().equals("Robot")){%>selected <%}%>>Robot</option>
									<option value="Window" <% if (structureFile.getControltype().equals("Window")){%>selected <%}%>>Window</option>
									<option value="URL" <% if (structureFile.getControltype().equals("URL")){%>selected <%}%>>URL</option>
									<option value="JSScript" <% if (structureFile.getControltype().equals("JSScript")){%>selected <%}%>>JSScript</option>
									<option value="ListBox" <% if (structureFile.getControltype().equals("ListBox")){%>selected <%}%>>ListBox</option>
									<option value="Slider" <% if (structureFile.getControltype().equals("Slider")){%>selected <%}%>>Slider</option>
									<option value="MaskedInputDate" <% if (structureFile.getControltype().equals("MaskedInputDate")){%>selected <%}%>>MaskedInputDate</option>
									<option value="ActionClick" <% if (structureFile.getControltype().equals("ActionClick")){%>selected <%}%>>ActionClick</option>
									<option value="ActionMouseOver" <% if (structureFile.getControltype().equals("ActionMouseOver")){%>selected <%}%>>ActionMouseOver</option>
									<option value="IFrame" <% if (structureFile.getControltype().equals("IFrame")){%>selected <%}%>>IFrame</option>
									
									<option value="Wait" <% if (structureFile.getControltype().equals("Wait")){%>selected <%}%>>Wait</option>
								</select>
								</td>
								<td>
								<select class="form-control" placeholder="Select ControlID"
									id="structcontrolid<%=i+1%>" name="structcontrolid<%=i+1%>">
									<option value="" <% if (structureFile.getControlid().equals("")){%>selected <%}%>>Select</option>
									<option value="Id" <% if (structureFile.getControlid().equalsIgnoreCase("Id")){%>selected <%}%>>Id</option>
									<option value="Name" <% if (structureFile.getControlid().equalsIgnoreCase("Name")){%>selected <%}%>>Name</option>
									<option value="TagName" <% if (structureFile.getControlid().equalsIgnoreCase("TagName")){%>selected <%}%>>TagName</option>
									<option value="ClassName" <% if (structureFile.getControlid().equalsIgnoreCase("ClassName")){%>selected <%}%>>ClassName</option>
									<option value="XPath" <% if (structureFile.getControlid().equalsIgnoreCase("XPath")){%>selected <%}%>>XPath</option>
									<option value="AjaxPath" <% if (structureFile.getControlid().equalsIgnoreCase("AjaxPath")){%>selected <%}%>>AjaxPath</option>
									<option value="LinkText" <% if (structureFile.getControlid().equalsIgnoreCase("LinkText")){%>selected <%}%>>LinkText</option>
									<option value="HTMLID" <% if (structureFile.getControlid().equalsIgnoreCase("HTMLID")){%>selected <%}%>>HTMLID</option>
									<option value="CSSSelector" <% if (structureFile.getControlid().equalsIgnoreCase("CSSSelector")){%>selected <%}%>>CSSSelector</option>
									<option value="Id_p" <% if (structureFile.getControlid().equalsIgnoreCase("Id_p")){%>selected <%}%>>Id_p</option>
									<option value="XPath_p" <% if (structureFile.getControlid().equalsIgnoreCase("XPath_p")){%>selected <%}%>>XPath_p</option>
								</select>
								</td>
								<td><input  name="structimage<%=i+1%>" type='text' placeholder='ImageType'  class='form-control input-md' value="<%=structureFile.getImagetype()%>">
								</td>
								<td><input  name="structindex<%=i+1%>" type='text' placeholder='Index'  class='form-control input-md' value="<%=structureFile.getIndex()%>">
								</td>
								<td><input  name="structdyindex<%=i+1%>" type='text' placeholder='DynamicIndex'  class='form-control input-md' value="<%=structureFile.getDynamicindex()%>">
								</td>
								<td><input  name="structcolname<%=i+1%>" type='text' placeholder='ColumnName'  class='form-control input-md' value="<%=structureFile.getColumnname()%>">
								</td>
								<td style="display: none"><input  name="structrow<%=i+1%>" type='text' placeholder='RowNo'  class='form-control input-md' value="<%=structureFile.getRowno()%>">
								</td>
								<td style="display: none"><input  name="structcol<%=i+1%>" type='text' placeholder='ColumnNo'  class='form-control input-md' value="<%=structureFile.getColumnno()%>">
								</td>
							</tr>
							
						<% }}%>
					
							
						
							
							
						
					
					</tbody>

					</table>
					
							</div>
							</div>
							
							<a id="add_row_structure" class="btn btn-default pull-left margin-15 button-color">Add Row</a><a id='delete_row_strucure' class="pull-left btn btn-default margin-15 button-color">Delete Row</a>
							
							
						</div>
					</div>
			
			
			<%} %>
</body>
</html>