<%@page import="com.majesco.itaf.dao.VerificationStructureFile"%>
<%@page import="java.util.List"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<%
List<VerificationStructureFile>  verifystructureFiles =(List<VerificationStructureFile>) request.getAttribute("currentProVerifyTranStrdefault");
%>


<%if(verifystructureFiles !=null && verifystructureFiles.size()>0) {%>
	
				<input type="hidden" name="noVerifyStrucure" id="noVerifyStrucure" value=""/>
				<input type="hidden" name="saveverifystructure" id="saveverifystructure" value=""/>
				<input type="hidden" name="parentprojectname" id="parentprojectname" value="${currentProject}"/>
				<input type="hidden" name="verifyinputfile" value="${currentverifyTransaction}"/>
				
				<div id="Structure" class="col-xs-12 form-group" style="overflow-x: scroll;">
				<div class="">
				<div class="row clearfix">
				<div class="col-md-12 column">
				
				
				
				<table class="table table-bordered table-hover table-responsive" id="tab_logic_verify" title="verifyStructurTable">
						<thead>
							<tr>
							    <th></th>
							    <th></th>
								<th style="width:75px;">TableType</th>
								<th style="width:100px;">TableID</th>
								<th>TableIDType</th>
								<th>StartRow</th>
								<th>EndRow</th>
								<th style="width:150px;">ColumnName</th>
								<th>Row</th>
								<th>Column</th>
								<th>ControlName</th>
								<th>ControlType</th>
								<th>ControlID</th>
								<th>ControlIndex</th>
								<th style="display: none">VerificationType</th>
								<th style="display: none">Order</th>
							</tr>
						</thead>
						<tbody id="tab_logic_verify_body">
				
						<%if(verifystructureFiles!=null && verifystructureFiles.size()>0) {%>
						<%for(int i=0 ; i<verifystructureFiles.size();i++){ %>
						<%VerificationStructureFile vstructureFile = verifystructureFiles.get(i); %>
								<tr id='addrverify<%=i+1%>'><input type="hidden" name="indexid" id="indexid" value="<%=i+1%>"/>
								<td> <a href="#" class='up'>Up</a></td>
                                <td> <a href="#" class='down'>Down</a></td>
								<td>
									<!-- <input  name="tabletype<%=i+1%>" type='text' placeholder='TableType'  class='form-control input-md' value="">-->
								<select class="form-control" placeholder="Select TableType"
									id="tabletype<%=i+1%>" name="tabletype<%=i+1%>">
									<option value="ControlNames" <% if (vstructureFile.getTableType() !=null && vstructureFile.getTableType().equals("ControlNames")){%>selected <%}%>>ControlNames</option>
									<option value="HTML5" <% if (vstructureFile.getTableType() !=null && vstructureFile.getTableType().equals("HTML5")){%>selected <%}%>>HTML5</option>
									<option value="Uniform" <% if (vstructureFile.getTableType() !=null && vstructureFile.getTableType().equals("Uniform")){%>selected <%}%>>Uniform</option>
									<option value="NonUniform" <% if (vstructureFile.getTableType() !=null && vstructureFile.getTableType().equals("NonUniform")){%>selected <%}%>>NonUniform</option>
								</select>
								
								
								</td>
								<td>
								
								<input  name="tableid<%=i+1%>" type='text' placeholder='TableID'  class='form-control input-md' value="<%=vstructureFile.getTableId()%>">
								</td>
								<td>
								<input  name="tableidtype<%=i+1%>" type='text' placeholder='TableIDType'  class='form-control input-md' value="<%=vstructureFile.getTableIdType()%>">
								
								</td>
								<td><input  name="startrow<%=i+1%>" type='text' placeholder='StartRow'  class='form-control input-md' value="<%=vstructureFile.getStartRow()%>">
								</td>
								<td>
									<input  name="endrow<%=i+1%>" type='text' placeholder='EndRow'  class='form-control input-md' value="<%=vstructureFile.getEndRow()%>">
								</td>
								<td>
									<input  name="columnname<%=i+1%>" type='text' placeholder='ColumnName'  class='form-control input-md' value="<%=vstructureFile.getColumnName()%>">
								</td>
								<td>
									<input  name="row<%=i+1%>" type='text' placeholder='Row'  class='form-control input-md' value="<%=vstructureFile.getRow()%>">
								</td>
								<td>
									<input  name="column<%=i+1%>" type='text' placeholder='Column'  class='form-control input-md' value="<%=vstructureFile.getColumn()%>">
								</td>
								<td>
									<input  name="controlname<%=i+1%>" type='text' placeholder='ControlName'  class='form-control input-md' value="<%=vstructureFile.getControlName()%>">
								</td>
								<td>
									<!--  <input  name="controltype<%=i+1%>" type='text' placeholder='ControlType'  class='form-control input-md' value="">-->
									<select class="form-control" placeholder="Select ControlType"
									id="controltype<%=i+1%>" name="controltype<%=i+1%>">
									<option value="WebEdit" <% if (vstructureFile.getControlType().equals("WebEdit")){%>selected <%}%>>WebEdit</option>
									<option value="WebList" <% if (vstructureFile.getControlType().equals("WebList")){%>selected <%}%>>WebList</option>
									<option value="WebButton" <% if (vstructureFile.getControlType().equals("WebButton")){%>selected <%}%>>WebButton</option>
									<option value="WebLink" <% if (vstructureFile.getControlType().equals("WebLink")){%>selected <%}%>>WebLink</option>
									<option value="CheckBox" <% if (vstructureFile.getControlType().equals("CheckBox")){%>selected <%}%>>CheckBox</option>
									<option value="Radio" <% if (vstructureFile.getControlType().equals("Radio")){%>selected <%}%>>Radio</option>
									<option value="WebElement" <% if (vstructureFile.getControlType().equals("WebElement")){%>selected <%}%>>WebElement</option>
									<option value="WebTable" <% if (vstructureFile.getControlType().equals("WebTable")){%>selected <%}%>>WebTable</option>
									<option value="Browser" <% if (vstructureFile.getControlType().equals("Browser")){%>selected <%}%>>Browser</option>
									<option value="Alert" <% if (vstructureFile.getControlType().equals("Alert")){%>selected <%}%>>Alert</option>
									<option value="Robot" <% if (vstructureFile.getControlType().equals("Robot")){%>selected <%}%>>Robot</option>
									<option value="Window" <% if (vstructureFile.getControlType().equals("Window")){%>selected <%}%>>Window</option>
									<option value="URL" <% if (vstructureFile.getControlType().equals("URL")){%>selected <%}%>>URL</option>
									<option value="JSScript" <% if (vstructureFile.getControlType().equals("JSScript")){%>selected <%}%>>JSScript</option>
									<option value="ListBox" <% if (vstructureFile.getControlType().equals("ListBox")){%>selected <%}%>>ListBox</option>
									<option value="Slider" <% if (vstructureFile.getControlType().equals("Slider")){%>selected <%}%>>Slider</option>
									<option value="MaskedInputDate" <% if (vstructureFile.getControlType().equals("MaskedInputDate")){%>selected <%}%>>MaskedInputDate</option>
									<option value="ActionClick" <% if (vstructureFile.getControlType().equals("ActionClick")){%>selected <%}%>>ActionClick</option>
									<option value="ActionMouseOver" <% if (vstructureFile.getControlType().equals("ActionMouseOver")){%>selected <%}%>>ActionMouseOver</option>
									<option value="IFrame" <% if (vstructureFile.getControlType().equals("IFrame")){%>selected <%}%>>IFrame</option>
									<option value="Wait" <% if (vstructureFile.getControlType().equals("Wait")){%>selected <%}%>>Wait</option>
								</select>
								
								</td>
								<td>
									<!-- <input  name="controlid<%=i+1%>" type='text' placeholder='ControlID'  class='form-control input-md' value="">-->
									<select class="form-control" placeholder="Select ControlID"
									id="controlid<%=i+1%>" name="controlid<%=i+1%>">
									<option value="" <% if (vstructureFile.getControlId().equals("")){%>selected <%}%>>Select</option>
									<option value="Id" <% if (vstructureFile.getControlId().equals("Id")){%>selected <%}%>>Id</option>
									<option value="Name" <% if (vstructureFile.getControlId().equals("Name")){%>selected <%}%>>Name</option>
									<option value="TagName" <% if (vstructureFile.getControlId().equals("TagName")){%>selected <%}%>>TagName</option>
									<option value="ClassName" <% if (vstructureFile.getControlId().equals("ClassName")){%>selected <%}%>>ClassName</option>
									<option value="XPath" <% if (vstructureFile.getControlId().equals("XPath")){%>selected <%}%>>XPath</option>
									<option value="AjaxPath" <% if (vstructureFile.getControlId().equals("AjaxPath")){%>selected <%}%>>AjaxPath</option>
									<option value="LinkText" <% if (vstructureFile.getControlId().equals("LinkText")){%>selected <%}%>>LinkText</option>
									<option value="HTMLID" <% if (vstructureFile.getControlId().equals("HTMLID")){%>selected <%}%>>HTMLID</option>
									<option value="CSSSelector" <% if (vstructureFile.getControlId().equals("CSSSelector")){%>selected <%}%>>CSSSelector</option>
									<option value="Id_p" <% if (vstructureFile.getControlId().equals("Id_p")){%>selected <%}%>>Id_p</option>
									<option value="XPath_p" <% if (vstructureFile.getControlId().equals("XPath_p")){%>selected <%}%>>XPath_p</option>
								</select>
								
								</td>
								<td>
									<input  name="controlindex<%=i+1%>" type='text' placeholder='ControlIndex'  class='form-control input-md' value="<%=vstructureFile.getControlIndex()%>">
								</td>
								
								<td style="display: none">
									<input  name="verificationtype<%=i+1%>" type='text' placeholder='VerificationType'  class='form-control input-md' value="<%=vstructureFile.getVerificationType()%>">
								</td>
								<td style="display: none">
									<input  name="order<%=i+1%>" type='text' placeholder='Order'  class='form-control input-md' value="<%=vstructureFile.getOrder()%>">
								</td>
							</tr>
							
						<% }}%>
					
							
						
							
							
						
					
					</tbody>

					</table>
					
							</div>
							</div>
							<!-- <a id="save_rows_map" class="btn btn-default pull-left" name="save_rows_map" value="mappingSave">Save</a>-->
							<!-- <a id="add_row_structure" class="btn btn-default pull-left">Add Row</a><input type="submit" id="save_rows_str" name="save_rows_str" value="Save">Save</input><a id='delete_row_strucure' class="pull-left btn btn-default">Delete Row</a>-->
							
							<!-- <a href="#" title = "save_rows_back_createStr" class="btn btn-default pull-left margin-15 button-color" name="save_rows_back_strData" >Back</a>-->
							<a id="add_row_verifystructure" class="btn btn-default pull-left margin-15 button-color">Add Row</a><a id='delete_row_verifystrucure' class="pull-left btn btn-default margin-15 button-color">Delete Row</a>
							<!-- <a class="btn btn-default pull-left margin-15 button-color" href="#save_continue_rows_str" name="save_continue_rows_str">Save & Continue</a>-->
							
						</div>
					</div>
			
			
			<%} %>
</body>
</html>