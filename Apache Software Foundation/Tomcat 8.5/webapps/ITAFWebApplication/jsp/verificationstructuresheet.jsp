<%@page import="java.util.List"%>
<%@page import="com.majesco.itaf.dao.ProjectTransactions"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

	<form  action="./itafpack.do" role="createverifyform" data-toggle="validator" method="post">
			<input type="hidden" name="projectname" value="${currentProject}"/>
			    
			     <input type="hidden" name="verifytransactionaction" value="${currentverifyTask}"/>
			    
					<div class="col-md-12 form-group">
						
							
					</div>
					
					<div class=" col-md-12 form-group">
								
									<label class="col-sm-2" for="verifytransactionname">Verification Data
										Name</label>
									<div class="row">
										<div class="col-sm-4" >
											<input type="text" class="form-control" id="verifyverifyTransactionName"
												name="verifyverifyTransactionName" placeholder="Enter Object Element Name" maxlength="30"
												 value="">
										</div>
										<div class="col-sm-4" style="padding-left:0px;">
											<button type="submit" class="btn btn-default button-color" id="searchverifytrans"
												name="searchverifytrans" value="search">Search</button>
										</div>
									
								     </div>
						</div>

						<div class="col-md-12 form-group" id="btnCreate">
							<div class="col-sm-10" style="margin-left:180px;">
								<button type="submit" class="btn btn-default button-color" id="createverifyTrans"
									name="createverifyTrans" value="create">Create Object Verification</button>
							</div>
						</div>
						
				</form>
				<c:if test="${verificationTableList != null && fn:length(verificationTableList) gt 0}">
				
				<form  action="./itafpack.do" role="createTransactionform"  method="post">
			    <input type="hidden" name="verifytransactionname" value=""/>
			     <input type="hidden" name="verifytransactionaction" value=""/>
				<input type="hidden" name="projectname" value="${currentProject}"/>
					
					<div class="col-md-12">
					<div class="row clearfix">
				    
						<div class="column margin-15">
					<div class="table-responsive">
					  <table class="table table-condensed table-striped table-bordered table-hover no-margin">
					    <thead>
					      <tr>
					        <th style="width:5%">
					          <label class="no-margin"></label>
					        </th>
					        <th style="width:25%">Object Element Name</th>
					        <th style="width:15%" class="hidden-phone">Date</th>
					        <th style="width:20%" class="hidden-phone">Created By</th>
					         <th style="width:40%" class="hidden-phone">Actions</th>
					       
					      </tr>
					    </thead>
					    <tbody id="VerficationTable">
					    <c:forEach items="${verificationTableList}" var="vitem" varStatus="vstatus">
					  
					      <tr>
					        <td>
					          <label class="no-margin"><c:out value="${(vstatus.index)+1}"/></label>
					        </td>
					        <td class="clickabletVerifyname tdtextcolor">
					          <span class="name"><c:out value="${vitem.transactionName}"/></span>
					        </td>
					       
					         <td class="hidden-phone"><c:out value="${vitem.createddate}"/></td>
					        <td class="hidden-phone"><c:out value="${vitem.createdby}"/></td>
					       <td class="hidden-phone">
					          <div class="btn-group btn-group-sm" role="group">
					           <a href="#objverifyDtl" class="btn btn-default button-color" id="#objverifyDtl"
										name="#objverifyDtl" value="#objverifyDtl">Object Element Definition</a>
						        <!-- <a href="#" class="btn btn-default button-color" id="copyTrans"
										name="copyTrans" value="copyTrans">Copy Transaction</a>		
								<a href="#" class="btn btn-default button-color" id="renameTrans"
										name="renameTrans" value="renameTrans">Rename</a>	-->	
								<a href="#" class="btn btn-default button-color" id="deleteverifyTrans"
										name="deleteverifyTrans" value="deleteverifyTrans">Delete</a>				
					          </div>
					        </td>
					        
					      </tr>
					     
					     </c:forEach>
					    </tbody>
					  </table>
					</div>
					</div>
					<div class="col-md-12 text-center">
				      <ul class="pagination pagination-sm pager" id="myVPager"></ul>
				     </div>
					</div>
					
				
			<!-- 	<div class="modal fade" id="renameTDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-header button-color">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        <h4 class="modal-title button-color" id="myModalLabel">Rename Transaction</h4>
					      </div>
					      <div class="modal-body">
					       <div class="row">
					       		<div class="col-sm-4" >
									<input type="text" class="form-control" id="oldtranname"
												name="oldtranname" placeholder="Enter Transaction Name" maxlength="30"
												 value="" readonly="readonly">
							</div>
										
								<div class="col-sm-4" >
									<input type="text" class="form-control" id="newtranname"
												name="newtranname" placeholder="Enter New Transaction Name" maxlength="30"
												 value="" required>
								</div>
					       </div>
					      </div>
					      <div class="modal-footer">
					        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					        <button type="submit" class="btn btn-primary" name ="renameTrans" id ="renameTrans" value="renameTrans">Save</button>
					      </div>
					    </div>
					  </div>
				</div>-->
				
				 <div class="modal fade" id="deleteVDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-header button-color">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        <h4 class="modal-title button-color" id="myModalLabel">Delete Verification Details</h4>
					      </div>
					      <div class="modal-body">
					          Do you really want to delete Verification Details?
					      </div>
					      <div class="modal-footer">
					         <button type="submit" class="btn btn-primary" name ="deleteverifytransSubmit" id ="deleteverifytransSubmit" value="delete">Yes</button>
					         <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
					      </div>
					    </div>
					  </div>
				</div>
				</div>
				</form>
				
				</c:if>	
				<div class="modal fade" id="verifystrucureSheetDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<form id="verifystructureform" name="verifystructureform" action="./itafpack.do" method="post" role="verifystructureform" data-toggle="validator">
					  <div class="modal-dialog mymodal-lg" role="document">
					    <div class="modal-content">
					      <div class="modal-header button-color">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        <h4 class="modal-title button-color" id="myModalLabel">Object Element -<c:out value="${currentverifyTransaction}"/></h4>
					        					        
					      </div>
					      <div class="modal-body" style="padding: 0px;">
					          <jsp:include page="verificationStrDetails.jsp"></jsp:include>
					      </div>
					      <div class="modal-footer">
					         <button type="submit" class="btn btn-primary" name ="save_rows_str_verify" id ="save_rows_str_verify" value="">Save</button>
					         <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					      </div>
					    </div>
					  </div>
				  </form>
				</div>
</body>
</html>