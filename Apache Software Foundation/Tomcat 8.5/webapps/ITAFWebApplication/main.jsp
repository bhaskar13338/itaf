<html>
<head>

<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">


<link rel="stylesheet" type="text/css" href="css/demo3/try.css">

<title>ITAF-Object Capture Utility</title>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/demo3/responsiveTables.js"></script>

<script>
	//alert(localStorage.getItem('projectName'));
	var projectPath;
	$(document).ready(function() {

		$('#btn').on('click', function() {
			// alert('clicked');  
			// alert($('#urltxt').val());         
			$('#frame').attr('src', $('#urltxt').val());
		});
	});

	$(window)
			.bind(
					"load",
					function() {

						$
								.ajax({
									type : 'POST',
									url : '/ITAFWebApplication/Seleniumservice/resources/getProjectDetails',
									dataType : 'text',
									//data:{projectName: localStorage.getItem('projectName')},
									contentType : 'application/x-www-form-urlencoded; charset=utf-8',
									success : function(response) {

										//alert(response);
										var data = JSON.parse(response);
										var transactionList = (data.tranSactionList)
												.split(',');
										//	alert(transactionList)
										projectPath = data.reproPath;
										$("#urltxt").val(data.baseURL);
										$("#dropdown").find('option').remove();
										for (i = 0; i < transactionList.length; i++) {
											$("#dropdown")
													.append(
															'<option value="'+transactionList [i]+'">'
																	+ transactionList[i]
																	+ '</option>');
										}

									},
									error : function(error) {
										console.log(error);
									}
								});
					});
	function insertScript(doc, target, src, callback) {
		var s = doc.createElement("script");
		s.type = "text/javascript";
		if (callback) {
			if (s.readyState) { //IE
				s.onreadystatechange = function() {
					if (s.readyState == "loaded" || s.readyState == "complete") {
						s.onreadystatechange = null;
						callback();
					}
				};
			} else { //Others
				s.onload = function() {
					callback();
				};
			}
		}
		s.src = src;
		target.appendChild(s);
	}
</script>
<style type="text/css">
.iframecon {
	width: 100%;
}

.iframecon1 {
	height: 0;
	width: 100%;
	padding-bottom: 50%;
	overflow: hidden;
	position: relative;
}

.iframecon1 iframe {
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	position: absolute;
}

@media only screen and (max-width: 800px) {
	/* Force table to not be like tables anymore */
	#no-more-tables table, #no-more-tables thead, #no-more-tables tbody,
		#no-more-tables th, #no-more-tables td, #no-more-tables tr {
		display: block;
	}

	/* Hide table headers (but not display: none;, for accessibility) */
	#no-more-tables thead tr {
		position: absolute;
		top: -9999px;
		left: -9999px;
	}
	#no-more-tables tr {
		border: 1px solid #ccc;
	}
	#no-more-tables td {
		/* Behave like a "row" */
		border: none;
		border-bottom: 1px solid #eee;
		position: relative;
		padding-left: 50%;
		white-space: normal;
		text-align: left;
	}
	#no-more-tables td:before {
		/* Now like a table header */
		position: absolute;
		/* Top/left values mimic padding */
		top: 6px;
		left: 6px;
		width: 45%;
		padding-right: 10px;
		white-space: nowrap;
		text-align: left;
		font-weight: bold;
	}

	/*
        Label the data
        */
	#no-more-tables td:before {
		content: attr(data-title);
	}
}
</style>
<title>Untitled Document</title>
</head>

<script>
	var arrData = {};
	//var global;
	arrData.elementsList = new Array();
	var eventMethod = window.addEventListener ? "addEventListener"
			: "attachEvent";
	var eventer = window[eventMethod];
	var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";

	// Listen to message from child window
	eventer(messageEvent, function(e) {
		//alert(e.data);
		var mess = e.data;
		//alert(mess);
		console.log('parent received message!:  ', mess);
		var tabId = "dataTable";
		addRow("dataTable", mess);
		//arrData = typeof mess != 'object' ? JSON.parse(mess) : mess;
		arrData.elementsList.push(JSON.parse(mess));
		global = mess;
		console.log(JSON.stringify(arrData));
		//alert(JSON.stringify(arrData));
	}, false);

	function addRow(tableId, mess) {

		var table = document.getElementById(tableId);
		var json = JSON.parse(mess);
		var split;
		var rowCount = table.rows.length;
		var row = table.insertRow(rowCount);

		var cell1 = row.insertCell(0);
		var element1 = document.createElement("input");
		element1.type = "checkbox";
		element1.name = "chkbox[]";
		cell1.appendChild(element1);

		var cell2 = row.insertCell(1);
		cell2.innerHTML = rowCount;

		var cell3 = row.insertCell(2);
		var element2 = document.createElement("input");
		element2.type = "text";
		element2.name = "txtbox[]";
		//	element2.style="width:200px"
		element2.className = "form-control";
		element2.value = (json.logicalName).split(',')[0];
		cell3.appendChild(element2);

		var cell4 = row.insertCell(3);
		var element3 = document.createElement("select");
		element3.id = "controlID_" + rowCount;
		element3.name = "txtboxid";
		//element3.style="width:200px";
		element3.className = "form-control";
		element3.onchange = function() {
			getControlValue(this.value, tableId);
		};
		//element3.value=json.controlID;
		cell4.appendChild(element3);

		if (!isEmpty(json.elementId)) {
			var option1 = document.createElement("option");
			option1.value = "id";
			option1.selected = "";
			option1.innerHTML = "id";
			element3.appendChild(option1);
		}
		if (!isEmpty(json.elementName)) {
			var option2 = document.createElement("option");
			option2.value = "name";
			option2.selected = "";
			option2.innerHTML = "name";
			element3.appendChild(option2);
		}
		if (!isEmpty(json.xPathValue)) {
			var option3 = document.createElement("option");
			option3.value = "xPath";
			option3.selected = "";
			option3.innerHTML = "xPath";
			element3.appendChild(option3);
		}
		var cell5 = row.insertCell(4);
		var element4 = document.createElement("input");
		element4.type = "text";
		element4.id = "controlName_" + rowCount;
		//element4.style="width:200px"
		element4.className = "form-control";
		element4.value = (json.elementId).split(',')[0];
		cell5.appendChild(element4);

		var cell6 = row.insertCell(5);
		var element5 = document.createElement("input");
		element5.type = "text";
		element5.id = "controlValue_" + rowCount;
		//element5.style = "width:200px"
		element5.className = "form-control";
		element5.value = json.elementValue;
		cell6.appendChild(element5);

		var cell7 = row.insertCell(6);
		var element6 = document.createElement("input");
		element6.type = "text";
		element6.id = "controlPath_" + rowCount;
		//element6.style="width:200px"
		element6.className = "form-control";
		if (!isEmpty(json.elementId)) {

			element6.value = (json.elementId).split(',')[1];

		} else if (!isEmpty(json.elementName)) {
			element6.value = (json.elementName).split(',')[1];
		} else if (!isEmpty(json.xPathValue)) {
			element6.value = (json.xPathValue);
		}
		cell7.appendChild(element6);

		var cell8 = row.insertCell(7);
		var element7 = document.createElement("input");
		element7.type = "text";
		element7.id = "controlType_" + rowCount;
		//element7.style="width:200px"
		element7.className = "form-control";
		element7.value = json.controlType;
		cell8.appendChild(element7);
	}

	function getControlValue(val, tableId) {
		var rows = document.getElementById(tableId).getElementsByTagName(
				'tbody')[0].getElementsByTagName('tr');
		var index;
		var drpValue = val;
		for (i = 0; i < rows.length; i++) {
			rows[i].onchange = function(val) {
				//alert(this.rowIndex);
				index = this.rowIndex;
				if (drpValue == 'name') {
					document.getElementById("controlPath_" + index).value = (arrData.elementsList[index - 1].elementName)
							.split(',')[1];
					arrData.elementsList[index - 1].controlID = drpValue;
				}
				if (drpValue == 'id') {
					document.getElementById("controlPath_" + index).value = (arrData.elementsList[index - 1].elementId)
							.split(',')[1];
					arrData.elementsList[index - 1].controlID = drpValue;
				}
				if (drpValue == 'xPath') {
					document.getElementById("controlPath_" + index).value = arrData.elementsList[index - 1].xPathValue;
					arrData.elementsList[index - 1].controlID = drpValue;
				}
			}

		}
		//if(val=='name'){
		//document.getElementsByName("controlPath_"+index).value=(arrData.elementsList[index-1].elementName).split(',')[1];
		//}
	}
	function deleteRow(tableID) {
		try {
			var table = document.getElementById(tableID);
			var rowCount = table.rows.length;

			for (var i = 1; i < rowCount; i++) {
				var row = table.rows[i];
				//var chkbox = row.cells[0].childNodes[0];
				var chkbox = row.querySelector('input[type=checkbox]');
				if (null != chkbox && true == chkbox.checked) {
					table.deleteRow(i);
					arrData.elementsList.splice(i - 1, 1);
					rowCount--;
					i--;
				}
			}
		} catch (e) {
			alert(e);
		}
	}
	function inject_Script() {
		var http = new getXMLHttpRequestObject();
		var url = "/ITAFWebApplication/Seleniumservice/resources/injectScript";
		var parameters = "";
		http.open("POST", url, true);

		// Send the proper header information along with the request
		http.setRequestHeader("Content-type",
				"application/x-www-form-urlencoded");
		//http.setRequestHeader("Content-length", parameters.length);
		//http.setRequestHeader("Connection", "close");

		http.onreadystatechange = function() {// Handler function for call
			// back on state change.
			if (http.readyState == 4) {
				//alert(http.responseText);
			}
		}
		http.send(parameters);
	}

	function save_Data() {
		var http = new getXMLHttpRequestObject();
		var url = "/ITAFWebApplication/Seleniumservice/resources/updateCSV";
		//arrData=JSON.parse(arrData);
		var str = JSON.stringify(arrData);
		var parameters = str;
		var transactFilePathName = projectPath + "\\" + $('#dropdown').val();
		//alert(transactFilePathName);
		http.open("POST", url, true);

		// Send the proper header information along with the request
		http.setRequestHeader("Content-type",
				"application/x-www-form-urlencoded");
		http.setRequestHeader("Content-length", parameters.length);
		http.setRequestHeader("Connection", "close");

		http.onreadystatechange = function() {// Handler function for call
			// back on state change.
			if (http.readyState == 4) {
				//alert(http.responseText);
			}
		}
		http.send("name=" + parameters + "&" + "filePath="
				+ transactFilePathName);
		arrData.elementsList = new Array();
		$("#dataTable").empty();
	}
	function getXMLHttpRequestObject() {
		var xmlhttp;
		/*
		 * @cc_on @if (@_jscript_version >= 5) try { xmlhttp = new
		 * ActiveXObject("Msxml2.XMLHTTP"); } catch (e) { try { xmlhttp = new
		 * ActiveXObject("Microsoft.XMLHTTP"); } catch (E) { xmlhttp = false; } }
		 * @else xmlhttp = false; @end @
		 */
		if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
			try {
				xmlhttp = new XMLHttpRequest();
			} catch (e) {
				xmlhttp = false;
			}
		}
		return xmlhttp;
	}

	function isEmpty(val) {
		return (val === undefined || val == null || val.length <= 0) ? true
				: false;
	}
</script>

<body>
	<div class="container"
		style="margin-top: 25px; margin-left: 15px; padding: 0px;">
		<div class=" col-md-12 form-group">
			<div class="row">
				<label class="col-sm-2 input-lg" for="appUrl">Application
					Url:</label>
				<div class="col-sm-4">

					<input type="text" class="form-control" id="urltxt" name="urltxt"
						placeholder="Enter Screen Object Name" value="">
				</div>
				<div class="col-sm-4" style="padding-left: 0px;">
					<button type="submit" class="btn btn-default button-color" id="btn"
						name="btn" value="Go">Go</button>
				</div>

			</div>

			<!-- <input type="text" id="urltxt" style="width:600px" value="" />
		<input type="button" id="btn" value="Go" />-->
		</div>
		<div class=" col-md-12 form-group">


			<div class="row">
				<label class="col-sm-2 input-lg" for="transactionname">Object
					Screen Name:</label>
				<div class="col-sm-4">
					<select name="dropdown" id="dropdown" class="form-control">
						<option>Select</option>
					</select>
				</div>
				<div class="col-sm-4" style="padding-left: 0px;">
					<button type="submit" class="btn btn-default button-color"
						id="Next" name="Next" value="Save" onclick='save_Data();'>Save</button>
				</div>

			</div>
		</div>

		<!-- <p> 
	<select id="dropdown"></select>
	</p>
	<p>
		<input type='button' name='Next' value='Save'
			onclick='save_Data();' />	</p>-->
		<div id="message"></div>
		<div class="iframecon">
			<div class="iframecon1">
				<!-- <iframe id="frame" src="" width="100%" height="300" onload="inject_Script();" > </iframe> -->
				<iframe id="frame" src="" onload="inject_Script();"
					class="embed-responsive-item" sandbox="allow-same-origin allow-scripts"></iframe>
			</div>
		</div>
		<div class="container" style="margin-top: 10px;">
			<div class="row">
				<div class="no-more-tables">
					<table id="dataTable"
						class="col-sm-12 table-condensed table-striped table-bordered table-hover no-margin">
						<tbody>
							<tr></tr>
						</tbody>
					</table>

				</div>
			</div>
		</div>
		<!--<TABLE id="dataTable" width="600px" border="1">
		<TR>
			
		</TR>
	</TABLE>-->
		<div class="col-md-12 form-group" style="margin-top: 10px;">
			<div class="row">
				<button type="submit" class="col-sm-2 btn btn-default button-color"
					id="deletedbtn" onclick="deleteRow('dataTable')" name="deletedbtn"
					value="Delete Row">Delete Row</button>
			</div>
		</div>
		<!-- <p>
	<INPUT type="button" value="Delete Row" onclick="deleteRow('dataTable')" />
	</p>-->

	</div>
</body>
</html>